import os

def replace_recursive(token, replacement):
    for dname, dirs, files in os.walk("web"):
        for fname in files:
            if not(fname.endswith(".html") or fname.endswith(".js")):
                print("Ignore {}".format(fname))
                continue

            fpath = os.path.join(dname, fname)
            print("Reading {}".format(fpath))
            with open(fpath, encoding="utf-8") as fr:
                s = fr.read()
            s = s.replace(token, replacement)
            with open(fpath, "w", encoding="utf-8") as fw:
                fw.write(s)


""" def main():
    try:
        fp = open('.env.local')
        line = fp.readline()
        while line:
            line_elements = line.split("=")
            if len(line_elements) < 2:
                continue
            print("Replacinv {} by {}".format(line_elements[0], line_elements[1]))
            replace_recursive(trim(line_elements[0]), trim(line_elements[1]))
            line = fp.readline()
    finally:
        fp.close()

    
if __name__ == "__main__":
    sys.exit(main()) """
