if (!loc) {
  var loc = {};
}

loc.locale = "en";

loc.payOk = "Your payment has been taken into account";
loc.ok = "Transaction completed";
loc.ko = "Transaction failed";
loc.cancel = "Transaction cancelled";
loc.wait = "Transaction pending";

loc.send = "Send";
loc.confirm = "Are you sure ?";
loc.generalError = "We are unable to fulfill your request";
loc.mandatoryFields = "Theses fields are mandatory.";
loc.pleasAgreeLicence = "Please agree our user licence first.";
loc.passwordConfirm = "The password and its confirmation are not identical.";
loc.other = "Other";
loc.results = "results";

loc.fillLater = "Not filled";
loc.noResultsSearch = "No results, please search";
loc.noResultsSearchFor = 'No results for "{0}"';
loc.workUnsaved =
  "Your work will be lost if you don't save it to an account. Still want to close this window ?";
loc.shouldBeAuth =
  "You should be authenticated for this. Please re-login or register.";
loc.shouldBeHttps = "You should be in HTTPS for this. Go to HTTPS ?";
loc.saveItFirst = "Save your work before doing this";
loc.popupBlocked =
  "To make it work, please desactivate the pop-up bloquer for this site.";
loc.thank = "Thank you!";
loc.someone = "someone";
loc.paymentLeft = "{0} to pay";

loc.payDelay = "in {0} d";
loc.payLate = "{0} d late";

loc.del = "Delete";
loc.edit = "Edit";
loc.setIdentity = "Set current identity";
loc.newDoc = "New document";

loc.phoneAbrev = "Ph.";
loc.faxAbrev = "Fax";
loc.fromUntil = "{0} from {1} until {2}";
loc.fromNow = "{0} from {1} until now (subscription)";

loc.messageStatus = {};
loc.messageStatus["NONE"] = "Not saved";
loc.messageStatus["MODIFIED"] = "Draft";
loc.messageStatus["SENT"] = "Sent";
loc.messageStatus["ISSUED"] = "Issued";
loc.messageStatus["RECEIVED_BY"] = "Received";
loc.messageStatus["SENDER_ACK_RECEPTION"] = "Received";
loc.messageStatus["ACCEPTED"] = "Accepted";
loc.messageStatus["REFUSED"] = "Refused";
loc.messageStatus["CLOSED"] = "Closed";
loc.messageStatus["ISSUE-ACTION_CANCELED"] = "Issue cancelled";
loc.messageStatus["CLOSED-ACTION_CANCELED"] = "Closing cancelled";
loc.messageStatus["TRASH"] = "Trash";
loc.messageStatus["DELETED"] = "Deleted";
loc.messageStatus["PAYMENT"] = "Partly paid";
loc.messageStatus["UNPAID"] = "To be paid";
loc.messageStatus["PAID"] = "Paid";
loc.messageStatus["UNREAD"] = "Unread";
loc.messageStatus["READ"] = "Read";

loc.placeholder = {};
loc.placeholder["address-name"] = "Company name";
loc.placeholder["address-street1"] = "Address";
loc.placeholder["address-street2"] = "Address (next)";
loc.placeholder["postal_code-code"] = "Zip code";
loc.placeholder["postal_code-city"] = "City";
loc.placeholder["country"] = "Country";
loc.placeholder["address-vat-id"] = "VAT ID";
loc.placeholder["address-misc"] = "Other company information";
loc.placeholder["person-label"] = "Contact name";
loc.placeholder["person-email"] = "Contact email";
loc.placeholder["person-phone"] = "Contact phone";
loc.placeholder["person-fax"] = "Contact fax";
loc.placeholder["person-misc"] = "Other contact information";

loc.addressDel = "The address has been deleted";

Number.prototype.formatMoney = function () {
  return this.formatMoneyBase(2, ".", ",");
};

loc.defaultCurrency = "GBP";

loc.date = {
  months: [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ],
  monthNamesShort: [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ],
  dayNames: [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
  ],
  days: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
  today: "Aujourd'hui",
  clear: "Init",
  close: "Fermer",
  dateFormat: "dd/mm/yy",
  dateTimeFormat: "dd/mm/yy HH:MM"
};

loc.number = {
  dec_sep: ".",
  thou_sep: ","
};

loc.linkStatus = {};
loc.linkStatus.loading = "Loading...";
loc.linkStatus.saving = "Saving...";
loc.linkStatus.saved = "Content saved";
loc.linkStatus.saveError = "Saving error!";
loc.linkStatus.error = "Error";

loc.history = {};
loc.yourself = "yourself";
loc.history["MODIFIED"] = "Modified by {0}";
loc.history["ISSUED"] = "Issued by {0}";
loc.history["SENT"] = "Sent by {0}";
loc.history["RECEIVED_BY"] = "Received par {0}";
loc.history["SENDER_ACK_RECEPTION"] = "Sender {0} acknoledged reception";
loc.history["ACCEPTED"] = "Accepted by {0}";
loc.history["REFUSED"] = "Refused by {0}";
loc.history["PAYMENT"] = "Payment registered by {0}";
loc.history["CLOSED"] = "Closed by {0}";
loc.history["ACTION_CANCELED"] = "Cancelled on {0} by {1}";
loc.history["TRASH"] = "Put into trash of {0}";
loc.history["DELETED"] = "Deleted of {0}'s books";
loc.history.sending = {};
loc.history.sending["OTHER"] = "to {0} (off-site)";
loc.history.sending["EMAIL"] = "to {0} by e-mail";
loc.history.sending["FAX"] = "to {0} by fax at {1}";
loc.history.sending["POST"] = "to {0} by postal mail at {1}";
loc.history.sending["SMS"] = "to {0} by SMS at {1}";
loc.history.sending["SITE"] = "to {0} by the web-site";

loc.messageTypes = {};
loc.messageTypes["QUOTE_REQUEST"] = "Quote Request";
loc.messageTypes["QUOTE"] = "Quote";
loc.messageTypes["PURCHASE_ORDER"] = "Purchase Order";
loc.messageTypes["INVOICE"] = "Invoice";
loc.messageTypes["PROFORMA_INVOICE"] = "Proforma Invoice";
loc.messageTypes["SELF_INVOICE"] = "Invoice";
loc.messageTypes["FEE_NOTE"] = "Fee Note";
loc.messageTypes["CREDIT_NOTE"] = "Credit Note";
loc.messageTypes["PAYMENT_REQUEST"] = "Payment Request";
loc.messageTypes["USER_FEEDBACK"] = "User Feedback";
loc.messageTypes["LETTER"] = "Letter";

loc.paymentTypes = {};
loc.paymentTypes.CASH = "in cash";
loc.paymentTypes.CB = "by credit-card";
loc.paymentTypes.PAYPAL = "by Paypal";
loc.paymentTypes.TRANSFERT = "by transfert";
loc.paymentTypes.CHECK = "by cheque";
loc.paymentTypes.OTHER = "";

loc.paymentSent = "Payment sent with reference {0}";

loc.writingStatus = {};
loc.writingStatus.VALID = "Valid";
loc.writingStatus.UNVALID = "Error";
loc.writingStatus.PENDING = "Not closed";
loc.writingStatus.CHALLENGED = "Challenged";

loc.smtpStatus = {};
loc.smtpStatus.boxFull = "Mailbox full";
loc.smtpStatus.badAddress = "Wrong mail address";
loc.smtpStatus.tooBig = "Message too big";
loc.smtpStatus.tempProblem = "Temporary recipient mail server error";
loc.smtpStatus.generalError = "Recipient mail server error";

loc.first = "First";
loc.prev = "Previous";
loc.page = "Page {0}";
loc.next = "Next";
loc.last = "Last";

loc.statFeedback = "{0} from {1} to {2}, grouped by {3}";
loc.year = "year";
loc.quarter = "quater";
loc.month = "month";
loc.week = "week";
loc.message_number = "message number";
loc.toIssue = "to be issued";
loc.recipient = "recipient";
loc.reference = "reference";
loc.description = "description";
loc.tax_rate = "tax rate";
loc.none = "period";

loc.registerProposal = {
  title: "E-mail Validation",
  initialText:
    '<p><b>{0}</b> sent you a message to the address <b>{1}</b>.</p><p>After having read and agreed <a href="licence.html">our licence</a>, please validate this address by clicking the hyperlink in the message we are about to send to you.</p><p>You will have to do it only once and after you will have a direct access to your messages.</p>',
  sendMeLink: "Send me the link !",
  confirmation: "Please click on the link to acknoledge the present message. "
};

loc.countries = {
  AF: "Afghanistan",
  AX: "Åland Islands",
  AL: "Albania",
  DZ: "Algeria",
  AS: "American Samoa",
  AD: "Andorra",
  AO: "Angola",
  AI: "Anguilla",
  AQ: "Antarctica",
  AG: "Antigua and Barbuda",
  AR: "Argentina",
  AM: "Armenia",
  AW: "Aruba",
  AU: "Australia",
  AT: "Austria",
  AZ: "Azerbaijan",
  BS: "Bahamas",
  BH: "Bahrain",
  BD: "Bangladesh",
  BB: "Barbados",
  BY: "Belarus",
  BE: "Belgium",
  BZ: "Belize",
  BJ: "Benin",
  BM: "Bermuda",
  BT: "Bhutan",
  BO: "Bolivia",
  BA: "Bosnia and Herzegovina",
  BW: "Botswana",
  BV: "Bouvet Island",
  BR: "Brazil",
  IO: "British Indian Ocean Territory",
  BN: "Brunei Darussalam",
  BG: "Bulgaria",
  BF: "Burkina Faso",
  BI: "Burundi",
  KH: "Cambodia",
  CM: "Cameroon",
  CA: "Canada",
  CV: "Cape Verde",
  KY: "Cayman Islands",
  CF: "Central African Republic",
  TD: "Chad",
  CL: "Chile",
  CN: "China",
  CX: "Christmas Island",
  CC: "Cocos (Keeling) Islands",
  CO: "Colombia",
  KM: "Comoros",
  CG: "Congo",
  CK: "Cook Islands",
  CR: "Costa Rica",
  HR: "Croatia",
  CU: "Cuba",
  CY: "Cyprus",
  CZ: "Czech Republic",
  KP: "Democratic People's Republic of Korea",
  DK: "Denmark",
  DJ: "Djibouti",
  DM: "Dominica",
  DO: "Dominican Republic",
  EC: "Ecuador",
  EG: "Egypt",
  SV: "El Salvador",
  GQ: "Equatorial Guinea",
  ER: "Eritrea",
  EE: "Estonia",
  ET: "Ethiopia",
  FK: "Falkland Islands (Malvinas)",
  FO: "Faroe Islands",
  FM: "Federated States of Micronesia",
  FJ: "Fiji",
  FI: "Finland",
  FR: "France",
  GF: "French Guiana",
  PF: "French Polynesia",
  TF: "French Southern Territories",
  GA: "Gabon",
  GM: "Gambia",
  GE: "Georgia",
  DE: "Germany",
  GH: "Ghana",
  GI: "Gibraltar",
  GR: "Greece",
  GL: "Greenland",
  GD: "Grenada",
  GP: "Guadeloupe",
  GU: "Guam",
  GT: "Guatemala",
  GG: "Guernsey",
  GN: "Guinea",
  GW: "Guinea-Bissau",
  GY: "Guyana",
  HT: "Haiti",
  HM: "Heard Island and Mcdonald Islands",
  VA: "Vatican City State",
  HN: "Honduras",
  HK: "Hong Kong",
  HU: "Hungary",
  IS: "Iceland",
  IN: "India",
  ID: "Indonesia",
  IR: "Iran",
  IQ: "Iraq",
  IE: "Ireland",
  IM: "Isle of Man",
  IL: "Israel",
  IT: "Italy",
  CI: "Ivory Coast",
  JM: "Jamaica",
  JP: "Japan",
  JE: "Jersey",
  JO: "Jordan",
  KZ: "Kazakhstan",
  KE: "Kenya",
  KI: "Kiribati",
  KW: "Kuwait",
  KG: "Kyrgyzstan",
  LA: "Lao People's Democratic Republic",
  LV: "Latvia",
  LB: "Lebanon",
  LS: "Lesotho",
  LR: "Liberia",
  LY: "Libyan Arab Jamahiriya",
  LI: "Liechtenstein",
  LT: "Lithuania",
  LU: "Luxembourg",
  MO: "Macao",
  MK: "Macedonia",
  MG: "Madagascar",
  MW: "Malawi",
  MY: "Malaysia",
  MV: "Maldives",
  ML: "Mali",
  MT: "Malta",
  MH: "Marshall Islands",
  MQ: "Martinique",
  MR: "Mauritania",
  MU: "Mauritius",
  YT: "Mayotte",
  MX: "Mexico",
  MD: "Moldova",
  MC: "Monaco",
  MN: "Mongolia",
  ME: "Montenegro",
  MS: "Montserrat",
  MA: "Morocco",
  MZ: "Mozambique",
  MM: "Myanmar",
  NA: "Namibia",
  NR: "Nauru",
  NP: "Nepal",
  NL: "Netherlands",
  AN: "Netherlands Antilles",
  NC: "New Caledonia",
  NZ: "New Zealand",
  NI: "Nicaragua",
  NE: "Niger",
  NG: "Nigeria",
  NU: "Niue",
  NF: "Norfolk Island",
  MP: "Northern Mariana Islands",
  NO: "Norway",
  PS: "Occupied Palestinian Territory",
  OM: "Oman",
  PK: "Pakistan",
  PW: "Palau",
  PA: "Panama",
  PG: "Papua New Guinea",
  PY: "Paraguay",
  PE: "Peru",
  PH: "Philippines",
  PN: "Pitcairn",
  PL: "Poland",
  PT: "Portugal",
  PR: "Puerto Rico",
  QA: "Qatar",
  KR: "Republic of Korea",
  RE: "Réunion",
  RO: "Romania",
  RU: "Russian Federation",
  RW: "Rwanda",
  BL: "Saint Barthélemy",
  SH: "Saint Helena",
  KN: "Saint Kitts and Nevis",
  LC: "Saint Lucia",
  MF: "Saint Martin",
  PM: "Saint Pierre and Miquelon",
  VC: "Saint Vincent and the Grenadines",
  WS: "Samoa",
  SM: "San Marino",
  ST: "Sao Tome and Principe",
  SA: "Saudi Arabia",
  SN: "Senegal",
  RS: "Serbia",
  SC: "Seychelles",
  SL: "Sierra Leone",
  SG: "Singapore",
  SK: "Slovakia",
  SI: "Slovenia",
  SB: "Solomon Islands",
  SO: "Somalia",
  ZA: "South Africa",
  GS: "South Georgia and the south Sandwich Islands",
  ES: "Spain",
  LK: "Sri Lanka",
  SD: "Sudan",
  SR: "Suriname",
  SJ: "Svalbard and Jan Mayen",
  SZ: "Swaziland",
  SE: "Sweden",
  CH: "Switzerland",
  SY: "Syrian",
  TW: "Taiwan",
  TJ: "Tajikistan",
  TZ: "Tanzania",
  TH: "Thailand",
  CD: "The Democratic Republic of Congo",
  TL: "Timor-Leste",
  TG: "Togo",
  TK: "Tokelau",
  TO: "Tonga",
  TT: "Trinidad and Tobago",
  TN: "Tunisia",
  TR: "Turkey",
  TM: "Turkmenistan",
  TC: "Turks and Caicos Islands",
  TV: "Tuvalu",
  UG: "Uganda",
  UA: "Ukraine",
  AE: "United Arab Emirates",
  GB: "United Kingdom",
  US: "United States",
  UM: "United States Minor Outlying Islands",
  UY: "Uruguay",
  UZ: "Uzbekistan",
  VU: "Vanuatu",
  VE: "Venezuela",
  VN: "Viet Nam",
  VG: "Virgin Islands, British",
  VI: "Virgin Islands, U.S.",
  WF: "Wallis and Futuna",
  EH: "Western Sahara",
  YE: "Yemen",
  ZM: "Zambia",
  ZW: "Zimbabwe"
};
