/**
 * OAuth callback
 */
(function (window, undefined) {
  // Get data from URL
  var queryString = location.hash.substring(1) || location.search.substring(1),
    stateObj,
    system;
  if (queryString) {
    var params = queryString.split("&");

    for (var i = 0; i < params.length; i++) {
      if (params[i].indexOf("state=") === 0 && params[i].length > 8) {
        var state;
        try {
          state = decodeURIComponent(params[i].substring(6));
          if (state) {
            stateObj = JSON.parse(state);
            if (stateObj.system) {
              system = stateObj.system;
              $(".systemName").html(stateObj.system);
            }
          }
        } catch (e) {
          ns.logError("Error parsing state " + state);
          ns.logError(e);
        }
      }
    }
  }

  // Re-validate data on server
  console.log("Submiting " + system + " OAuth " + queryString);
  ns.ws("POST", "v1/oauth?" + queryString)
    .then(function (result) {
      console.log(system + " OAuth sucessfull, outcome " + result.outcome);
      ns.logEvent("Session", "oauthSuccess", stateObj.system);
      $("#error").toggle(false);
      $("#pleaseWait").toggle(false);
      $("#success").toggle(true);

      window.location.href =
        (result.outcome == "register" ? "register_identity.html" : null) ||
        stateObj.nextUrl ||
        ns.serverUrl; // Go to next-step
    })
    .catch(function (xhr) {
      console.log(xhr.jsonResp);
      ns.logEvent("Session", "oauthFailure", system);
      $("#pleaseWait").toggle(false);
      $("#error").toggle(true);

      $(".registerLnk").click(function (e) {
        process(queryString + "&forceReg=true", system);
      });
    });
})(this);
