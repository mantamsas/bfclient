/**
 * Display relevant page
 */
(function(window) {
    var params = ns.getQueryParams(),
        outcome = params.outcome && loc[params.outcome];
    $('#pleaseWait').text(outcome ? outcome : "");
    if (params && params.returnUrl) {
        params.returnUrl = decodeURIComponent(params.returnUrl);
        if (params.messageId) {
            params.returnUrl += '?messageId' + params.messageId;
        }
        $('#backLnk').attr('href', params.returnUrl);
    }
    $('#backLnk').toggle(true)
})(this);