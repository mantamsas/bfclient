"use strict";

$(document).ready(function() {
    $('#activateCb').click(function(e) {
        ns.ws('POST', 'v1/preference', {
                etransaccbAllowed: true,
                etransaccbdevAllowed: true
            })
            .then(function(e) {
                $('#activateCb').toggle(false);
                $('#followUp').toggle(true);
            });
    });
});
//# sourceURL=/static/js/enrollCb.js