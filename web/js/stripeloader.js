"use strict";

/* In charge of loading Stripe scripts and keys and to show the forms */
ns.stripeloader = {};

ns.stripeloader.ensureHttps = function () {
  var aLoc = window.location;
  if (aLoc.protocol != "https:") {
    if (confirm(loc.shouldBeHttps)) {
      console.log("Redirection to HTTPS");
      location.href = "https:" + aLoc.href.substring(aLoc.protocol.length);
    }
  }
};

// Preload Stripe
ns.stripeloader.loadstripe = function () {
  return ns.ldone("Stripe", "https://js.stripe.com/v3/", true);
};

ns.stripeloader.getkey = function () {
  return new Promise(function (resolve, reject) {
    ns.ws("GET", "v1/stripe/key")
      .then(function (result) {
        if (result.publicKey) {
          ns.stripeloader.pk = result.publicKey;
          resolve(result.publicKey);
        } else {
          ns.showMessages({ errors: [{ message: "No Stripe public key" }] });
          reject();
        }
      })
      .catch(function (xhr) {
        reject(xhr);
      });
  });
};

ns.stripeloader.load = function (opts) {
  return new Promise(function (resolve, reject) {
    Promise.all([ns.stripeloader.loadstripe(), ns.stripeloader.getkey()])
      .then(function () {
        ns.stripeloader.stripe = Stripe(ns.stripeloader.pk, opts);
        console.log("Stripe init ok");
        resolve(ns.stripeloader.stripe );
      })
      .catch(function (e) {
        reject(e);
      });
  });
};

ns.stripeloader.style = {
  base: {
    color: "#32325d",
    lineHeight: "18px",
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: "antialiased",
    fontSize: "16px",
    "::placeholder": {
      color: "#aab7c4"
    }
  },
  invalid: {
    color: "#fa755a",
    iconColor: "#fa755a"
  }
};

/* Init and propose card form */
ns.stripeloader.propose = function (formId, onSubmit) {
  return new Promise(function (resolve, reject) {
    try {
      var elements = ns.stripeloader.stripe.elements({
        locale: (loc && loc.locale) || "fr"
      });
      ns.stripeloader.payment = elements.create("payment", {
        style: ns.stripeloader.style
      });

      ns.stripeloader.payment.mount("#stripe-payment-element");

      // Handle real-time validation errors from the card Element.
      ns.stripeloader.card.addEventListener("change", function (event) {
        var displayError = document.getElementById("payment-errors");
        if (event.error) {
          displayError.textContent = event.error.message;
        } else {
          displayError.textContent = "";
        }
      });
      document.getElementById("stripeWindow").style.display = "";

      resolve();
    } catch (error) {
      ns.logError(error);
      reject(error.message);
    }
  });
};

ns.stripeloader.initvue = function (templateId, urlParam, explain) {
  ns.stripeloader.vue = new Vue({
    el: templateId,
    data: {
      showw: false,
      explain: explain,
      chargeparam: urlParam
    }
  });
};

// Stripe payment popup window
Vue.component("stripe-popup", {
  template:
    '\
        <transition name="modal">\
            <div id="stripeWindow" class="modalWindow" style="pointer-events: auto;opacity: 1" v-show="showw">\
                <div class="window" style="border: 1px solid white; background-color: rgb(227, 232, 238);">\
                    <div class="header" style="background-color: #6772e5; height: 35px;position: relative; margin-bottom:0">\
                        <a href="https://stripe.com" style="color: white">\
                            <svg width="62" height="27">\
                                <title>Stripe</title>\
                                <path d="M5 10.1c0-.6.6-.9 1.4-.9 1.2 0 2.8.4 4 1.1V6.5c-1.3-.5-2.7-.8-4-.8C3.2 5.7 1 7.4 1 10.3c0 4.4 6 3.6 6 5.6 0 .7-.6 1-1.5 1-1.3 0-3-.6-4.3-1.3v3.8c1.5.6 2.9.9 4.3.9 3.3 0 5.5-1.6 5.5-4.5.1-4.8-6-3.9-6-5.7zM29.9 20h4V6h-4v14zM16.3 2.7l-3.9.8v12.6c0 2.4 1.8 4.1 4.1 4.1 1.3 0 2.3-.2 2.8-.5v-3.2c-.5.2-3 .9-3-1.4V9.4h3V6h-3V2.7zm8.4 4.5L24.6 6H21v14h4v-9.5c1-1.2 2.7-1 3.2-.8V6c-.5-.2-2.5-.5-3.5 1.2zm5.2-2.3l4-.8V.8l-4 .8v3.3zM61.1 13c0-4.1-2-7.3-5.8-7.3s-6.1 3.2-6.1 7.3c0 4.8 2.7 7.2 6.6 7.2 1.9 0 3.3-.4 4.4-1.1V16c-1.1.6-2.3.9-3.9.9s-2.9-.6-3.1-2.5H61c.1-.2.1-1 .1-1.4zm-7.9-1.5c0-1.8 1.1-2.5 2.1-2.5s2 .7 2 2.5h-4.1zM42.7 5.7c-1.6 0-2.5.7-3.1 1.3l-.1-1h-3.6v18.5l4-.7v-4.5c.6.4 1.4 1 2.8 1 2.9 0 5.5-2.3 5.5-7.4-.1-4.6-2.7-7.2-5.5-7.2zm-1 11c-.9 0-1.5-.3-1.9-.8V10c.4-.5 1-.8 1.9-.8 1.5 0 2.5 1.6 2.5 3.7 0 2.2-1 3.8-2.5 3.8z" style="fill: currentColor;"></path>\
                            </svg>\
                        </a>\
                        <a style="top: -3px;position: absolute;right: 0px;" href="#" class="closeBt" @click.prevent="close"><i class="fa fa-times" style="color: white; margin-top: 1px; font-size: 1.3em"></i></a>\
                    </div>\
                <div class="body" style="width: calc(100% - 4rem)">\
                    <slot></slot>\
                    <div v-bind:class="feedbackstatus">{{feedback}}</div>\
                    <form action="#" method="post" id="stripe-form">\
                        <div id="card-container" v-show="!payed" style="margin: 1em 0; widht"></div>\
                        <div v-show="!payed" class="buttons">\
                            <button @click.prevent="send" class="actbt stripe ajax">{{btlabel}}<div v-bind:class="[loading ? \'on\' : \'\', \'ajax-indicator\', \'ddd\']">&nbsp;</div></button>\
                        </div>\
                    </form>\
                </div>\
            </div>\
            </div>\
        </transition>\
        ',
  props: {
    showw: Boolean,
    btlabel: String,
    chargeparam: String
  },
  data: function () {
    return {
      payed: false,
      client_secret: "",
      loading: false,
      feedback: "",
      feedbackstatus: "info"
    };
  },
  mounted: function () {
    var that = this,
      elements = ns.stripeloader.stripe.elements({
        locale: (loc && loc.locale) || "fr"
      });

    ns.stripeloader.card = elements.create("card", {
      style: ns.stripeloader.style
    });
    ns.stripeloader.card.mount("#card-container");

    // Handle real-time validation errors from the card Element.
    ns.stripeloader.card.addEventListener("change", function (event) {
      if (event.error) {
        that.feedback = event.error.message;
        that.feedbackstatus = "error";
      } else {
        that.feedback = "";
      }
    });

    ns.ws("POST", "v1/stripe/client_payment_intent?" + this.chargeparam).then(function (
      result
    ) {
      if (result.client_secret) {
        that.client_secret = result.client_secret;
        console.log("Stripe client secret ok");
      } else {
        that.feedback = "No client_secret when creating payment intent";
      }
    });
  },
  methods: {
    send: function () {
      var that = this;
      console.log("Stripe button clicked");
      this.loading = true;
      ns.stripeloader.stripe
        .confirmCardPayment(this.client_secret, {
          payment_method: { card: ns.stripeloader.card }
        })
        .then(function (result) {
          that.loading = false;
          console.log("Stripe confirmCardPayment returned");
          console.log(result);
          if (result.error) {
            that.feedback = result.error;
            that.feedbackstatus = "error";
          } else {
            that.feedback = ns.formatMessage(loc.paymentSent, [
              result.paymentIntent.id
            ]);
            that.payed = true;
            that.feedbackstatus = "info";
          }
        })
        .catch(function (result) {
          ns.logError("Stripe confirmCardPayment failed");
          ns.logError(result);
        });
    },
    close: function () {
      this.$emit("close");
    }
  }
});

//# sourceURL=/static/js/stripeloader.js
