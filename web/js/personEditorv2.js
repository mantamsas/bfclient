"use strict";

ns.emptyPerson = {
  label: "",
  email: "",
  phone: "",
  fax: "",
  misc: "",
  address: {
    name: "",
    street1: "",
    street2: "",
    vat_id: "",
    misc: "",
    postal_code: {
      city: "",
      country: "FR"
    }
  }
};

Vue.component("person-email-display", {
  props: {
    "value": Object
  },
  template: `
    <div v-if="value.email">
        <template v-if="value.label">{{value.label}} </template>&lt;{{value.email}}&gt;
    </div>
    <div v-else>
      {{'fillLater' | localize}}
    </div>
  `
});

Vue.component("person-display", {
  props: {
    "value": Object
  },
  template: `
    <div v-if="value && !personIsEmpty(value)">
        <template v-if="value.address">
            <div v-if="value.address.name">{{value.address.name}}</div>
            <div v-if="value.address.street1">{{value.address.street1}}</div>
            <div v-if="value.address.street2">{{value.address.street2}}</div>
            <template v-if="value.address.postal_code">
              <div>{{value.address.postal_code.code}} {{value.address.postal_code.city}} ({{value.address.postal_code.country}})</div>
            </template>
            <div v-if="value.address.misc" v-html="nb2br(value.address.misc)"></div>
        </template>
        <div v-if="value.label">{{value.label}}</div>
        <div v-if="value.email">{{value.email}}</div>
        <div v-if="value.phone">{{value.phone}}</div>
        <div v-if="value.misc" v-html="nb2br(value.misc)"></div>
    </div>
    <div v-else>
      {{'fillLater' | localize}}
    </div>
  `,
  methods: {
    personIsEmpty: function (person) {
      return !person || (!person.label && !person.phone && !person.email && !person.fax && !person.misc
        && this.addressIsEmpty(person.address));
    },

    addressIsEmpty: function (address) {
      return !address || (!address.name && !address.street1 && !address.street2
        && this.pcIsEmpty(address.postal_code));
    },

    pcIsEmpty: function (pc) {
      return !pc || (!pc.city && !pc.code);
    },

    nb2br: function (intxt) {
      if (!intxt) return '';
      return intxt.replace(/(?:rn|r|n)/g, '<br/>');
    },
  }
});

Vue.component("person-picker", {
  props: [
    "value",
    "for",
    "contact_id",
    "org_id",
    "opened",
    "window_title",
    "readonly",
    "winstyle",
    "connected",
    "explaination"
  ],
  template: `
    <div :id="compId">
        <div @click.prevent="clickOpen" :class="{personDisplay : true, edited : !readonly}" tabindex="0">
        <slot></slot>
        </div>
        <popup id="personWindow" :showw="is_popup_opened" :windowtitle="window_title" @close="close" :winstyle="winstyle"
            ref="popup">
            <div class="search" style="display: inline-block" v-if="show_search">
                <input type="text" v-model="searchQuery" @keyup="dosearch" style="width: inherit"/><i class="fa fa-search"></i>
            </div>

            <div class="topButtons">
                <a href="#" @click.prevent="newAddress" v-if="newAllowed || !connected" class="bt"><i class="fa fa-user-plus"></i></a>
                <a href="#" @click.prevent="fillLater" v-if="delAllowed" class="bt"><i class="fa fa-user-times"></i></a>
            </div>
            <div v-if="explaination" v-html="explaination" style="margin: 1em 0"></div>
            <div class=".feedback"></div>
            <div class="form" v-if="showForm">
                <fieldset class="address">
                    <input type="text" v-model="edited.address.name" :placeholder="placeholder('address-name')"/><br/>
                    <input type="text" v-model="edited.address.street1" :placeholder="placeholder('address-street1')"/><br/>
                    <input type="text" v-model="edited.address.street2" :placeholder="placeholder('address-street2')"/><br/>
                    <input type="text" v-model="edited.address.postal_code.code" :placeholder="placeholder('postal_code-code')" id="postal_code-code"/><input type="text" v-model="edited.address.postal_code.city" :placeholder="placeholder('postal_code-city')" id="postal_code-city"/><br/>
                    <select v-model="edited.address.postal_code.country" :placeholder="placeholder('country')">
                        <option v-for="(value, name) in countries" :value="name">{{value}}</option>
                    </select>
                    <input type="text" v-model="edited.address.vat_id" :placeholder="placeholder('address-vat-id')"/><br/>
                    <textarea v-model="edited.address.misc" rows="3" :placeholder="placeholder('address-misc')"></textarea>
                </fieldset>
                <fieldset class="person">
                    <input type="text" v-model="edited.label" :placeholder="placeholder('person-label')"/><br/>
                    <input type="text" v-model="edited.email" :placeholder="placeholder('person-email')"/><br/>
                    <input type="text" v-model="edited.phone" class="half" :placeholder="placeholder('person-phone')"/><br/>
                    <input type="text" v-model="edited.fax" class="half" :placeholder="placeholder('person-fax')"/><br/>
                    <textarea v-model="edited.misc" rows="3" :placeholder="placeholder('person-misc')"></textarea>
                </fieldset>
                <div class="buttons">
                    <input class="actbt mainact" type="submit" @click.prevent="submit"/>
                </div>
            </div>
            <div class="results" v-else>
                <div v-for="(result, index) in searchResults" :class="['result','personDisplay',result.type ?  'extAddr' : 'intAddr']" @click.prevent="select(result)">
                    <div style="float:right; font-weight: normal">{{result.person.updated | dateFormat}}</div>
                    <template v-if="result.person.address">
                        <div v-if="result.person.address.name">{{result.person.address.name}}</div>
                        <div v-if="result.person.address.street1">{{result.person.address.street1}}</div>
                        <div v-if="result.person.address.street2">{{result.person.address.street2}}</div>
                        <div v-if="result.person.address.postal_code">{{postalDisplay(result.person.address.postal_code)}}</div>
                        <div v-if="result.person.address.misc">{{result.person.address.misc}}</div>
                    </template>
                    <div v-if="result.person.label">{{result.person.label}}</div>
                    <div v-if="result.person.email">{{result.person.email}}</div>
                    <div v-if="result.person.phone">{{'phoneAbrev' | localize}}{{result.person.phone}}</div>
                    <div v-if="result.person.fax">{{'faxAbrev' | localize}}{{result.person.fax}}</div>
                    <div v-if="result.person.misc">{{result.person.misc}}</div>
                </div>
                <div v-show="(!searchResults || searchResults.length == 0) && !noresultsFound" class="result">{{'noResultsSearch' | localize}}</div>
                <div v-show="(!searchResults || searchResults.length == 0) && noresultsFound" class="result">{{noresultFor}}</div>
            </div>  
        </popup>
    </div>
    `,
  data: function () {
    return {
      editing: false,
      showForm: false,
      showSearch: true,
      newAllowed: true,
      delAllowed: true,
      edited: ns.emptyPerson,
      feedback: [],
      searchQuery: "",
      noresultsFound: false,
      searchResults: [],
      countries: loc.countries
    };
  },
  methods: {
    openEditing: function () {
      console.log("Open editing");
      ns.resetMessages(this.ajaxOpt);
      if (this.readonly) {
        return;
      }

      if (!this.connected) {
        this.newAddress();
        return;
      }
      if (this.for == 'sign') {
        this.showForm = false;
        this.newAllowed = false;
        this.delAllowed = false;
        this.dosearch();
      } else if (this.for == 'identity') {
        this.edited = ns.cloneObject(this.value);
        this.paddPerson(this.edited);
        this.showForm = true;
        this.newAllowed = false;
        this.delAllowed = false;
      } else if (this.value) {
        this.edited = ns.cloneObject(this.value);
        this.paddPerson(this.edited);
        this.showForm = true;
      }else {
        this.edited = ns.emptyPerson;
        this.showForm = false;
      }
    },
    newAddress: function () {
      this.edited = ns.emptyPerson;
      this.showForm = true;
    },
    fillLater: function () {
      this.$emit("change", null);
      this.edited = ns.emptyPerson;
      this.editing = false;
    },
    postalDisplay: function (postalObj) {
      return (
        (postalObj.code ? postalObj.code + " " : "") +
        (postalObj.city || "") +
        (postalObj.country ? " (" + postalObj.country + ")" : "")
      );
    },
    dosearch: function (event) {
      var that = this,
        val = event && event.target.value;

      if (this.for !== 'sign' && (!val || val.length < 3)) {
        return;
      }

      var params = new URLSearchParams();
      if (this.for)
        params.append('for', this.for);
      if (val)
        params.append('s', val);

      ns.ws(
        "GET",
        "v1/person?" + params.toString(),
        null,
        this.ajaxOpt
      ).then(function (result) {
        console.log("Searching address with " + val + ": ");
        console.log(result);

        that.newAllowed = true;
        that.searchResults = [];
        that.showForm = false;
        that.searchResults = result.persons;

        var ggContactLen =
          result.googleContacts && result.googleContacts.length;
        if (ggContactLen > 0) {
          ns.logEvent("Integration", "gotGoogleContact", null, ggContactLen);
          for (var rowIdx = 0; rowIdx < ggContactLen; rowIdx++) {
            var person = result.googleContacts[rowIdx];
            that.searchResults.push({ person: person, type: "google" });
          }
        }
      });
    },

    noresultFor: function () {
      return loc.noResultsSearch + this.searchQuery;
    },

    select: function (result) {
      var that = this;

      if (result.person && result.person.id) {
        that.edited = ns.cloneObject(result.person);
        that.$emit("change", result);
        that.editing = false;
      } else {
        // Save selected person if it has no id
        var posted = {};
        posted.person = ns.cloneObject(result.person);
        delete posted.person.address;
        posted.address = this.edited.address;
        posted.validationUrl = window.location.href;
        posted.org = { id: this.org_id };
        posted.contact = { id: this.contact_id };

        this.save(posted);
      }
    },

    submit: function () {
      var posted = {};
      posted.person = ns.cloneObject(this.edited);
      posted.validationUrl = window.location.href;
      posted.org = { id: this.org_id };
      posted.contact = { id: this.contact_id };

      // Remove ids to force resolution
      var p = posted.person,
        a = p.address;
      delete p.id;
      delete p.address_id;
      delete a.id;
      delete a.postal_code_id;
      delete a.postal_code.id;
      this.save(posted);
    },

    save: function (posted) {
      var that = this;
      ns.ws(
        "POST",
        "v1/person" + (this.for ? "?for=" + this.for : ""),
        posted,
        this.ajaxOpt
      ).then(function (result) {
        console.log("Created person");
        console.log(result);

        if (result.persons && result.persons.length > 0) {
          var person = result.persons[0].person;
          that.edited = ns.cloneObject(person);
          that.paddPerson(that.edited);
          that.$emit("change", result );
          that.editing = false;
        } else {
          ns.showMessages(result, that.ajaxOpt);
        }
      }).catch(function(result){
        ns.showMessages(result, that.ajaxOpt);
      });
    },

    paddPerson: function (person) {
      if (!person.address) {
        person.address = {};
      }
      if (!person.address.postal_code) {
        person.address.postal_code = {};
      }
    },

    clickOpen: function(){
      if(this.is_popup_opened) return;
      this.editing = true;
    },

    close: function () {
      this.editing = false;
      this.$emit('close');
    },

    placeholder: function (key) {
      return loc.placeholder[key];
    }
  },
  watch: {
    'is_popup_opened' : function(newVal){
      console.log("is_popup_opened changed");
      this.openEditing();
    }
  },
  computed: {
    compId: function () {
      return "person-picker-" + (this.$vnode.key || this.for);
    },
    ajaxOpt: function () {
      var tid = "#" + this.compId;
      return {
        feedback: tid + " .feedBack",
        indicator: tid + " .ajax-indicator"
      };
    },
    is_popup_opened: function () {
      
      var res = this.editing || this.opened;  
      return res;
    },
    show_search: function(){
      return this.connected && this.for != 'identity';
    }
  }
});
