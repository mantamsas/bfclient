ns['login'] = {
  template: "#loginComp",
  props: {
    showw: Boolean,
    firstAction: String
  },
  data: function () {
    return {
      action: 'log',
      loading: false,
      messages: [],
      alreadyExists: false,
      notExists: false,
      agreeLicence: false,
      licenceWarn: false,
      noThanks: false,
      login: '',
      password: '',
      adminPerson: {}
    };
  },
  mounted: function () {
    this.action = firstAction;
    console.log("Login loaded");
    ns.logEvent('Session', 'open' + this.action, 'Open '+this.action+' form')
  },
  methods: {
    trigger_action: function () {

      var apiAction = null,
        req = {
          login: this.login,
          password: this.password,
          persistent: true,
          validationUrl: window.location.origin + ns.locDir() + 'id-settings.html',
        };

      if (this.adminPerson) {
          req.adminPerson = this.adminPerson;
      }

      if (this.action == 'reg') {
        if (!this.agreeLicence) {
          this.licenceWarn = true;
          return;
        }
        apiAction = 'register';
        if (formOptions && formOptions.adminPerson) {
          req.adminPerson = formOptions.adminPerson;
        }
      } else if (action == 'pw') {
        apiAction = 'passwordRecall';
      } else {
        apiAction = 'login';
      }
      ns.logEvent('Session', 'submit' + apiAction, 'Submit ' + apiAction);

      ns.ws('POST', 'v1/session?action=' + apiAction, req, {
        feedback: '#signInWindow .feedBack',
        indicator: '#signInWindow .ajax-indicator'
      })
        .then(function (result) {
          if (this.action == 'pw') {
            // Display message and stay on page
          } else if (this.action == 'reg') {
            if (window.onRegister) {
              window.onRegister(result);
            } else if (window.onLogin) {
              window.onLogin(result);
            }
          } else {
            if (window.onLogin) {
              window.onLogin(result);
            }
          }
        }).catch(function (result) {
          ns.logEvent('Session', 'submitError', 'Submit Error');
          ns.logError("session query error " + apiAction);
        });
    },

    oauth: function (system) {
      if (this.action == 'reg') {
        if (!this.agreeLicence) {
          this.licenceWarn = true;
          return;
        }
      }
      var param = {
        action: this.action
      };

      if (this.adminPerson) {
        req.adminPerson = this.adminPerson;
      }

      ns.logEvent('Session', system + 'oauthSubmit', system + ' OAuth submit');

      param.nextUrl = ns.locDir() + 'messages_list.html';
      ns.oauthSignIn(system, param);
    }
  },

  noThanksAction: function() {
    ns.goHome();
  }
};

//# sourceURL=/static/js/loginv.js
