"use strict";

var getMatchedCSSRules = (el, css = el.ownerDocument.styleSheets) =>
    [].concat(...[...css].map(s => [...s.cssRules || []])) /* 1 */
        .filter(r => el.matches(r.selectorText));

/*
Load template : from ID
    WS
    Get body
    Put body 
*/

(function () { //IIFE

    // put a click handler on each element of the template
    const element_editor = document.getElementById('element_editor')
    const element_name = document.getElementById('name')
    const styleEl = document.getElementById('style')

    const template_body = document.getElementById('template_body')
    let selectedElement, templateStyle, templateId

    template_body.addEventListener('click', selectElement, true)

    function selectElement(event) {

        /*if (selectedElement)
            selectedElement.style.border = selectedElement.original_border ? selectedElement.original_border : '';

        selectedElement = selectedElement == event.target ? event.target.parentNode : event.target
        selectedElement.style.border = '1px solid red'

        element_name.innerHTML = selectedElement.id || selectedElement.className
        styleEl.innerHTML = ''
        for (let i = 0; i < templateStyle.sheet.cssRules.length; i++) {
            let r = templateStyle.sheet.cssRules[i]
            if (selectedElement.matches(r.selectorText))
                styleEl.innerHTML += ("\n" + r.cssText)
        }*/
    }

    function loadTemplate(templateId) {
        return ns.ajax('GET', ns.serverUrl + 'api/v1/document_template?includeLogo=true&templateId=' + templateId, 'html', null, {creds: true})
            .then((res) => {
                template_body.innerHTML = res.responseText;
                console.log("Template loaded");

                templateStyle = document.querySelector('#template_body style')
                styleEl.value = formatCSS(templateStyle.innerHTML);

                styleEl.addEventListener('change', () =>{
                    templateStyle.innerHTML = formatCSS(styleEl.value);
                })
            });
    }

    function formatCSS(input) {
        return input.replaceAll(/\s/g, ' ').replaceAll(/}/g, '\n}\n\n').replace(/{/g, ' {\n\t').replace(/;/g, ';\n\t');
    }

    document.getElementById('saveTemplateBt').addEventListener('click', (event) => {
        event.preventDefault();
        let url = ns.serverUrl + 'api/v1/document_template'
        if(templateId)
            url += ('?templateId=' + templateId);

        // Remove logo data
        let bodyClone = template_body.cloneNode(true);
        let logoElement = bodyClone.querySelector("#logo");
        if(logoElement)
            logoElement.src = null;

        return ns.ajax('POST', url, 'html', bodyClone.innerHTML, {creds: true})
            .then((res) => {
                console.log("Template saved");
            });
    });

    document.getElementById('plusBt').addEventListener('click', (event) => {
        event.preventDefault();
        let z = template_body.style.zoom;
        template_body.style.zoom = z * 1.1;
    });

    document.getElementById('minusBt').addEventListener('click', (event) => {
        event.preventDefault();
        let z = template_body.style.zoom;
        template_body.style.zoom = z * 0.9;
    });

    const urlParams = new URLSearchParams(window.location.search);
    templateId= urlParams.get('template_id');
    if(templateId)
        loadTemplate(templateId);
})();