"use strict";

ns.eceditor = {};

ns.createVueComp("sending");
ns.createVueComp("catalog-picker");
ns.createVueComp("payment-editor");

Vue.component("usual-mentions", function (resolve) {
  ns.ajax("GET", "usual-mentions.js", "js").then(function (result) {
    resolve(eval(result.response));
  });
});

Vue.component("menu-item", {
  props: ["item", "level"],
  template: `\
  <div>
    <a href="#" @click.prevent="click"
      ><i :class="[icon(item.type)]"></i
      ><span class="lbl">{{item.label}}</span></a
    >
    <div :class="'n'+1" v-if="opened && item.children">
      <menu-item v-for="(child, index) in item.children" :key="child.key" :item="child" :level="level+1" @menu_click="propagate($event)"></menu-item>
    </div>
  </div>
  `,
  data: function () {
    return {
      opened: false,
    };
  },
  methods: {
    click: function () {
      if (this.item.children && this.item.children.length > 0) {
        this.opened = !this.opened;
      } else {
        this.$emit("menu_click", this.item);
      }
    },
    propagate: function (val) {
      this.$emit("menu_click", val);
    },
    icon: function (ttype) {
      return ns.icon[ttype];
    },
  },
});

var emptyMessage = {
  message_number: "",
  message_type: "",
  content_type: "",
  subject: "",
  issue_date: "",
  due_date: "",
  currency: "EUR",
  lines: [{ item: {}, quantity: "", subTotal: 0, tax_rate: 0, vat: 0 }],
  subTotal: 0,
  paidAmount: "",
  sender_reference: "",
  recipient_reference: "",
  action: [],
  roles: {},
  lang: "",
  summary_note: ''
};

ns.eceditor.vue = new Vue({
  el: "#messageEditor",
  data: {
    message: emptyMessage,
    possibleActions: [],
    persons: {},
    users: {},
    roles: { sender: {}, recipient: {} },
    senderEditing: false,
    recipientEditing: false,
    urlParams: {},
    loading: true,
    selectCol: false,
    lineSelection: [],
    openLineTool: -1,
    connected: false,
    readonly: true,
    UoMColumn: false,
    itemRefColumn: false,
    status: loc.linkStatus.loading,
    currencyPrecision: 2,
    lastQuery: "",
    mentionOpenned: false,
    mentionsContent: "",
    historyOpened: false,
    catalogOpened: false,
    cancelOpened: false,
    paymentEditorOpened: false,
    catLineIndex: -1,
    sendingLoaded: false,
    editIssueDate: "",
    generateNumOnIssue: "",
    editMessageNumber: "",
    advance: "",
    due_date: "",
    issue_date: "",
    more_options: false,
  },

  mounted: function () {
    // Get parameters and load
    this.urlParams = ns.getQueryParams();
    this.queryMessage(this.urlParams);
  },

  methods: {
    // Query message from server
    queryMessage: function (params) {
      // init page
      var messageId = params.message_id || params.messageId || "new",
        url = "v1/message/" + messageId + "?loadAddress=true";

      if (
        (messageId == "new" || params.action == "duplicate") &&
        !params.content_type
      ) {
        params.content_type = "commercial_message";
      }

      url += ns.params2Url(params);

      this.loading = true;
      this.status = loc.linkStatus.loading;
      var that = this;
      return ns
        .ws("GET", url)
        .then(function (result) {
          try {
            that.loadMessage(result);
            that.status = "";
            if (params.action) {
              that.message.links = [
                { source_id: messageId, type: params.action },
              ];
            }
          } catch (ex) {
            console.log(ex);
            that.status = loc.linkStatus.error;
          }
          that.loading = false;
        })
        .catch(function (xhr) {
          that.loading = false;
          that.status = loc.linkStatus.error;

          var result = xhr.jsonResp;
          if (!result) {
            console.error(
              "Query message error result no valid response " +
                (xhr.response || xhr.responseText)
            );
          } else if (result.registerAndValidateUser) {
            ns.requires("registerProposal", "registerProposal.js").then(
              function () {
                ns.registerProposal.load(
                  result.senderPerson,
                  result.registerAndValidateUser
                );
              }
            );
          } else if (result.proposedLogin) {
            $(window).openLogin("log", {
              adminPerson: { email: result.proposedLogin },
            });
          }
        });
    },

    // Load data into view
    loadMessage: function (data) {
      ns.setAuthUi(data.connected);

      // Set data
      for (var prop in data) {
        if (data.hasOwnProperty(prop)) {
          try {
            this[prop] = data[prop];
          } catch (e) {
            console.log("Index " + i);
            console.log(e);
          }
        }
      }
      this.roles.sender = this.getRole("sender");
      this.roles.recipient = this.getRole("recipient");

      // Lines
      var lines = this.message.lines || [],
        linesNb = (lines && lines.length) || 0;
      for (var i = 0; i < linesNb; i++) {
        var line = lines[i];
        if (!line.item) {
          this.$set(line, "item", {});
        }
        this.rekonLine(line);
      }

      // Define if read-only
      if (data.modifiable === false) {
        this.readonly = true;
      } else {
        var ro = false,
          nb =
            (data.message &&
              data.message.actions &&
              data.message.actions.length) ||
            0;

        for (var i = 0; i < nb; i++) {
          var act = data.message.actions[i];
          if (act.canceledby) continue;
          if (
            act.type == "ISSUED" ||
            act.type == "CLOSED" ||
            act.type == "TRASH" ||
            act.type == "DELETED"
          ) {
            ro = true;
            break;
          }
        }
        this.readonly = ro;
        if (!this.readonly) this.checkBlanklines();
      }

      // Create payment menu if needed
      var stripeUserId = false;
      if (data.possibleActions)
        for (var i = 0; i < data.possibleActions.length; i++) {
          if (
            data.possibleActions[i].key == "pay" &&
            data.possibleActions[i].params
          ) {
            stripeUserId = data.possibleActions[i].params.stripe_user_id;
            break;
          }
        }

      if (stripeUserId) {
        var that = this;
        ns.stripeloader
          .load({ stripeAccount: stripeUserId })
          .then(function () {
            return ns.loadhtml("inc/stripe.html");
          })
          .then(function () {
            ns.stripeloader.initvue(
              "#paymentPopup",
              "messageId=" + data.message.id,
              {
                amount: that.toPay,
                currency: data.message.currency,
                payee:
                  data.persons && data.persons[data.message.roles.sender]
                    ? data.persons[data.message.roles.sender].label
                    : "",
                doc: data.message.message_number,
              }
            );
          });
      }

      if (!this.readonly) {
        // Show defined advance of calculate it
        if (typeof data.message.paidAmount == "undefined") {
          if (data.defaultAdvance) {
            //calculate future advance
            if (data.defaultAdvance.endsWith("%")) {
              this.advance = ns.round(
                (data.defaultAdvance.substr(0, data.defaultAdvance.length - 1) *
                  this.totals.total) /
                  100,
                this.currencyPrecision
              );
            } else {
              this.advance = data.defaultAdvance;
            }
          }
        } else {
          this.advance = data.message.paidAmount;
        }
        // Make this prop reactive
        Vue.set(this.message, "paidAmount", this.message.paidAmount || 0);

        // Show due date if empty and calculated
        if (typeof data.message.issue_date == "undefined") {
          this.issue_date = Math.round(Date.now() / 1000);
        } else {
          this.issue_date = data.message.issue_date;
        }

        // Show due date if empty and calculated
        if (typeof data.message.due_date == "undefined") {
          if (data.defaultDueDate) {
            this.due_date = data.defaultDueDate;
          }
        } else {
          this.due_date = data.message.due_date;
        }
      } else {
        this.issue_date = data.message.issue_date;
        this.due_date = data.message.due_date;
        this.advance = data.message.paidAmount;
      }
    },

    updateUrl: function () {
      // Update URL if needed
      var params = this.urlParams;
      params.messageId = this.message.id;
      if (params.action == "duplicate") {
        delete params.action;
      }
      delete params.message_type;
      delete params.content_type;
      delete params.priceVatIncluded;
      var qstring = "?" + ns.params2Url(params).substr(1);
      if (window.location.search != qstring) {
        window.history.replaceState(
          null,
          null,
          window.location.pathname + qstring
        );
      }
    },

    rekonLine: function (line) {
      var docPrec = this.currencyPrecision,
        linePrec = line.item.unit_price
          ? ns.getPrecisionOf(line.item.unit_price)
          : 2,
        prec =
          typeof docPrec === "undefined" || docPrec == -1 ? linePrec : docPrec,
        qte = line.quantity,
        up = line.item.unit_price,
        tr = line.tax_rate;

      // Fix vat rate
      line.tax_rate =
        line.tax_rate > 1000
          ? 1000
          : line.tax_rate < -1000
          ? -1000
          : line.tax_rate;

      if (this.message.priceVatIncluded && tr) {
        line.subTotal = ns.round(
          (up * qte) / (1 + tr / 100),
          docPrec == -1 ? prec + tr.toString().length : prec
        );
        this.$set(line, "vat", up * qte - line.subTotal);
      } else {
        this.$set(line, "subTotal", ns.round(qte * up, prec));
        this.$set(
          line,
          "vat",
          tr ? ns.round((line.subTotal * tr) / 100, prec) : 0
        );
      }
    },

    addLine: function (index) {
      if (!this.message.lines) {
        this.message.lines = [];
      }
      var newL = this.message.lines[0]
        ? ns.cloneObject(this.message.lines[0])
        : { item: {} };
      newL.quantity = 1;
      newL.item.reference = "";
      newL.item.description = "";
      newL.item.unit_price = "";
      newL.item.id = null;
      newL.item.unit_price = 0;
      newL.id = null;
      newL.subTotal = 0;
      newL.vat = 0;
      if (typeof index == "undefined" || index === null || index < 0) {
        this.message.lines.push(newL);
      } else {
        this.message.lines.splice(index, 0, newL);
      }
    },

    // Add up to 3 blank lines for edition if needed
    checkBlanklines: function () {
      if (this.readonly) return;

      var lines = this.message.lines || [],
        linesNb = lines.length;
      if (linesNb < 3 || !this.isBlank(lines[linesNb - 1])) {
        var toAdd = 3 - linesNb;
        if (toAdd < 1) toAdd = 1;
        for (var i = 0; i < toAdd; i++) this.addLine();
      }
    },

    trimEmptyLines: function (lines) {
      var newLines = [],
        linesNb = lines ? lines.length : 0,
        lineIdx = 0;

      for (var i = 0; i < linesNb; i++) {
        var line = lines[i];
        if (!this.isBlank(line)) {
          line.line_number = lineIdx++;
          line.id = null;
          line.item.id = null;
          newLines.push(line);
        }
      }
      return newLines;
    },

    isBlank(line) {
      return !line.item || (!line.item.description && !line.item.reference);
    },

    formatMoney: function (value) {
      return value ? (value.formatMoney ? value.formatMoney() : value) : "";
    },

    changeCurrency: function (currency) {
      this.message.currency = currency.code;
      this.currencyPrecision = currency.dec;
    },

    getRole: function (role) {
      var pId = this.message.roles && this.message.roles[role];
      return pId && this.persons[pId];
    },

    changeRole: function (role, event) {
      if(event){
        var person = event.person || (event.persons && event.persons[0]);
        person = person && person.person || person;
      }

      var pId = person && person.id || null
      
      this.message.roles[role] = pId || person;
      if (pId) {
        this.persons[pId] = person;
        this.roles[role] = person;
      } else if (!person) {
        this.roles[role] = null;
      }
    },

    openMentions: function () {
      this.mentionOpenned = true;
    },

    pasteMention: function (event) {
      var m = this.message;
      m.summary_note = (m.summary_note || "") + "<p>" + event + "</p>\n";
      ns.eceditor.htmleditor.trumbowyg("html", m.summary_note);
    },

    saveMessage: function () {
      console.log("Saving message");

      this.status = loc.linkStatus.saving;

      var that = this,
        savmess = ns.cloneObject(this.message),
        theUrl = "v1/message" + (savmess.id ? "/" + savmess.id : "");

      savmess.time_zone = Intl.DateTimeFormat().resolvedOptions().timeZone;
      savmess.lines = this.trimEmptyLines(savmess.lines);

      return new Promise(function (resolve, reject) {
        ns.ws("POST", theUrl, savmess)
          .then(function (result) {
            that.status = loc.linkStatus.saved;

            setTimeout(function () {
              that.status = "";
            }, 1000); // Blank after 5s

            resolve(result);
          })
          .catch(function (xhr) {
            if (xhr.status == 401) {
              that.proposeRegister();
            } else {
              that.status = loc.linkStatus.saveError;
              ns.logError("Error saving message: " + (xhr && xhr.responseText));
              reject(xhr);
            }
          });
      });
    },

    proposeRegister: function (result) {
      var anonmessage = ns.cloneObject(this.message);
      anonmessage.lines = this.trimEmptyLines(anonmessage.lines);

      anonmessage.roles.sender = this.getRole("sender");
      anonmessage.roles.recipient = this.getRole("recipient");
      var admin = anonmessage.roles.sender;

      ns.setLocalPref("anonmessage", anonmessage, true);
      window.location.href =
        ".//v/register/" +
        (admin && admin.email ? "?email=" + admin.email : "");
    },

    menuclick: function (event) {
      console.log(event);
      var func = this[event.key + "Act"];
      if (func) {
        func();
      } else {
        var message = $(window).data("message"),
          that = this;
        ns.ws("POST", "v1/message/" + this.message.id + "/" + event.key).then(
          function (result) {
            if (result.message) {
              that.loadMessage(result);
            }
          }
        );
        console.log("Action " + event.key + " not found, posting action");
      }
    },

    createInvoiceAct: function () {
      ns.logEvent("Document", "toInvoice");
      var popup = window.open();
      this.saveAndMessWindow(popup, "INVOICE");
    },

    advanceInvoiceAct: function () {
      ns.logEvent("Document", "paymentInvoice");
      var popup = window.open();
      if(this.advance) {
        var url = window.location.toString();
        url = ns.setUrlParam(url, "totalValue", this.advance);
        url = ns.setUrlParam(url, "linkType", "ADVANCE_PAYMENT");
        this.saveAndMessWindowUrl(popup, url);
      } else {
        this.saveAndMessWindow(popup, "INVOICE");
      }
    },

    createPOAct: function () {
      ns.logEvent("Document", "toPurchaseOrder");
      var popup = window.open();
      this.saveAndMessWindow(popup, "PURCHASE_ORDER");
    },

    createQuoteAct: function () {
      ns.logEvent("Document", "toQuote");
      var popup = window.open();
      this.saveAndMessWindow(popup, "QUOTE");
    },

    // Close document
    closingAct: function () {
      var that = this;
      ns.ws("PUT", "v1/message/" + this.message.id + "/close").then(function (
        result
      ) {
        if (result.message) {
          result.action = "view";
          that.loadMessage(result);
        } else {
          alert("No message data on closing");
        }
      });
    },

    addPaymentAct: function () {
      this.paymentEditorOpened = true;
    },

    cancelInvoiceAct: function () {
      this.cancelOpened = true;
    },

    submitCancel: function () {
      ns.logEvent("Document", "toCreditNote");
      var popup = window.open();
      this.saveAndMessWindow(popup, "CREDIT_NOTE");
      this.cancelOpened = true;
    },

    // Get data and submit post to autorization server
    payAct: function () {
      //ns.stripeloader.ensureHttps();
      ns.stripeloader.vue.showw = true;
    },

    // Close window
    closeAct: function () {
      if (this.readonly) {
        ns.goHome();
      } else {
        this.saveMessage().then(function () {
          ns.goHome();
        });
      }
    },

    saveAct: function () {
      var that = this;
      this.saveMessage().then(function (result) {
        that.loadMessage(result);
        that.updateUrl();
      });
    },

    modifyAct: function () {
      this.readonly = false;
    },

    historyAct: function () {
      this.historyOpened = true;
    },

    sendAct: function () {
      this.sendingLoaded = true;
    },

    trashAct: function () {
      this.delete(true);
    },

    deleteAct: function () {
      this.delete(false);
    },

    delete: function (onlyTrash) {
      if (ns.isAuth() && this.message && this.message.id) {
        ns.ws(
          "DELETE",
          "v1/message/" + this.message.id + (onlyTrash ? "?onlyTrash=true" : "")
        ).then(function () {
          ns.goHome();
        });
      } else {
        alert(loc.shouldBeAuth);
      }
    },

    duplicateAct: function () {
      ns.logEvent("Document", "duplicate");
      var popup = window.open();
      this.saveAndMessWindow(popup, this.message.message_type);
    },

    printPdfAct: function () {
      ns.logEvent("Document", "pdf");
      var that = this;

      var fileName = that.message.message_number || "commercialMessage";
      if (ns.isAuth()) {
        if (this.readonly && this.message.id) {
          ns.newWindow(
            ns.serverUrl +
              "pdf-docm/mId-" +
              that.message.id +
              "/" +
              fileName +
              ".pdf",
            "_new",
            "scrollbars=yes"
          );
        } else {
          this.saveMessage()
            .then(function (result) {
              that.loadMessage(result);
              that.updateUrl();
            })
            .then(function (result) {
              ns.newWindow(
                ns.serverUrl +
                  "pdf-docm/mId-" +
                  that.message.id +
                  "/" +
                  fileName +
                  ".pdf",
                "_new",
                "scrollbars=yes"
              );
            });
        }
      } else {
        var init = function (win) {
          var bdy = win.document.body;
          bdy.innerHTML =
            '<form id="postPdf" method="POST" target="pdfWindow" action="' +
            ns.serverUrl +
            "pdf-docm/" +
            fileName +
            '.pdf"><input type="hidden" name="data" id="data" value=""/><input type="submit"/></form>';
          var form = bdy.querySelector("form");
          form
            .querySelector("#data")
            .setAttribute("value", JSON.stringify(that.message));
          form.submit();
        };
        var postPdf = ns.newWindow(
          ns.clientUrl + "blank.html",
          "pdfWindow",
          "status=0,title=0,height=600,width=800,scrollbars=1"
        );
        if (postPdf.document.readyState === "complete") {
          init(postPdf);
        } else {
          postPdf.addEventListener("load", function () {
            init(this);
          });
        }
      }
    },

    cancelAction: function (action, actionIndex) {
      var that = this;
      ns.ws("PUT", "v1/messageAction/" + action.id + "/cancel", null, {
        feedback: "#historyWindow .feedBack",
        handleError: true,
      }).then(function (result, options) {
        that.loadMessage(result);
        if (result.cancelled_action) {
          that.message.actions[actionIndex] = result.cancelled_action;
          that.message.actions.push(result.canceling_action);
        }

        console.log("MessageAction " + action.id + " canceled");
      });
    },

    personLabel: function (pObj) {
      var personStr = pObj.label;
      if (personStr && pObj.email) {
        personStr += " (" + pObj.email + ")";
      } else if (pObj.email) {
        personStr = pObj.email;
      } else if (pObj.address && pObj.address.name) {
        personStr = pObj.address.name;
      }
      return personStr;
    },

    actionActor: function (act) {
      var personStr;
      if (act.person && this.persons[act.person]) {
        personStr = this.personLabel(this.persons[act.person]);
      } else if (act.user && this.users[act.user]) {
        personStr = this.users[act.user];
      } else {
        personStr = "person:" + act.person + " user:" + act.user;
      }
      if (act.isConnectedUser) {
        personStr += " (" + loc.yourself + ")";
      }
      return personStr;
    },

    historyLabel: function (action) {
      var label = loc.history[action.type];
      return label && label.replace("{0}", this.actionActor(action));
    },

    historyPaymentLabel: function (pay) {
      var texte = ns.formatAmount(pay.amount);
      if (loc.paymentTypes[pay.paymentType]) {
        texte += " " + loc.paymentTypes[pay.paymentType];
      }
      if (pay.description) {
        texte += " " + pay.description;
      }
      return texte;
    },

    sendingPaymentLabel: function (snd) {
      var label = loc.history.sending[snd.type] || snd.type;
      if (snd.person && this.persons[snd.person]) {
        var sp = this.persons[snd.person];
        label = label.replace("{0}", this.personLabel(sp));

        if (snd.type == "EMAIL" || snd.type == "OTHER") {
        } else if (snd.type == "FAX") {
          label = label.replace("{1}", sp.fax);
        } else if (snd.type == "POST") {
          label = label.replace(
            "{1}",
            sp.organisation.address + " " + sp.organisation.postal_code.city
          );
        } else if (snd.type == "SMS") {
          label = label.r | eplace("{1}", sp.phone);
        } else {
          label = label.replace("{1}", "#ERROR#");
        }
      } else if (!snd.person) {
        label = label.replace("{0}", '"' + loc.someone + '"');
      } else {
        label = label.replace("{0}", "person:" + snd.person);
        label = label.replace("{1}", "#ERROR#");
      }
      if (snd.comment) {
        label += "<br/>" + snd.comment;
      }
      return label;
    },

    sendErrorLabel: function (snd) {
      var errorMessage = loc.smtpStatus["generalError"];
      switch (snd.errorCode) {
        case 452:
        case 522:
          errorMessage = loc.smtpStatus["boxFull"];
          break;
        case 550:
          errorMessage = loc.smtpStatus["badAddress"];
          break;
        case 552:
          errorMessage = loc.smtpStatus["tooBig"];
          break;
        default:
          if (500 > snd.errorCode) {
            errorMessage = loc.smtpStatus["tempProblem"];
          }
      }
      return errorMessage;
    },

    cancelActionLabel: function (act) {
      var cancelAct = null,
        actLen = this.message.actions.length;
      for (var i = 0; i < actLen; i++) {
        var anAct = this.message.actions[i];
        if (act.canceledby == anAct.id) {
          cancelAct = anAct;
          break;
        }
      }
      if (!cancelAct) {
        return null;
      }
      var cancelDate = new Date(cancelAct.date * 1000).format(
        loc.date.dateFormat
      );
      var label = loc.history["ACTION_CANCELED"];
      label = label.replace("{0}", cancelDate);
      return label.replace("{1}", this.actionActor(cancelAct));
    },

    saveAndMessWindowUrl: function (theWindowObj, url) {
      if (ns.isAuth()) {
        if (!this.readonly) {
          var that = this;
          this.saveMessage()
            .then(function (result) {
              that.loadMessage(result);
              that.updateUrl();
            })
            .then(function () {
              theWindowObj.location = url;
            });
        } else {
          theWindowObj.location = url;
        }
      } else {
        alert(loc.shouldBeAuth);
      }
    },

    saveAndMessWindow: function (theWindowObj, message_type) {
      var url = window.location.toString();
      url = ns.setUrlParam(url, "action", "duplicate");
      url = ns.setUrlParam(
        url,
        "message_type",
        message_type ? message_type : "invoice"
      );
      this.saveAndMessWindowUrl(theWindowObj, url);
    },

    selectAll: function () {
      this.selectCol = true;
      var ls = this.message.lines,
        lLen = ls.length;
      for (var i = 0; i < lLen; i++) {
        if (!this.isBlank(ls[i])) {
          this.$set(this.lineSelection, i, true);
        }
      }
    },

    openTools: function (index) {
      this.openLineTool = this.openLineTool == index ? -1 : index;
    },

    top: function (index) {
      var ls = this.message.lines,
        theLine = ls.splice(index, 1)[0];
      ls.splice(0, 0, theLine);
    },

    bottom: function (index) {
      var ls = this.message.lines;
      ls.push(ls.splice(index, 1)[0]);
    },

    switchLine(src, dest) {
      var ls = this.message.lines;
      this.$set(ls, dest, ls.splice(src, 1, ls[dest])[0]);
    },

    up: function (index) {
      this.switchLine(index, index - 1);
    },

    down: function (index) {
      this.switchLine(index, index + 1);
    },

    treatIdx: function (inArr) {
      var inLen = inArr.length,
        out = [];
      for (var i = 0; i < inLen; i++) {
        var idx = inArr[i];
        if (!idx) {
          continue;
        }
        out.push(idx === true ? i : idx);
      }
      out.sort();
      return out;
    },

    delLine: function (indexes) {
      var idNb = indexes.length;
      for (var j = idNb - 1; j > -1; j--) {
        this.message.lines.splice(indexes[j], 1);
      }
    },

    storeCopied: function (data) {
      if (data && data.length > 0) {
        if (ns.isStorage(true)) {
          ns.setLocalPref("lines", JSON.stringify(data), true);
        } else {
          window.lines = data;
        }
      }
    },

    copy: function (indexes, successMsg) {
      var idNb = indexes.length,
        tocopy = [],
        lines = this.message.lines;
      for (var i = 0; i < idNb; i++) {
        var newLine = ns.cloneObject(lines[indexes[i]]);
        delete newLine.id;
        delete newLine.message_id;
        delete newLine.line_number;
        tocopy.push(newLine);
      }
      this.storeCopied(tocopy);
      ns.showMessages(
        { infos: [{ message: successMsg }] },
        { feedback: "#mainFeedBack" }
      );
    },

    cut: function (indexes, successMsg) {
      this.copy(indexes);
      this.delLine(indexes);
      ns.showMessages(
        { infos: [{ message: successMsg }] },
        { feedback: "#mainFeedBack" }
      );
    },

    paste: function (index) {
      var linestr = localStorage && localStorage.getItem("lines"),
        linesArr = linestr ? JSON.parse(linestr) : window.lines;
      if (linesArr) {
        var start = index || 0,
          ls = this.message.lines;
        for (var i = 0; i < linesArr.length; i++) {
          var l = linesArr[i];
          l.line_number = index + i;
          ls.splice(index + i, 0, linesArr[i]);
        }
      }
    },

    insertItem: function (item) {
      console.log("Insert catalog item at " + this.catLineIndex);
      var lines = this.message.lines,
        lineNb = lines.length;
      if (this.catLineIndex == -1) {
        var i = 0;
        for (; i < lineNb; i++) {
          if (this.isBlank(lines[i])) break;
        }
        this.message.lines[i].item = item;
        if (i > lineNb - 2) this.addLine();
      } else {
        this.addLine(this.catLineIndex);
        this.message.lines[this.catLineIndex].item = item;
      }
    },
    feedback: function (result) {
      ns.showMessages(result, { reset: true, feedback: "#mainFeedBack" });
    },
    oldpage: function () {
      window.location.pathname = window.location.pathname.replace(
        "edit_commercialv.html",
        "edit_commercial.html"
      );
    },

    before_send: function () {
      var that = this;
      return new Promise(function (resolve, reject) {
        var modif = false;
        if (!that.message.due_date && that.defaultDueDate) {
          that.message.due_date = that.defaultDueDate;
          modif = true;
        }
        if (!that.message.issue_date) {
          that.message.issue_date = Math.round(Date.now() / 1000);
          modif = true;
        }
        if (modif) {
          that.saveMessage().then(
            function (result) {
              that.loadMessage(result);
              that.updateUrl();
              resolve(result.message);
            },
            function (err) {
              reject(err);
            }
          );
        } else {
          resolve(that.message);
        }
      });
    },

    isAuth: function() {
      return ns.isAuth();
    }
  },
  computed: {
    messageStatLabel: function () {
      var message_status = this.message_status
        ? this.message_status
        : "MODIFIED";
      if (
        this.message.message_type &&
        loc.messageStatus[
          this.message.message_type.toUpperCase() + "-" + message_status
        ]
      ) {
        message_status =
          loc.messageStatus[
            this.message.message_type.toUpperCase() + "-" + message_status
          ];
      } else if (loc.messageStatus[message_status]) {
        message_status = loc.messageStatus[message_status];
      }
      return message_status;
    },
    sender: function () {
      return this.getRole("sender");
    },
    recipient: function () {
      return this.getRole("recipient");
    },
    totals: function () {
      var subTotal = 0,
        vatTotal = 0,
        message = this.message,
        lines = message.lines || [],
        linesNb = lines.length;

      for (var i = 0; i < linesNb; i++) {
        var line = lines[i],
          lineVat = line.vat || 0,
          lineSubTotal = line.subTotal || 0;

        var lPrec = lineSubTotal.getPrecision();
        if (
          this.currencyPrecision != -1 &&
          lPrec &&
          lPrec > this.currencyPrecision
        ) {
          this.currencyPrecision = lPrec;
        }
        subTotal += lineSubTotal;
        if (!message.noVat) {
          vatTotal += lineVat;
        }
      }
      return {
        subTotal: ns.round(subTotal, this.currencyPrecision),
        vat: ns.round(vatTotal, this.currencyPrecision),
        total: ns.round(vatTotal + subTotal, this.currencyPrecision),
      };
    },
    totalSpan: function () {
      return this.UoMColumn ? 3 : 2;
    },
    toPayFirst: function () {
      return (this.totals.total || 0) - (this.message.paidAmount || 0);
    },
    // Return the total minus initial payment minus other payments
    toPay: function () {
      var res = this.toPayFirst,
        acts = this.message.actions,
        actsLen = acts ? acts.length : 0;
      for (var i = 0; i < actsLen; i++) {
        if (acts[i].canceledby) {
          continue;
        }
        var paymts = acts[i].payments,
          paymtsLen = paymts ? paymts.length : 0;
        for (var j = 0; j < paymtsLen; j++) {
          res -= paymts[j].amount;
        }
      }
      return ns.round(res, this.currencyPrecision);
    },
    docType: function () {
      var message_type =
        this.urlParams.message_type ||
        (this.message && this.message.message_type) ||
        "INVOICE";
      message_type = message_type.toUpperCase();

      return (
        ((this.urlParams.feeNote || this.message.feeNote) &&
          loc.messageTypes["FEE_NOTE"]) ||
        loc.messageTypes[message_type] ||
        message_type
      );
    },
    primaryActions: function () {
      return this.possibleActions.slice(0, 4);
    },

    secondaryActions: function () {
      return this.possibleActions.slice(4);
    },
  },
  watch: {
    readonly: function (newVal, oldValue) {
      if (newVal && !oldValue) {
        this.message.lines = this.trimEmptyLines(this.message.lines);
      } else if (!newVal && oldValue) {
        this.addLine();
      }
    },
  },

  updated: function () {
    var that = this;

    this.$nextTick(function () {
      if (this.readonly) {
        var trum = document.querySelector(".trumbowyg");
        if (trum) {
          var tare = trum.querySelector("#summary_note");
          if (tare) {
            trum.parentNode.appendChild(tare);
            trum.remove();
          }
        }
      } else {
        if (document.querySelector(".trumbowyg")) {
          return;
        }
        ns.eceditor.htmleditor = $("textarea#summary_note").trumbowyg({
          lang: ns.getLocale(),
          btns: ns.html4pdfButtons,
        });

        ns.eceditor.htmleditor.on("tbwchange", function () {
          that.$data.message.summary_note = ns.eceditor.htmleditor.trumbowyg(
            "html"
          );
        });
      }
    });
  },
});
