"use strict";

ns.logEvent('Document', 'cancelInvoice');
console.log('Loading cancel invoice panel');

ns.cancelInvoice = {

	load: function (messageId, topay, currency) {

		$(window).openModal(
			'cancelWindow',
			'cancelInvoice',
			function ($modal, init) {
				if (init) {
					var ajaxOpt = { indicator: '#cancelWindow .ajax-indicator', feedback: '#cancelWindow .feedBack' };

					$(".mainact", $modal).click(function (e) {
						e.preventDefault();

						if (messageId) {
							ns.ws('POST', 'v1/message/' + messageId + '/cancelInvoice', null, ajaxOpt)
								.then(function (result) {
									$('#cancelWindow').toggleFade(false);
									// TODO load new message
								});
						} else {
							alert(loc.saveItFirst);
						}
					});
				}
			}
		);
	}
};

//# sourceURL=/static/js/cancelInvoice.js