"use strict";

ns.idSettings4 = {};

ns.idSettings4.vue = new Vue({
    el: '#isSettings',

    data: {
        orgs: [],
        org_id: '',
        contact_id: '',
        edited_person: {},
        is_editing: false
    },

    mounted: function () {
        this.loadIdentities(true);
    },

    methods: {
        // Load from database
        loadIdentities: function (keepFbk) {
            this.orgs = [];
            var that = this;
            ns.ws('GET', 'v1/org/address',
                null, {
                indicator: ".rightMenu .ajax-indicator",
                feedback: '#identityFeedBack',
                keepFbk: keepFbk,
                handleError: true
            })
                .then(function (result) {

                    if (result && result.orgs) {
                        console.log('Got identities');
                        that.orgs = result.orgs;
                    } else {
                        console.log("No identities in response");
                        console.log(result);
                    }
                });
        },

        isAdminOf: function (org) {
            return true;//org && org.rights && org.rights.indexOf('ADMIN') > -1;  
        },

        personStatus: function (person) {
            return {
                person: true,
                admin: this.isPersonAdmin(person),
                currentId: this.isPersonId(person)
            };
        },

        isPersonAdmin: function (person) {
            return person.contact && person.contact.rights && person.contact.rights.indexOf('ADMIN') > -1;
        },

        isPersonId: function (person) {
            return person.validations && person.validations.indexOf('current-identity') != -1;
        },

        openEdit: function (inData) {

            this.edited = inData;
        },

        onEditChange: function ($event) {
            this.loadIdentities();
            ns.showMessages($event);
            console.log('Identity list reloaded after person edition');
            this.is_editing = false;
        },

        clickNew: function (e, obj) {
            var inData = $(obj).data();
            openEdit(inData);
        },

        newOrg: function () {
            console.log("New Org");
            this.org_id = -1;
            this.contact_id = null;
            this.is_editing = true;
        },

        newAddress: function (org) {
            console.log("New Adress");
            this.org_id = org.id;
            this.contact_id = null;
            this.edited_person = {};
            this.is_editing = true;
        },

        newPerson: function (org, address) {
            console.log("New Person");
            this.org_id = org.id;
            this.edited_person = this.cleanPerson({ label: '', email: '', phone: '', fax: '', misc: '', address: address });
            this.contact_id = null;
            this.is_editing = true;
        },

        editPerson: function (org, address, person) {
            console.log("Edit Person");
            person.address = Object.assign({}, address);
            person.address.persons = null;

            this.org_id = org.id;
            this.contact_id = person.contact && person.contact.id;
            this.edited_person = this.cleanPerson(person);
            this.is_editing = true;
        },

        cleanPerson: function (person) {
            person.address.persons = null;
            person.validations = null;
            return person;
        },

        setId: function (person) {
            var that = this;
            ns.ws('PUT', 'v1/person/identity?personid=' + person.contact.person_id,
                null, this.idAjaxOptions).then(function () {
                    that.loadIdentities();
                });
        },

        valEmail: function (person) {
            ns.ws(
                'POST',
                'v1/validation',
                {
                    'type': 'email',
                    'data': person.email,
                    'validationUrl': window.location.href
                },
                this.idAjaxOptions());
        },

        deleteContact: function (contact) {
            var that = this;
            ns.ws('DELETE', 'v1/org/address/' + contact.id, null, this.idAjaxOptions())
                .then(function (result) {
                    console.log('Getting address ' + result);
                    that.loadIdentities();
                });
        },

        idAjaxOptions: function () {
            return {
                indicator: ".rightMenu .ajax-indicator",
                feedback: '#identityFeedBack'
            };
        }
    }
});