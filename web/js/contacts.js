"use strict";

ns.contacts = (function () {
  // Load from database
  self.loadList = function (reqPage) {
    $("#loading").toggle(true);

    // if page is not defined by param, take it from hash or 1 by default
    if (!reqPage) {
      reqPage =
        window.location.hash && parseInt(window.location.hash.replace("#", ""));
    }

    var $listTable = $("#contactListTable");
    var listName = $listTable.data("list");

    // Get data from filter
    var url = "v1/person?pageSz=10&for=recipient";
    if (reqPage) {
      url += "&pageNb=" + reqPage;
    }
    var urlLen = url.length;
    var val = $("#personCrit").val();
    if (val) {
      url += "&s=" + val.trim();
    }

    var hasCrit = url.length > urlLen;

    // Query and render result
    ns.ws("GET", url, null, { handleError: true, indicator: '#loading' })
      .then(function (result) {
        var $tbody = $("tbody", $listTable);

        // Update table
        if (result.count) {
          $("#resNb").setValue(result.count);
        }

        if (
          result.persons &&
          result.persons.length &&
          result.persons.length > 0
        ) {
          var adrCnt = result.persons.length;

          $("tr", $tbody).remove();
          $(".placeHolder").toggle(false);
          var now = Math.floor(Date.now() / 1000);

          for (var i = 0; i < adrCnt; i++) {
            var person = result.persons[i];

            var label = person.person && person.person.label;
            var oLabel =
              person.person &&
              person.person.address &&
              person.person.address.name;
            if (label && oLabel) {
              label += ", " + oLabel;
            } else if (!label && oLabel) {
              label = oLabel;
            } else if (!label && !oLabel) {
              label = "[ Contact ]";
            }

            var contactId = person.contact && person.contact.id;
            var personId = person.person && person.person.id;
            var email = person.person && person.person.email;

            var row =
              "<tr " +
              (i == 0 ? ' class="first"' : "") +
              'data-personid="' +
              personId +
              '" data-contactid="' +
              contactId +
              '"><td class="first-col">&nbsp;</td>' +
              '<td class="label-col"><a href="#">' +
              label +
              "</a></td>" +
              '<td class="email-col">' +
              (email ? email : "") +
              "</td>" +
              '<td class="action-col">' +
              '<a href="#" class="newMessageForLnk" title="' +
              loc.newDoc +
              '"><i class="fa fa-file-o"></i></a>' +
              '<a href="#" class="deleteLnk" title="' +
              loc.del +
              '"><i class="fa fa-trash-o"></i></a>' +
              '</td><td class="last-col"></td></tr>';

            $tbody.append($(row));
          }
        } else {
          $("tr", $tbody).remove();
          $("#pager").toggle(false);
          $(hasCrit ? "#noContent" : "#noContentYet").toggle(true);
        }

        // Update navigation
        var pageSize = 10;
        var currPage = result.page;
        window.location.hash = "#" + currPage;

        var showNav = result.count > pageSize;
        $("#pager").toggle(showNav);

        if (showNav) {
          var lastPage = Math.ceil(result.count / pageSize);
          $("#firstPage")
            .toggle(currPage > 1)
            .attr("href", "#1");
          $("#prevPage")
            .toggle(currPage > 1)
            .attr("href", "#" + (currPage - 1));
          $("#nextPage")
            .toggle(currPage < lastPage)
            .attr("href", "#" + (currPage + 1));
          $("#lastPage")
            .toggle(currPage < lastPage)
            .attr("href", "#" + lastPage);

          $("#pager .pageLnk").remove();
          var maxPageIdx = Math.min(lastPage, currPage + 4) + 1;
          for (var i = Math.max(1, currPage - 4); i < maxPageIdx; i++) {
            var currCls = i == currPage ? "current" : "";
            $(
              '<a href="#' +
              i +
              '" id="page' +
              i +
              '" class="pageLnk ' +
              currCls +
              '">' +
              i +
              "</a>"
            ).insertBefore("#nextPage");
          }
        }
      });
  };

  return self;
})();

/**
 * List common functions.
 */
$(document).ready(function () {
  $("#contactListTable").on(
    "click",
    ".newMessageForLnk",
    function (e) {
      e.preventDefault();
      var $t = $(this);
      var $line = $t.closest("tr");
      var personid = $line.data("personid");

      var $mw = $("#newMessageWindow");
      $mw.toggleFade(true);

      // Get each link, add or change the toPerson parameter
      $(".body a", $mw).each(function () {
        $t = $(this);
        var href = $t.attr("href");
        href = href.replace(/&toPerson=(\d*)/, "");
        if (personid) {
          href += "&toPerson=" + personid;
        }
        $t.attr("href", href);
      });
    }.debounce(200, true)
  );

  $("#contactListTable").on(
    "click",
    ".deleteLnk",
    function (e) {
      e.preventDefault();
      var $t = $(this);
      var $row = $t.closest("tr");
      var personid = $row.data("personid");

      if (!personid) {
        return;
      }

      var r = confirm(loc.confirm);
      if (r == true) {
        ns.ws("DELETE", "v1/person/" + personid).then(function (result) {
          console.log("Deleting contact with person " + personid);
          ns.contacts.loadList();
        });
      }
    }.debounce(200, true)
  );

  $("#contactListTable tbody").on(
    "click",
    ".label-col a",
    function (e) {
      e.preventDefault();
      var $pw = ns.personEditor.getPanel(
        $(this)
          .closest("tr")
          .data()
      );
      $pw.off("ns-select").on("ns-select", function (e, data, $pw) {
        // edition window callback
        ns.contacts.loadList();
        console.log("Address list reloaded after person edition");
        $(this).toggleFade(false);
      });
    }.debounce(200, true)
  );

  $("#newContactBt").click(
    function (e) {
      e.preventDefault();
      var indata = $(this).data();
      indata.showform = true;
      indata.personid = 0;

      var $pw = ns.personEditor.getPanel(indata);
      $pw.off("ns-select").on("ns-select", function (e, data, $pw) {
        // edition window callback
        ns.contacts.loadList();
        console.log("Address list reloaded after person edition");
        $(this).toggleFade(false);
      });
    }.debounce(200, true)
  );

  $('<a href="#" class="closeBt"><i class="fa fa-times"></i></a>')
    .appendTo(".modalWindow .header")
    .click(function (e) {
      e.preventDefault();
      $(this)
        .closest(".modalWindow")
        .toggleFade(false);
    });

  $("#filterLink").click(function (e) {
    e.preventDefault();
    $("#contactListTable")
      .find(".filterRow")
      .toggle();
  });

  $("#filterSubmitBt").click(function (e) {
    e.preventDefault();
    window.location.hash = "";
    ns.contacts.loadList();
  });

  ns.contacts.loadList();

  $("#pager").on(
    "click",
    "a",
    function (e) {
      var href = $(this).attr("href");
      var page = href && parseInt(href.replace("#", ""));
      ns.contacts.loadList(page);
    }.debounce(200, true)
  );
});
