"use strict";

ns.accountBalance = new Vue({
  el: "#accountBalance",
  data: {
    periods: [],
    loading: false,
    noContent: false,
    count: 0,
    hasCrit: false,
    crit: {
      label: "",
      accountPath: "",
      assetPath: "",
      periodId: ns.accounting.defPeriod && ns.accounting.defPeriod.value
    },
    keepFeedback: true,
    filterOn: false,
    accountLabel: "",
    assetLabel: ""
  },
  mounted: function () {
    var that = this,
      params = ns.getQueryParams();

    if (params.accountPath) {
      this.crit.accountPath = params.accountPath;
      this.accountLabel = params.accountPath;
    } else {
      var defAcc = ns.getLocalPref("defAccount");
      this.crit.accountPath = defAcc && defAcc.value;
    }
    if (this.crit.accountPath) {
      this.loadList();
    }

    ns.nextAd();
  },
  methods: {
    // Reload if crit changes
    changeCrit: function (name, val) {
      console.log("Crit " + name + " changed " + val);
      if (this.crit[name] != val) {
        this.crit[name] = val;
        this.loadList();
      }
    },
    // Load from database
    loadList: function () {
      var that = this;

      if (!this.crit.accountPath) {
        this.periods = [];
        this.count = 0;
        this.noResults = true;
        return;
      }

      this.loading = true;

      function addCrit(name) {
        var cr = that.crit[name];
        if (cr) {
          url +=
            "&" + name + "=" + encodeURIComponent(cr.trim ? cr.trim() : cr);
        }
      }

      // Get data from filter
      var url = "v1/accounting/accountBalance?a=1";

      var urlLen = url.length;

      addCrit("label");
      addCrit("accountPath");
      addCrit("assetPath");
      addCrit("periodId");

      this.hasCrit = url.length > urlLen;

      // Query and render result
      ns.ws("GET", url, { handleError: true })
        .then(function (result) {
          if (
            !result.accountBalance ||
            !result.accountBalance.periods ||
            result.accountBalance.periods.count == 0
          ) {
            that.noResults = true;
            that.periods = [];
          } else {
            // Update table
            that.periods = result.accountBalance.periods;
            that.count = result.accountBalance.periods.count;
            that.noResults = false;
          }
          that.loading = false;
        });
    },
    formatAmount(value) {
      return value ? value.formatMoney().replace(" ", "&nbsp;") : "";
    }
  }
});
