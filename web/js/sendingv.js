ns.sending = {
  template: "#sendingComp",
  props: {
    showw: Boolean,
    sender: Object,
    recipient: Object,
    message: Object,
    connected: Boolean,
    before_send: Function
  },
  data: function () {
    var to = new Date();
    to.setFullYear(to.getFullYear() + 1); // Default to one year from here

    return {
      planningOpen: false,
      sendType: "email",
      sendBy: "you",
      editedRecipient: this.recipient || { address: { postal_code: {} } },
      sendingFrequency: "once",
      sendingStartDate: ns.toTs(new Date()),
      sendingEndDate: ns.toTs(to),
      neverEnds: false,
      trumbowyg: {},
      sendingSubject: '',
      sendingComment: '',
      keepAsTemplate: false,
      successMessage: '',
      shareUrl: '',
      publicShare: true,
      feedback: "#sendingWindow .feedBack"
    };
  },
  mounted: function () {
    // Rich text editor
    ns.sending.trumbowyg = $("textarea#emailBody").trumbowyg({
      lang: ns.getLocale(),
      btns: ns.html4pdfButtons
    });

    // Define expeditor label
    this.getTemplate(false);

    ns.ldone('ClipboardJS', '../js/libs/clipboard.min.js').then(function () {
      new ClipboardJS('.copyLnk');
    });
    ns.logEvent('Document', 'openSending');
  },
  watch: {
    recipient: function (newVal) {
      this.editedRecipient = newVal;
    }
  },
  methods: {
    personLabel: function (person) {
      var res =
        person &&
        (person.label ||
          (person.address && person.address.name));
      if (!res) {
        res = loc.someone;
      }
      return res.trim();
    },

    remplaceInTemplate: function (template, replacements) {
      for (var propsName in replacements) {
        var val = ns.xssFilter(replacements[propsName]);
        var regExp = new RegExp(propsName, "gi");
        if (template.subject) {
          template.subject = template.subject.replace(regExp, val);
        }
        if (template.body) {
          template.body = template.body.replace(regExp, val);
        }
      }
      return template;
    },

    getTemplate: function (reset) {
      var that = this,
        mess_type = this.message.message_type.toUpperCase(),
        prefs = mess_type
          ? [
            "message_notification" + mess_type + ".templ",
            "message_notification.templ"
          ]
          : ["message_notification.templ"];

      ns.ws(
        "GET",
        "v1/preference/" + prefs.join("%7C") + (reset ? "?reset=true" : ""),
        null,
        { feedback: this.feedback, handleError: true }
      ).then(function (result) {
        var templ = null;
        for (var i = 0; i < prefs.length; i++) {
          var prefName = prefs[i];
          if (result[prefName]) {
            templ = result[prefName];
            break;
          }
        }
        if (templ) {
          console.log("Valid template returned");
          templ = that.remplaceInTemplate(templ, {
            '<span class="token sender">.*?</span>': that.personLabel(that.sender),
            '<span class="token subject">.*?</span>': that.message.subject
          });
          that.sendingSubject = templ.subject;
          ns.sending.trumbowyg.trumbowyg('html', templ.body);
        } else {
          console.log("No valid template returned");
        }
      });
    },

    resetTemplate: function () {
      this.getTemplate(true);
    },

    send: function (indictr, issue, preview, successMsg) {
      var sendType = this.sendType.toUpperCase();
      if (issue) {
        ns.logEvent('Document', 'submitIssue', sendType);
      } else {
        ns.logEvent('Document', 'submitSending', sendType);
      }
      console.log('Loading emission panel');

      var request = {
        sendingTypes: [sendType],
        recipientPerson: this.editedRecipient,
        senderPerson: this.sendBy == "you" ? this.sender : null,
        sendingComment: this.sendingComment,
        sendingSubject: this.sendingSubject,
        sendingEmailBody: ns.sending.trumbowyg.trumbowyg('html'),
        keepAsTemplate: this.keepAsTemplate,
        preview: preview
      };

      if (this.planningOpen) {
        request.sendingStartDate = this.sendingStartDate;
        request.sendingFrequency = this.sendingFrequency;
        if (!this.neverEnds) {
          request.sendingEndDate = this.sendingEndDate;
        }
      }

      var that = this;
      this.before_send().then(function (theMessage) {
        if (theMessage && theMessage.id) {
          ns.ws('POST', 'v1/message/' + theMessage.id + '/' + (issue ? 'issue' : 'send'), request,
            { feedback: that.feedback, indicator: indictr, handleError: true, noFbOnSuccess: !preview, reset: true })
            .then(function (result) {
              if (!preview) {
                that.$emit("sent", result);
              }
            });
        } else {
          ns.showMessages({ errors: [{ message: loc.saveItFirst }] }, { reset: true });
        }
      });
    },

    createShare: function (indictr, issue, successMsg) {
      ns.logEvent('Document', issue ? 'submitIssueAndShare' : 'submitSendingAndShare');
      console.log('Loading emission panel');

      var that = this,
        message = this.message,
        sendRequest = {
          sendingTypes: ['SHARELINK'],
          recipientPerson: this.editedRecipient,
          publicShare: this.publicShare
        };

      this.before_send().then(function (theMessage) {
        if (theMessage && theMessage.id) {
          ns.ws('POST', 'v1/message/' + theMessage.id + '/' + (issue ? 'issue' : 'send'), sendRequest,
            { feedback: this.feedback, indicator: indictr, handleError: true, reset: true })
            .then(function (result) {
              if (result && result.sendings && result.sendings.length > 0) {
                // Show link
                var s = result.sendings[0],
                  share = navigator.share;

                that.shareUrl = s.url;

                // Update background message display
                result.infos = result.infos || [];
                result.action = 'view';
                result.infos = {};
                result.warnings = {};
                result.errors = {};
                that.$emit("share", result);
              }
            });
        } else {
          ns.showMessages({ errors: [{ message: loc.saveItFirst }] }, { reset: true });
        }
      });
    },

    share: function () {
      if (this.shareEnabled) {
        navigator.share({
          title: this.message && this.message.subject,
          text: this.shareUrl,
          url: this.shareUrl,
        });
      } else {
        ns.logError('Cannot use share on this navigator')
      }
    },

    resetMessages: function () {
      ns.resetMessages({ feedback: this.feedback });
    }
  },
  computed: {
    emailUrl: function () {
      return 'mailto:' + this.editedRecipient.email + '?body=' + encodeURIComponent(this.shareUrl);
    },
    shareEnabled: function () {
      return navigator.share;
    }
  }
};

//# sourceURL=/static/js/sendingv.js
