"use strict";

ns.accounting = {
    defAsset: ns.getLocalPref('defAsset') || { value: '/CURRENCY/EUR', text: 'EUR' },
    defPeriod: ns.getLocalPref('defPeriod'),
};

Vue.component('acc-period-chooser', {
    template: '\
        <select @change="onChange">\
            <option v-for="option in options" v-bind:value="option.value" v-bind:selected="option.sel">\
                {{ option.text }}\
            </option>\
        </select>\
    ',
    props: ['emptyallowed', 'value', 'default'],
    data: function () {
        return {
            selected: 'A',
            options: [{
                text: '--',
                value: 0
            }],
            loading: false
        }
    },
    methods: {
        onChange: function (e) {
            var t = e.target,
                val = t && t.value,
                os = t && t.options,
                si = os && os.selectedIndex,
                txt = si > -1 ? os && si && os[si] && os[si].text : null;
            ns.setLocalPref('defPeriod', { value: val, text: txt });
            this.$emit('critchanged', ['periodId', val, txt]);
        }
    },
    mounted: function () {
        var that = this;

        ns.ws('GET', 'v1/accounting/period', null, {
            keepFbk: true
        })
            .then(function (result) {
                that.selected = null;
                that.loading = true;
                that.options = [];

                if (that.emptyallowed) {
                    that.options.push({
                        text: '--',
                        value: 0
                    });
                }
                var pNb = result.periods && result.periods.length,
                    selId = that.value || result.periods[pNb - 1].id;

                for (var i = 0; i < pNb; i++) {
                    var p = result.periods[i],
                        fromStr = new Date(p.from_date * 1000).format(loc.date.dateFormat),
                        toStr = new Date(p.to_date * 1000).format(loc.date.dateFormat);
                    that.options.push({
                        text: fromStr + ' - ' + toStr,
                        value: p.id,
                        sel: p.id == selId
                    });
                }
            });
    },
});

var pickerInit = function (searchUrl, resultToArr, showPath, defaultValKey, valueProp, ajOpts) {
    return {
        template: '\
            <span class="autocomplete">\
                <input type="text" @input="onChange" v-model="selected.label" @keyup.down="onArrowDown" @keyup.up="onArrowUp" @keyup.enter.prevent="onEnter" v-bind:class="status" v-bind:disabled="disabled">\
                <div class="results sticker" style="min-with:20em; width: max-content" v-show="isOpen">\
                    <div v-if="loading" class="loading">' + loc.linkStatus['loading'] + '</div>\
                    <div v-else class="result" v-for="opt in options" :key="opt.i" v-bind:data-path="opt.path" @click="setResult(opt.i)" :class="{ \'is-active\': opt.i === arrowCounter }">{{opt.label}}</div>\
                </div>\
            </span>\
            ',
        data: function () {
            var that = this;
            return {
                selected: { value: that.value, label: that.label },
                loading: false,
                options: [],
                status: '',
                isOpen: false,
                arrowCounter: -1
            }
        },
        props: ['value', 'label', 'emptyallowed', 'disabled', 'default'],
        methods: {
            onChange: function () {
                var that = this,
                    sl = this.selected.label;

                // If empty, remove values
                if (this.emptyallowed && sl == '') {
                    this.selected.value = null;
                    this.isOpen = false;
                    this.status = null;
                    this.$emit('change', {
                        label: '',
                        value: null
                    });

                } else if (sl && sl.length > 2) {
                    var url1 = searchUrl + encodeURIComponent(sl),
                        url2 = (ns.accounting.defPeriod && ns.accounting.defPeriod.value) ? ('&periodId=' + ns.accounting.defPeriod.value) : '';

                    ns.ws('GET', url1 + url2, null, { feedback: '#' + this.$root.$el.id + ' .feedBack' })
                        .then(function (result) {
                            that.loading = true;
                            that.options = [];
                            that.status = 'dirty';
                            that.isOpen = true;

                            if (that.emptyallowed) {
                                that.options.push({
                                    label: '--',
                                    value: 0
                                });
                            }

                            var sl = that.selected.label,
                                rows = resultToArr(result),
                                rNb = rows ? rows.length : 0;

                            console.log('Searching autocompletion with ' + sl + ': ');

                            for (var rowIdx = 0; rowIdx < rNb; rowIdx++) {
                                var row = rows[rowIdx];
                                that.options.push({
                                    i: rowIdx,
                                    value: row.id,
                                    path: row.path,
                                    label: (showPath ? (row.path + ' - ') : '') + row.label
                                });
                            }
                            that.loading = false;
                        });
                }
            },

            setResult: function (index) {
                var opt = index > -1 && this.options[index];
                if (opt != null) {
                    this.selected = { value: opt.value, label: opt.label };
                    this.isOpen = false;
                    this.status = null;
                    ns.setLocalPref(defaultValKey, { value: opt[valueProp], text: opt.label });
                }
                this.$emit('change', { value: opt.value, label: opt.label, path: opt.path });
            },
            onArrowDown: function (evt) {
                if (this.arrowCounter < this.options.length) {
                    this.arrowCounter = this.arrowCounter + 1;
                }
            },
            onArrowUp: function () {
                if (this.arrowCounter > 0) {
                    this.arrowCounter = this.arrowCounter - 1;
                }
            },
            onEnter: function (ev) {
                ev.preventDefault();
                ev.stopPropagation();
                if (this.arrowCounter > -1) {
                    setResult(this.arrowCounter);
                    this.arrowCounter = -1;
                }
            },
            handleClickOutside: function (evt) {
                if (!this.$el.contains(evt.target)) {
                    this.isOpen = false;
                    this.arrowCounter = -1;
                }
            },
        },
        watch: {
            options: function (val, oldValue) {
                // actually compare them
                if (val.length !== oldValue.length) {
                    this.options = val;
                    this.isLoading = false;
                }
            },
            label: function (val, oldVal) {
                this.selected.label = val;
            },
            value: function (val, oldVal) {
                this.selected.value = val;
            }
        },
        created: function () {
            if (this.value) {
                this.selected = { 'value': this.value, 'label': this.label };
            } else if (this.default) {
                var defVal = ns.getLocalPref(defaultValKey);
                if (defVal)
                    this.selected = { 'value': defVal.value, 'label': defVal.text };
            }
        },
        mounted: function () {
            document.addEventListener('click', this.handleClickOutside);
        },
        destroyed: function () {
            document.removeEventListener('click', this.handleClickOutside)
        }
    };
};

// Account picker
Vue.component('account-picker', pickerInit('v1/accounting/accountSearch?search=', function (result) {
    return result.accounts
}, true, "defAccount", "path"));

//Asset picker
Vue.component('asset-picker', pickerInit('v1/accounting/asset?search=', function (result) {
    return result.assets
}, false, "defAsset", "path"));

Vue.filter('amountFormat', function (value) {
    var t = value ? value.formatMoney() : '';
    window.console.log(t);
    return t;
})

Vue.filter('dateFormat', function (value) {
    return value ? new Date(value * 1000).format(loc.date.dateFormat) : '';
})

Vue.filter('dateTimeFormat', function (value) {
    return value ? new Date(value * 1000).format(loc.date.dateTimeFormat) : '';
})

Vue.filter('localizeWstat', function (value) {
    return value && loc.writingStatus[value] ? loc.writingStatus[value] : value;
})
