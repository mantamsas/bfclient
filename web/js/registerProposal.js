"use strict";

var div = document.createElement("div");
document.body.appendChild(div);
div.outerHTML =
  '\
    <div id="registerProposal">\
        <transition name="modal">\
            <div v-if="show" id="regProposalWindow" class="modalWindow" style="opacity: 1; pointer-events: auto;"><div class="window"><div class="header"><h1>{{loc.title}}</h1><a href="#" class="closeBt" @click.prevent="close"><i class="fa fa-times"></i></a></div>\
                <div class="body">\
                    <img src="../img/pictures/enveloppe.jpg" alt="enveloppe" style="width: 100%; margin: 1rem 0"/>\
                    <div class="feedBack"></div>\
                    <div v-html="text"></div>\
                    <div class="buttons"><button @click.prevent="submit" class="actbt redact ajax log">{{loc.sendMeLink}}<div v-if="ajax" class="ajax-indicator on">&nbsp;</div></button></div>\
                </div>\
            </div>\
        </transition>\
    </div>';

ns.registerProposal = new Vue({
  el: "#registerProposal",
  data: {
    loc: window.loc.registerProposal,
    text: "",
    confirm: false,
    show: false,
    ajax: true,
    sent: false,
    proposedPerson: {}
  },
  methods: {
    // Load form with proposal data
    load: function(sender, proposedPerson) {
      this.proposedPerson = proposedPerson;
      this.text = window.loc.registerProposal.initialText
        .replace("{0}", ns.xssFilter(sender))
        .replace("{1}", ns.xssFilter(this.proposedPerson.email));
      this.ajax = false;
      this.show = true;
      ns.logEvent("Document", "registerToReadLoaded");
    },

    // Send registration and validation email
    submit: function() {
      ns.logEvent("Document", "registerToReadSubmit");
      if (!this.sent) {
        this.sent = true;
        var that = this,
          req = {
            login: this.proposedPerson.email,
            persistent: true,
            adminPerson: this.proposedPerson,
            validationUrl: window.location.href.split("#")[0] // Validation points to the current message
          };

        this.ajax = true;
        ns.ws("POST", "v1/session?action=regAndValidate", req, {
          feedback: "#regProposalWindow .feedBack"
        }).then(function() {
          that.text = that.loc.confirmation.replace(
            "{0}",
            ns.xssFilter(that.proposedPerson.email)
          );
          that.confirm = true;
          that.ajax = false;
          that.sent = false;
          ns.logEvent("Document", "registerToReadSubmitOk");
        });
      }
    },
    close: function() {
      this.show = false;
    }
  },
  mounted: function() {}
});
ns.logEvent("Document", "registerToReadLoad");

//# sourceURL=registerProposal.js
