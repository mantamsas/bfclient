"use strict";

ns.catalog = {};

Vue.filter('amountFormat', function (value) {
    return value ? (value.formatMoney ? value.formatMoney() : value) : '';
})

ns.catalog = new Vue({
    el: '#catalog-view',
    data: {
        count: 0,
        page: 1,
        pageSize: 50,
        search: null,
        items: [],
        noContent: false,
        loading: true,
        itwindow: false,
        impwindow: false,
        pasteon: false,
        showpage: false,
        blank_item: { reference: null, description: null, unit_price: null, unit: null, currency: loc.defaultCurrency },
        edited_item: {},
        UoMColumn: false,
        itemRefColumn: false
    },
    mounted: function () {
        this.loadCatalog();
        this.showpage = true;
        var that = this;

        document.getElementById('pasteInput').addEventListener("paste", function (event) {
            if (that.pasteon) {
                event.preventDefault();

                var content = event.clipboardData.getData('text/html'),
                    parser = new DOMParser(),
                    parsed = parser.parseFromString(content, 'text/html');
                var rowNodes = parsed.querySelectorAll('tr'),
                    table = [],
                    items = [];

                ns.logEvent('Catalog', 'import', rowNodes.length + ' lines');
                for (var i = 0; i < rowNodes.length; ++i) {
                    var colNodes = rowNodes[i].querySelectorAll('td');

                    table[i] = [];
                    for (var j = 0; j < colNodes.length; ++j) {
                        table[i][j] = colNodes[j].innerText;
                    }

                    var rdIdx = 0, currentItem = {};
                    if (that.itemRefColumn) {
                        currentItem.reference = table[i][rdIdx++];
                    }
                    currentItem.description = table[i][rdIdx++];
                    if (that.UoMColumn) {
                        currentItem.unit = table[i][rdIdx++];
                    }
                    currentItem.unit_price = new Number(table[i][rdIdx++].replace(',', '.').replace(' ', ''));
                    currentItem.currency = table[i][rdIdx++];
                    items.push(currentItem);
                }
                console.table(table);
                if (items.length > 0) {
                    ns.ws('POST', 'v1/catalog_item', items).then(function (result) {
                        that.edited_item = that.blank_item;
                        that.impwindow = false;
                        that.loadCatalog();
                    });
                }
            }
        });

        ns.ws('GET', 'v1/preference/UoMColumn+itemRefColumn', null, { handleError: true }).then(function (result) {
            if (result) {
                that.UoMColumn = result.UoMColumn;
                that.itemRefColumn = result.itemRefColumn;
            }
        });
    },
    methods: {
        loadCatalog: function (pageNb, source) {
            this.loading = true;
            if (pageNb) {
                this.page = pageNb;
            }
            var that = this,
                url = 'v1/catalog_item?pageNb=' + (this.page || 1);
            url += '&pageSz=50'; //&source=former_msg
            if (this.search)
                url += '&search=' + this.search;

            ns.ws('GET', url, null, { handleError: true }).then(function (result) {
                that.items = result.items;
                that.count = result.count;
                that.loading = false;
                that.noContent = !result.items || result.items.length == 0;
            });
        },
        submitItem: function (ev, close) {
            var that = this,
                replacedItemId = that.edited_item.id,
                up = that.edited_item.unit_price;

            that.edited_item.unit_price = (up && (typeof up === 'string' || up instanceof String)) ? up.replace(',', '.').replace(' ', '') : null;
            that.edited_item.updated = null;

            var cr = function () {
                ns.ws('POST', 'v1/catalog_item', [that.edited_item]).then(function (result) {
                    that.edited_item = that.blank_item;
                    if (close)
                        that.itwindow = false;
                    that.loadCatalog();
                });
            };

            if (replacedItemId) {
                ns.logEvent('Catalog', 'replace');
                ns.ws('DELETE', 'v1/catalog_item', [replacedItemId]).then(function (result) {
                    cr();
                });
            } else {
                ns.logEvent('Catalog', 'create');
                cr();
            }
        },
        deleteItem: function (itemId) {
            var that = this;
            ns.logEvent('Catalog', 'delete');

            ns.ws('DELETE', 'v1/catalog_item', [itemId]).then(function (result) {
                that.loadCatalog();
            });
        },
        toggleWindow: function () {
            if (!this.itwindow) {
                this.edited_item = this.blank_item;
            }
            this.itwindow = !this.itwindow;
        },
        changeProp: function (name, val) {
            this[name] = val;
        },
        edit: function (catItem) {
            this.edited_item = catItem.item;
            this.edited_item.currency = catItem.currency;
            this.itwindow = true;
        }
        ,
        edit: function (catItem) {
            this.edited_item = catItem.item;
            this.edited_item.currency = catItem.currency;
            this.itwindow = true;
        }
    }
});
