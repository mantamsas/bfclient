/**
 * Ad functions
 */
(function (window) {
  ns.ad = {};

  // Load ads
  ns.ad.load = function () {
    ns.ws("GET", "v1/ad/list")
      .then(function (result) {
        var $tbody = $("#adListTable tbody");

        // Update table
        if (result.ads) {
          $tbody.empty();

          var nb = result.ads.length,
            zero = "0" + loc.number.dec_sep + "00";
          for (var i = 0; i < nb; i++) {
            var adLine = result.ads[i],
              clickRt =
                adLine.clicked == 0
                  ? zero
                  : ns.formatNumber(
                    ns.round((adLine.clicked * 100) / adLine.displayed, 2)
                  ),
              dismissRt =
                adLine.dismissed == 0
                  ? zero
                  : ns.formatNumber(
                    ns.round((adLine.dismissed * 100) / adLine.displayed, 2)
                  ),
              line =
                '<tr data-adid="' +
                adLine.ad.id +
                '"><td class="first-col"></td><td><a class="editLnk" href="#">' +
                adLine.ad.title +
                "</a></td><td>" +
                adLine.ad.status +
                "</td><td>" +
                adLine.displayed +
                "</td><td>" +
                clickRt +
                " % (" +
                adLine.clicked +
                ")</td><td>" +
                dismissRt +
                " % (" +
                adLine.dismissed +
                ')</td><td class="action-col">';

            line +=
              adLine.ad.status == "ONLINE"
                ? '<a class="offLnk" href="#"><i class="fa fa-toggle-off"></i></a>'
                : '<a class="onLnk" href="#"><i class="fa fa-toggle-on"></i></a>';
            line += '<a class="viewLnk" href="#"><i class="fa fa-eye"></i></a>';
            line +=
              '<a class="delLnk" href="#"><i class="fa fa-trash"></i></a></td><td class="last-col"></td></tr>';
            $tbody.append(line);
          }
        }
        $("#noContentYet").toggle(!result.ads);
        $("#loading").toggle(false);
      })
      .catch(function (xhr) {
        ns.showMessages(xhr.jsonResp);
        console.log("Ad list load error");
        $("#loading").toggle(false);
        ns.logError(xhr);
      });
  };

  // Edition
  ns.ad.edit = function (adObj) {
    var $aw = $("#adWindow");
    $aw.toggleFade(true);
    $aw.find(".header .closeBt").on("click", function (e) {
      e.preventDefault();
      $(e.target)
        .closest("#adWindow")
        .toggleFade(false);
    });
    var $f = $aw.find("form");
    $("input[name=id]", $f).val(adObj ? adObj.id : "");
    $("input[name=title]", $f).setValue(adObj ? adObj.title : "");
    $("textarea[name=text]", $f).val(adObj ? adObj.text : "");
    $("input[name=link]", $f).val(adObj ? adObj.link : "");
    $("input[name=display_link]", $f).val(adObj ? adObj.display_link : "");
  };

  // Change status

  // Init
  $(document).ready(function () {
    ns.ad.load();
    $("#adListTable tbody").on("click", ".editLnk", function (e) {
      var $row = $(this).closest("tr"),
        adId = $row.data("adid");

      if (adId) {
        ns.ws("GET", "v1/ad/" + adId).then(function (result) {
          ns.ad.edit(result.ad);
        });
      }
    });

    $("#adListTable tbody").on("click", ".delLnk", function (e) {
      var $row = $(this).closest("tr"),
        adId = $row.data("adid");

      if (adId) {
        ns.ws("DELETE", "v1/ad/" + adId).then(function (result) {
          ns.ad.load();
        });
      }
    });

    function setTo(e, that, status) {
      e.preventDefault();
      e.stopPropagation();
      var $row = $(that).closest("tr"),
        adId = $row.data("adid");

      if (adId) {
        ns.ws("PUT", "v1/ad/" + adId + "/status?to=" + status).then(function (
          result
        ) {
          ns.ad.load();
        });
      }
    }

    $("#adListTable tbody").on("click", ".onLnk", function (e) {
      setTo(e, this, "ONLINE");
    });

    $("#adListTable tbody").on("click", ".offLnk", function (e) {
      setTo(e, this, "SUSPENDED");
    });

    $("#adListTable tbody").on("click", ".viewLnk", function (e) {
      var $row = $(this).closest("tr"),
        adId = $row.data("adid");

      if (adId) {
        ns.ws("GET", "v1/ad/" + adId).then(function (result) {
          ns.showAd(result.ad);
        });
      }
    });

    $("#newAdBt").click(function (e) {
      e.preventDefault();
      ns.ad.edit();
    });

    $("#adWindow form").submit(function (e) {
      e.preventDefault();
      var $f = $(this).closest("form"),
        id = $("input[name=id]", $f).val(),
        adObj = {
          id: id && id.length > 0 ? id : null,
          title: $("input[name=title]", $f).getValue(),
          text: $("textarea[name=text]", $f).val(),
          link: $("input[name=link]", $f).getValue(),
          display_link: $("input[name=display_link]", $f).getValue()
        };
      ns.ws("POST", id ? "v1/ad/" + id : "v1/ad", adObj).then(function (result) {
        $("#adWindow").toggleFade(false);
        ns.ad.load();
      });
    });
  });
})(this);
