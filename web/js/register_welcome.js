"use strict";

ns.registerview = new Vue({
    el: '#register-view-welcome',
    data: {
        ajOpt: { feedback: '#signInWindow .feedBack', indicator: '#regBt .ajax-indicator', handleError: true }
    },
    methods: {
        showVideo: function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.8&appId=1341914452541526";
            fjs.parentNode.insertBefore(js, fjs);
        }
    },
    mounted: function () {
        var that = this;
        ns.ws('GET', 'v1/session', null, this.ajOpt).then(function (result) {
            var con = result.status == 'ok' && result.connected == true;
            if (!con) {
                window.location.href = '/v/register/';
            }
        });
        this.showVideo(document, 'script', 'facebook-jssdk');
    }
});