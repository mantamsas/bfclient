"use strict";

var aLoc = window.location;
if (aLoc.protocol != "https:") {
  if (confirm(loc.shouldBeHttps)) {
    console.log("Redirection to HTTPS");
    location.href = "https:" + aLoc.href.substring(aLoc.protocol.length);
  }
}

/**
 * Subscribe page
 */
ns.subscription = {};

ns.subscription.view = new Vue({
  el: "#subscription-view",
  data: {
    prices: [],
    params: {
      register_stripe: false
    },
    unalterable: false,
    offer: null,
    frequency: "year",
    pwindow: false,
    strAjOpt: {
      feedback: "#stripeWindow .feedBack",
      indicator: "#stripeWindow .ajax-indicator"
    },
    trial_days: 30,
    loading: false
  },

  mounted: function () {
    this.params = ns.getQueryParams();
    if (this.params) {
      this.unmodifiable = this.params.unmodifiable;
    }
    this.load_prices();
  },

  methods: {
    load_prices: function () {
      var that = this;
      ns.ws("GET", "v1/stripe/prices")
        .then(function (result) {
          that.prices = result.prices;
        });
    },
    subscribe: function (price) {
      if (ns.isAuth()) {
        this.loading = true;

        var that = this;
        ns.ws("POST", "v1/stripe/register_checkout",
          { price_id: price.id, web_base: this.next_step, unalterable: this.unalterable })
          .then(function (result) {
            that.loading = false;
            if (result.url) {
              window.location = result.url;
              ns.logEvent("Subscription", "subscription-redirect", price.offer);
            } else {
              ns.logEvent("Subscription", "subscription-failure", price.offer);
              ns.logError(err);
            }
          })
          .catch(function (err) {
            that.loading = false;
            ns.logEvent("Subscription", "subscription-failure", price.offer, err);
            ns.logError(err);
          });
      } else {
        ns.showMessages(
          { errors: [{ message: loc.shouldBeAuth }] },
          null,
          null,
          true
        );
        ns.logEvent("Subscription", "noauth-open", "");
      }
    },
    stayFree: function () {
      ns.logEvent("Subscription", "stay-free");
      window.location = this.next_step;
      return true;
    },
    amount: function (price) {
      if (price === null) return "Err";
      return new Intl.NumberFormat().format(price.unit_amount / (100 * this.time_crits.divider));
    }
  },
  computed: {
    time_crits: function () {
      if (this.frequency === "year") {
        return { interval: "year", interval_count: 1, divider: 12 };
      }
      if (this.frequency === "semester") {
        return { interval: "month", interval_count: 6, divider: 6 };
      }
      return { interval: "month", interval_count: 1, divider: 1 };
    },

    pro_price: function () {
      let price = null;
      this.prices.forEach(v => {
        if (v.offer === "/MEMBERSHIP/PRO" &&
          v.interval === this.time_crits.interval &&
          v.interval_count === this.time_crits.interval_count) {
          price = v;
        }
      });
      return price;
    },

    enterprise_price: function () {
      let price = null;
      this.prices.forEach(v => {
        if (v.offer === "/MEMBERSHIP/ENTERPRISE" &&
          v.interval === this.time_crits.interval &&
          v.interval_count === this.time_crits.interval_count) {
          price = v;
        }
      });
      return price;
    },

    next_step: function () {
      return ns.subscription.next_step = ns.vue3Url + '/invoicing';
    }
  }
});
