"use strict";

//Pager navigation
Vue.component("pager", {
  template:
  `
	  	<div class="pager" v-show="count > pagesize">
			<span>{{results || count}}</span> {{'results' | localize}}
			<a href="#" v-show="page > 1" v-bind:title="'first' | localize" @click.prevent="goto(1)">&lt;&lt;</a>
			<a href="#" v-show="page > 1" rel="prev" v-bind:title="'prev' | localize" @click.prevent="goto(page - 1)">&lt;</a>
			<a v-for="pg in pages" :key="pg" href="#" @click.prevent="goto(pg)" :class="{ 'current': pg === page }">{{pg}}</a>
			<a href="#" v-show="page < lastPage" rel="next" v-bind:title="'next' | localize" @click.prevent="goto(page + 1)">&gt;</a>
			<a href="#" v-show="page < lastPage" v-bind:title="'last' | localize" @click.prevent="goto('last')">&gt;&gt;</a>
		</div>
	  `,
  props: ["results", "count", "page", "pagesize"],
  computed: {
    lastPage: function () {
      return Math.ceil(this.count / this.pagesize);
    },
    pages: function () {
      var maxPageIdx = Math.min(this.lastPage, this.page + 2),
        resp = [];

      for (var i = Math.max(1, this.page - 2); i < maxPageIdx + 1; i++) {
        resp.push(i);
      }
      return resp;
    }
  },
  mounted: function () {
    console.log("Pager et mounted");
  },
  methods: {
    goto: function (pageNb) {
      this.$emit(
        "gotopage",
        "last" == pageNb ? Math.floor(this.count / this.pagesize) : pageNb
      );
    }
  }
});

//Preference checkbox with auto update
Vue.component("preference-checkbox", {
  template:
  `<input type="checkbox" v-bind:name="name" @change="change($event)" :disabled="disabled" :checked="value ? 'CHECKED' : ''"/>`,
  props: {
    name: String,
    value: Boolean,
    disabled: Boolean
  },
  data: function () {
    return { messages: [] };
  },
  methods: {
    change: function ($event) {
      var req = {};
      req[this.name] = $event.target.checked;
      ns.resetMessages();
      ns.ws("POST", "v1/preference", req);
      this.$emit("update:" + this.name.toLowerCase(), req[this.name]);
    }
  }
});

Vue.component("preference-string", {
  template:
    '<span v-if="disabled || $parent.readonly" :style="$attrs.style">{{value}}</span><input v-else-if="!$parent.readonly" :type="$attrs.type" @change="change($event)" :disabled="disabled" v-model="edited" :style="$attrs.style" :maxlength="$attrs.size"/>',
  props: {
    name: String,
    value: [String, Number],
    disabled: Boolean,
    empty_is: "" // The value expressed by empty
  },
  data: function () {
    return { edited: '' };
  },
  methods: {
    change: function ($event) {
      var req = {},
        newVal = $event.target.value;
      req[this.name] = newVal === '' ? this.empty_is : newVal;
      ns.resetMessages();
      ns.ws("POST", "v1/preference", req);
      this.$emit("input", newVal);
    }
  },
  watch: {
    value: function (newVal) {
      if (this.edited != newVal)
        this.edited = newVal == this.empty_is ? '' : newVal;
    }
  }
});

// Generic popup window
Vue.component("popup", {
  template: `
    <transition name="modal">
        <div v-bind:id="id" class="modalWindow" :style="{'pointer-events': 'auto', 'opacity': showw ? 1 : 0}" v-show="showw"><div v-bind:style="winstyle" class="window"><div class="header"><h1>{{windowtitle}}</h1><a href="#" @click.prevent="closeme"><i class="fa fa-times"></i></a></div>
            <div class="body" style="width: initial">
                <div :class="[$parent.loading ? 'on' : '', 'ddd','ajax-indicator']" style="float:right">&nbsp;</div>
                <div class="feedBack"></div>
                <slot></slot>
            </div>
        </div></div>
    </transition>
    `,
  props: {
    id: String,
    showw: Boolean,
    windowtitle: String,
    winstyle: Object
  },
  data: function () {
    return { messages: [] };
  },
  methods: {
    closeme: function ($event) {
      console.log("closing popup " + this.id);
      this.$emit("close");
    },
    addEvent: function (level, message) { }
  }
});

Vue.component("vue-popup", {
  template: `
        <transition name="modal">
            <div v-bind:id="id" class="modalWindow" :style="{'pointer-events': 'auto', 'opacity': showw ? 1 : 0}" v-show="showw"><div v-bind:style="winstyle" class="window"><div class="header"><h1>{{windowtitle}}</h1><a href="#" @click.prevent="closeme"><i class="fa fa-times"></i></a></div>
                <div class="body" style="width: initial">
                    <div :class="[loading ? 'on' : '', 'ddd','ajax-indicator']" style="float:right">&nbsp;</div>
                    <div v-for="(message, index) in messages" :class="['feedBack', message.level]" v-html="message.text"></div>
                    <slot></slot>
                </div>
            </div></div>
        </transition>
        `,
  props: {
    id: String,
    showw: Boolean,
    windowtitle: String,
    winstyle: Object,
    messages: Array,
    loading: Boolean
  },
  data: function () {
  },
  methods: {
    closeme: function ($event) {
      console.log("closing popup " + this.id);
      this.$emit("close");
    },
  }
});

// Button with indicator
Vue.component("indicated-button", {
  template:
    `<button v-bind:class="[mainact ? 'mainact' : '', 'actbt', 'ajax']" @click.prevent="send"><slot></slot><div v-bind:class="[loading ? 'on' : '', 'ajax-indicator', colorclass]">&nbsp;</div></button>
    `,
  props: {
    loading: Boolean,
    mainact: Boolean,
    colorclass: String
  },
  data: function () {
    return {};
  },
  methods: {
    send: function () {
      this.$emit("click");
    }
  }
});

//Preference template link
Vue.component("preference-template", {
  template:
  `
    <div class="templateEd">
      <a href="#" @click.prevent="open" v-bind:id="name + 'Lnk'">{{label}}</a>
      <popup v-bind:id="name + 'Win'" v-bind:showw="wopen" @close="wopen = false" v-bind:windowtitle="windowtitle" v-bind:winstyle="winstyle">
        <slot></slot>
        <textarea v-bind:id="name + 'Ta'"></textarea>
        <div class="buttons" style="margin-top: 17px">
          <button class="actbt ajax" @click.prevent="send">{{sendLabel}}<div v-bind:class="[loading ? 'on' : '', 'ajax-indicator', 'ddd']">&nbsp;</div></button>
        </div>
      </popup>
    </div>
    `,
  props: {
    name: String,
    label: String,
    windowtitle: String,
    content: String,
    winstyle: Object
  },
  data: function () {
    return {
      wopen: false,
      wcontent: "",
      loading: false,
      trumObj: null,
      sendLabel: loc.send
    };
  },
  methods: {
    send: function () {
      var req = {},
        that = this;

      req[this.name + ".templ"] = that.trumObj.trumbowyg("html");
      ns.resetMessages();
      that.loading = true;
      ns.ws("POST", "v1/preference", req).then(function (result) {
        that.loading = false;
        that.wopen = false;
        that.$emit("change", that.name);
      });
    },
    open: function () {
      var that = this;

      // Init editor
      if (!that.trumObj) {
        that.trumObj = $("#" + this.name + "Ta");
        that.trumObj.trumbowyg({
          lang: ns.getLocale(),
          btns: ns.html4pdfButtons
        });
      }

      // Load former template
      ns.ws("GET", "v1/preference/" + this.name + ".templ").then(function (
        result
      ) {
        var v = result[that.name + ".templ"];

        if (!v || !v.body) {
          console.log(
            "Preference " + that.name + ".templ return empty response"
          );
          v = "";
        } else {
          v = v.body;
        }
        that.trumObj.trumbowyg("html", v);
        that.trumObj.html(v);
        that.loading = false;
      });
      that.loading = true;
      that.wopen = true;
    }
  }
});

Vue.filter("localize", function (value) {
  return value && loc[value] ? loc[value] : value;
});

Vue.filter("xssFilter", function (value) {
  return ns.xssFilter(value);
});

const dateOptions = { year: 'numeric', month: 'numeric', day: 'numeric',  timeZone: 'UTC'};
Vue.filter("dateFormat", function (value) {
  return new Date(value * 1000).toLocaleDateString(loc.locale, dateOptions);
});

Vue.filter("dateTimeFormat", function (value) {
  return new Date(value * 1000).format(loc.date.dateTimeFormat);
});

Vue.filter("formatMoney", function (value) {
  return value != null ? (value.formatMoney ? value.formatMoney() : value) : "";
});

Vue.filter("formatMoney2", function (value, precision) {
  return value != null ? (ns.formatNumber(value, { decimals: precision })) : "";
});

// Editable form components
Vue.component("editable-text", {
  template:
    '<span v-if="readonly || $parent.readonly" :style="$attrs.style">{{value}}</span><input v-else-if="!$parent.readonly" type="text" @change="change($event)" :value="value" :style="$attrs.style"/>',
  props: ["value", "readonly"],
  methods: {
    change: function ($event) {
      this.$emit("input", $event.target.value);
    }
  }
});

Vue.component("editable-textarea", {
  template:
    '<span v-if="readonly || $parent.readonly" :style="$attrs.style" v-html="value"></span><textarea v-else-if="!$parent.readonly" @change="change($event)" :style="$attrs.style">{{value}}</textarea>',
  props: {
    value: String,
    readonly: Boolean
  },
  methods: {
    change: function ($event) {
      this.$emit("input", $event.target.value);
    }
  }
});

// Date picker
Vue.component("date-picker", {
  template: `<span class="value date"><template v-if="readonly && value">{{value | dateFormat}}</template><input v-if="!readonly" type="text" class="date"/></span>`,
  props: ["value", "readonly"],
  mounted: function () {
    if (!this.readonly) {
      this.initpicker();
      if (this.value) this.setValue(this.value);
    }
  },
  methods: {
    initpicker: function () {
      var elem = this.$el.querySelector("input");
      if (!elem) {
        return;
      }
      var prs = function (str) {
        var ts = ns.parseDate(str) * 1000;
        return isNaN(ts) ? Date.now() : ts;
      },
        dp = TinyDatePicker(elem, {
          mode: "dp-below",
          lang: loc.date,
          format: ns.formatDate,
          parse: prs,
          hilightedDate: new Date()
        }),
        that = this;
      dp.on("statechange", function (_, piker) {
        if (piker.state.selectedDate && piker.state.selectedDate != NaN)
          that.$emit("input", ns.toTs(piker.state.selectedDate));
      });
      this.$el.dp = dp;
      this.setValue(this.value);

      var dateCss = document.getElementById("dateCss");
      if (!dateCss) {
        var css = document.createElement("LINK"),
          url = "../js/libs/datepicker/tiny-date-picker.css";
        css.id = "dateCss";
        css.rel = "stylesheet";
        css.type = "text/css";
        css.href = ns.debug ? url : url.replace(/css$/i, "min.css");
        css.media = "all";
        document.getElementsByTagName("head")[0].appendChild(css);
      }
      return dp;
    },
    setValue: function (newVal) {
      if (this.$el.dp)
        this.$el.dp.setState({
          selectedDate: newVal * 1000
        });
    }
  },
  updated: function () {
    this.$nextTick(function () {
      this.initpicker();
    });
  },
  watch: {
    value: function (newVal) {
      this.setValue(newVal);
    }
  }
});

Vue.component("amount-input", {
  props: ["decimals", "prefix", "suffix", "value"],
  template: `<input type="text" v-model="displayValue" @change="change" @blur="isInputActive = false" @focus="isInputActive = true"/>`,
  data: function () {
    return {
      isInputActive: false,
      displayValue: ""
    };
  },
  methods: {
    change: function () {
      let val = this.displayValue;
      if (this.suffix && val.endsWith(this.suffix)) {
        val = val.substring(0, val.length - this.suffix.length);
      }
      if (this.prefix && val.startsWith(this.prefix)) {
        val = val.substring(this.prefix);
      }
      val = ns.parseNumber(val);
      val = isNaN(val) ? 0 : val.valueOf();
      this.$emit("input", val);
    }
  },
  watch: {
    value: {
      immediate: true,
      handler: function (val) {
        this.displayValue =
          (this.prefix || "") +
          ns.formatNumber(this.value || 0, {
            decimals: this.decimals
          }) +
          (this.suffix || "");
      }
    }
  }
});

Vue.component("button-indicated", {
  props: {
    "main": Boolean
  },
  template: `<button :class="{actbt: true, ajax: true, mainact: main}" @click.prevent="clicked"><slot></slot><div class="ajax-indicator">&nbsp;</div></button>`,
  methods: {
    clicked: function (event) {
      this.$emit('click', this.$el.querySelector('.ajax-indicator'));
    },
  }
});

// Async component
ns.createVueComp = function (compKey) {
  Vue.component(compKey, function (resolve) {
    var tplPath = compKey + "v.html",
      comPath = "../js/" + compKey + "v.js";
    ns.loadhtml(tplPath, document.body) // localized template
      .then(function () {
        return ns.ldone(compKey, comPath); // localized component
      }, function () {
        console.error("Error loading " + tplPath)
      })
      .then(function () {
        resolve(ns[compKey]);
      }, function () {
        console.error("Error loading " + comPath);
      });
  });
};
