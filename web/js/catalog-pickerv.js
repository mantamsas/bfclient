"use strict";

ns["catalog-picker"] = {
  props: ["window_title"],
  template: "#catalogPickerComp",
  data: function () {
    return {
      search: "",
      items: [],
      searching: false
    };
  },
  methods: {
    searchCatalog: function () {
      if (!this.search || this.search.length == 0) {
        this.items = [];
        this.searching = false;
      } else if (this.search.length > 2) {
        this.searching = true;
        var that = this,
          url = "v1/catalog_item?pageNb=1&pageSz=15&search=" + this.search;
        ns.ws("GET", url).then(function (result) {
          that.items = result.items;
          that.searching = false;
        });
        ns.logEvent("Catalog", "search");
      }
    },
    selectItem: function (item) {
      ns.logEvent("Catalog", "select");
      this.$emit("select", item);
    }
  }
};
