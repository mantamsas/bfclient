"use strict";

(function($) {
    ns.eg = Object.create(ns.editor);

    /**
     * Functions related to generic message edition.
     * 
     * TODO: Make address
     * edition possible upon enter when tabbed
     * 
     */

    /**
     * Update page to follow loaded data and parameters
     */
    ns.eg.loadForm = function(data, noUpdateAuth) {
        if (!data) {
            return;
        }

        var $h = $('html');
        if (!noUpdateAuth) {
            $h.data('lastQuery', Date.now());
            ns.setAuthUi(data.connected);
        }

        if (data.editIssueDate) $(window).data('editIssueDate', true);

        var message_type = data.message_type || (data.message && data.message.message_type);
        message_type = message_type ? message_type : 'LETTER';

        var messageTypeLabel = message_type;
        if (loc.messageTypes[message_type]) {
            messageTypeLabel = loc.messageTypes[message_type];
        }
        $('.docType').html(messageTypeLabel);

        $('.messageDetails')
            .addClass(message_type.toLowerCase());

        $(window).data('message_type', message_type);

        var deleted = false;
        var issued = false;
        var closed = false;

        if (data.persons) {
            $(window).data('persons', $.extend($(window).data('persons'), data.persons));
        }
        if (data.users) {
            $(window).data('users', $.extend($(window).data('users'), data.users));
        }
        ns.createActionMenu($('#secondary-menu'), data.possibleActions, ns.eg);

        var now = Date.now() / 1000;

        // Load message data
        if (data.message) {
            var message = data.message;
            if (data.action == 'duplicate' || data.action == 'mutate') {
                message.id = null;
            }

            $(window).data('message', message);

            var actions = message.actions;
            var actNb = message.actions ? actions.length : 0;

            for (var j = 0; j < actNb; j++) {
                var action = actions[j];
                if(action.canceledby){
                    continue;
                }
                if (action.type == 'ISSUED') {
                    issued = true;
                }
                if (action.type == 'TRASH' || action.type == 'DELETE') {
                    deleted = true;
                }
                if (action.type == 'CLOSED') {
                    closed = true;
                }
            }

            // Force read-only if not modifiable
            if (!data.modifiable) {
                if (data.action == 'edit') {
                    data.action == 'view';
                }
            } else if (!issued && !closed && !deleted) {
                // Not issued => force to edit
                data.action = 'edit';
            }

            var messageStatus = data.messageStatus ? data.messageStatus : 'MODIFIED';
            if (data.message.message_type && loc.messageStatus[data.message.message_type.toUpperCase() + '-' + messageStatus]) {
                messageStatus = loc.messageStatus[data.message.message_type.toUpperCase() + '-' + messageStatus];
            } else if (loc.messageStatus[messageStatus]) {
                messageStatus = loc.messageStatus[messageStatus];
            }
            $('#messageStatus').html(messageStatus);

            // Set data
            $('#message_number').setValue(message && message.message_number);
            $('#subject').setValue(message && message.subject);
            $('#issue_date').setValue(message && message.issue_date);
            $('#due_date').setValue(message && message.due_date);
            var cr = message.recipient_reference;
            $('#recipient_reference').setValue(cr).parent().toggle(undefined != cr);
            var sr = message.sender_reference;
            $('#sender_reference').html(sr).parent().toggle(undefined != sr);

            if (message.roles && data.persons) {
                if (message.roles.sender && data.persons[message.roles.sender]) {
                    $('#sender-cell .personDisplay').loadPersonObj(data.persons[message.roles.sender]);
                }
                if (message.roles.recipient && data.persons[message.roles.recipient]) {
                    $('#recipient-cell .personDisplay').loadPersonObj(data.persons[message.roles.recipient]);
                }
            }
            $('#generic-content').setValue(message && message.content);
        } else {

            // Load empty data
            $('#issue_date').setValue(now);
            $('#due_date').setValue(now + 86400); // Tomorrow	
        }

        // Read-only toogle
        var editAction = data.action == 'edit';
        $('#save').parent().toggle(editAction);
        $('#modify').parent().toggle(!editAction);
        if (editAction) {
            if ($(window).data('action') != 'edit') {
                this.enableEdit(data);
            }
            var issue_date = $('#issue_date').getValue();
            if (!data.editIssueDate || !issue_date) {
                $('#issue_date').setValue(now);
            }
        } else if ($(window).data('action') == 'edit') {
            this.disableEdit();
        }
        $(window).data('action', data.action);
    };

    ns.eg.formToJson = function(params) {
        var savmess = {
            'content_type': 'GENERIC_MESSAGE'
        }; // Message
        // to save
        savmess.message_type = $(window).data('message_type');

        var message = $(window).data('message');
        if (message && message.id) {
            savmess.id = message.id;
        }

        console.log("Saving message " + savmess.id);

        // Main infos
        var fields = ['message_number',
            'subject',
            'issue_date',
            'due_date',
            'sender_reference',
            'recipient_reference',
            'text'
        ];
        for (var i = 0; i < fields.length; i++) {
            var key = fields[i];
            savmess[key] = $('#' + key).getValue();
        }

        savmess.content = $('#generic-content').getValue();

        // Document options
        var $wd = $(window).data();
        savmess.roles = {};

        savmess.roles.sender = $('#sender-cell').find('.personDisplay').data('personid');
        savmess.roles.recipient = $('#recipient-cell').find('.personDisplay').data('personid');
        return savmess;
    };

    ns.eg.enableEdit = function(options) {

        $('.item-line > .reference, #sender_reference, #recipient_reference, #due_date').elem2input('input');

        $('#subject, #generic-content').elem2input('textarea');

        if (options.editIssueDate) {
            $('#issue_date').elem2input('input');
            ns.setDatePicker("#issue_date > input");
        }
        $('#due_date').elem2input('input');
        ns.setDatePicker("#due_date > input");
        $('body').addClass('editing');
        var $tbody = $('#lines-table tbody');

        var self = this;
        // Initial
        $('#sender-cell .personDisplay, #recipient-cell .personDisplay')
            .makeAddressEditable();

        // Deal with option menu
        function initMenuOpt(id, messageOpt, callback) {

            $(id).click(function(e) {
                var $t = $(this);
                var checked = $t.is('.checked');
                $t.removeClass(checked ? 'checked' : 'unchecked')
                    .addClass(checked ? 'unchecked' : 'checked');
                if (messageOpt) {
                    var message = $d.data('message');
                    message.specifics = ns.setSpecif(message.specifics, messageOpt, !checked ? 'true' : null);
                }
                if (callback) {
                    callback(!checked);
                }
            });
        }

        // Auto save, each 3s when modified
        var onChangeFunc = function(e) {
            if ($(window).data('message') && $(window).data('message').id) {
                self.saveMessage();
            }
        };

        $('input, textarea, select').on("change keyup", onChangeFunc.debounce(1000, false));

        $('.personDisplay').on("change", onChangeFunc);

        $(document).ready(function() {
            $('input').nextOnReturn();
        });
    };

    ns.eg.disableEdit = function() {
        $('body').removeClass('editing');
        $('#sender-cell .personDisplay, #recipient-cell .personDisplay').removeClass('edited').off('click');
        $('.edited').input2elem();
        $('#sendingWindow .personDisplay').addClass('edited');

        // remove empty lines
        $('.item-line').filter(function(index) {
            return !$('.description', this).getValue() && !$('.reference', this).getValue();
        }).remove();
    };
}(jQuery));

$(document).ready(function() {
    ns.eg.editMessage();
});