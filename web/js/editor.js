"use strict";

(function () {
  /**
   * Common editor functions
   **/
  ns.editor = {};

  ns.editor.loadFeature = function (data, feature) {
    var f = false;
    if (data && data.message && data.message[feature]) {
      f = data.message[feature];
    } else {
      f = data[feature];
    }

    if (f) {
      $(window).data(feature, true);
    } else {
      $(window).data(feature, null);
    }
  };

  ns.editor.doAfterSave = function (after) {
    var message = $(window).data("message");
    if (message && $(window).data("action") == "edit") {
      this.saveMessage(
        {
          noReload: true
        },
        function (isOk, result) {
          after(result.message && result.message.id);
        }
      );
    } else {
      after(message.id);
    }
  };

  ns.editor.loadActionScript = function (formObjName, scriptName) {
    this.doAfterSave(function (messageId) {
      ns.requires(formObjName, scriptName).then(function (formObj) {
        formObj.load(messageId);
        formObj.onSuccess = function (result) {
          ns.showMessages(result, { reset: true });
          window.messageEditor.loadForm(result);
        };
      });
    });
  };

  /**
   * Query message from server and displays it.
   */
  ns.editor.editMessage = function (callback) {
    window.messageEditor = this;
    $("#linkStatus").html(loc.linkStatus.loading);

    // init page
    var params = ns.getQueryParams(),
      messageId = params.message_id || params.messageId || "new",
      url = "v1/message/" + messageId + "?loadAddress=true";

    if (
      (messageId == "new" || params.action == "duplicate") &&
      !params.content_type
    ) {
      params.content_type = "commercial_message";
    }

    for (var property in params) {
      if (params.hasOwnProperty(property)) {
        url += "&" + property + "=" + params[property];
      }
    }

    function both() {
      if (callback) {
        callback();
      }
    }

    var self = this;
    ns.ws("GET", url)
      .then(function (result) {
        self.loadForm(ns.extend(params, result));
        $("#linkStatus").html("");
        both();
      })
      .catch(function (xhr) {
        $("#linkStatus").html(loc.linkStatus.error);
        var result = xhr.jsonResp;

        if (!result) {
          ns.logError(xhr);
        } else if (result.registerAndValidateUser) {
          ns.requires("registerProposal", "registerProposal.js").then(
            function () {
              ns.registerProposal.load(
                result.senderPerson,
                result.registerAndValidateUser
              );
            }
          );
        } else if (result.proposedLogin) {
          $(window).openLogin("log", {
            adminPerson: { email: result.proposedLogin }
          });
        }
        both();
      });
  };

  ns.editor.saveMessage = function (options, callback) {
    if (!options) {
      options = {};
    }

    var params = ns.getQueryParams();
    if (!this.formToJson) {
      throw "formToJson not defined";
    }
    var savmess = this.formToJson(params);

    $("#linkStatus").html(loc.linkStatus.saving);

    var theUrl = "v1/message" + (savmess.id ? "/" + savmess.id : "");

    if (options.reattach) {
      theUrl = theUrl + "?reattach=true";
    }

    var self = this;

    ns.ws("POST", theUrl, savmess)
      .then(function (result) {
        $(window).data("message", result.message);
        $("#linkStatus").html(loc.linkStatus.saved);
        setTimeout(function () {
          $("#linkStatus").html("");
        }, 1000); // Blank
        // after															// 5s
        if (callback) {
          callback(true, result);
        }
        if (!options.noReload) {
          var params = ns.getQueryParams();

          // Change URL if needed, when the ID is set
          if (result.message.id != params.messageId) {
            params.messageId = result.message.id;
            delete params.action;
            window.location.search = "?" + $.param(params);
          }
        }
      })
      .catch(function (xhr) {
        if (xhr.status == 401) {
          window.onLogin = function (result, callBack) {
            console.log("Registration succeded");

            self.saveMessage(
              {
                noReload: true,
                reattach: true
              },
              function (isOk, savresult) {
                if (
                  isOk &&
                  savresult &&
                  savresult.message &&
                  savresult.message.id
                ) {
                  console.log("Messsage saved and re-attached");
                  window.onLogin = null;
                  window.setTimeout(function () {
                    window.location.href = "./messages_list.html";
                    $(window).load(function () {
                      ns.showMessages(result, { reset: true });
                      ns.showMessages(savresult);
                    });
                  }, 300);
                  if (callBack) {
                    callBack(isOk, savresult);
                  }
                } else {
                  $("#signInWindow").toggleFade(false);
                  if (callBack) {
                    callBack(false, savresult);
                  }
                }
              }
            );
          };
          $(window).openLogin("reg", {
            noThanks: true,
            adminPerson: $("#sender-cell")
              .find(".personDisplay")
              .data("person")
          });
        } else {
          $("#linkStatus").html(loc.linkStatus.saveError);
          console.log("Error saving message: " + xhr);
          if (callback) {
            callback(false, xhr);
          }
          ns.logError(xhr);
        }
      });
  };

  /**
   * Get message window URL
   */
  ns.editor.messWindowUrl = function (message_type) {
    var url = window.location.toString();
    url = ns.setUrlParam(url, "action", "duplicate");
    url = ns.setUrlParam(
      url,
      "message_type",
      message_type ? message_type : "invoice"
    );
    return url;
  };

  /**
   * Save and change window url to duplicate.
   */
  ns.editor.saveAndMessWindow = function (popup, message_type) {
    if ($("html").is(".auth")) {
      if ($(window).data("action") == "edit") {
        this.saveMessage({}, function (isOk, result) {
          if (isOk && result && result.message && result.message.id) {
            popup.location = ns.editor.messWindowUrl(message_type);
          }
        });
      } else {
        popup.location = ns.editor.messWindowUrl(message_type);
      }
    } else {
      alert(loc.shouldBeAuth);
    }
  };

  ns.editor.historyAct = function () {
    ns.logEvent("Document", "history");

    var $hw = $("#historyWindow");
    $hw.toggleFade(true);
    $hw.find(".header .closeBt").on("click", function (e) {
      e.preventDefault();
      $(e.target)
        .closest("#historyWindow")
        .toggleFade(false);
    });
    var $bd = $hw.find("#historyEventList");
    var message = $(window).data("message");
    if (message) {
      var actNb = message.actions && message.actions.length;

      if (actNb > 0) {
        $bd.find("*").remove();
        var persons = $(window).data("persons");
        var users = $(window).data("users");

        // Person display label
        var personLabel = function (pObj) {
          var personStr = pObj.label;
          if (personStr && pObj.email) {
            personStr += " ( " + pObj.email + " )";
          } else if (pObj.email) {
            personStr = pObj.email;
          } else if (pObj.address && pObj.address.name) {
            personStr = pObj.address.name;
          }
          return personStr;
        };

        // Action person display label
        var actionActor = function (act) {
          var personStr;
          if (act.person && persons[act.person]) {
            personStr = personLabel(persons[act.person]);
          } else if (act.user && users[act.user]) {
            personStr = users[act.user];
          } else {
            personStr = "person:" + act.person + " user:" + act.user;
          }
          if (act.isConnectedUser) {
            personStr += " (" + loc.yourself + ")";
          }
          return personStr;
        };

        var actMap = {};
        for (var i = 0; i < actNb; i++) {
          actMap[message.actions[i].id] = message.actions[i];
        }

        for (var i = 0; i < actNb; i++) {
          var act = message.actions[i];
          if (act.type == "ACTION_CANCELED") {
            continue;
          }

          var dateStr = new Date(act.date * 1000).format(
            loc.date.dateTimeFormat
          );

          var label = loc.history[act.type];
          label = label.replace("{0}", actionActor(act));

          var $a = $(
            '<div data-actionId="' +
            act.id +
            '" class="action' +
            (act.canceledby ? " canceled" : "") +
            '"> <div class="date">' +
            dateStr +
            '</div><div class="label">' +
            label +
            "</div>"
          );

          if (act.canceledby && actMap[act.canceledby]) {
            var cancelAct = actMap[act.canceledby];
            var cancelDate = new Date(cancelAct.date * 1000).format(
              loc.date.dateFormat
            );
            var label = loc.history["ACTION_CANCELED"];
            label = label.replace("{0}", cancelDate);
            label = label.replace("{1}", actionActor(cancelAct));
            $a.append(' <span class="cancel">( ' + label + ")</span>");
          }
          if (act.cancelable) {
            $a.append(
              '<a href="#" class="cancelBt"><i class="fa fa-trash-o"></i></a>'
            );
          }

          // Display sendings
          var actLen = act.sendings ? act.sendings.length : 0;
          for (var j = 0; j < actLen; j++) {
            var snd = act.sendings[j];
            var label = loc.history.sending[snd.type] || snd.type;
            if (snd.person && persons[snd.person]) {
              var sp = persons[snd.person];
              label = label.replace("{0}", personLabel(sp));

              if (snd.type == "EMAIL" || snd.type == "OTHER") {
              } else if (snd.type == "FAX") {
                label = label.replace("{1}", sp.fax);
              } else if (snd.type == "POST") {
                label = label.replace(
                  "{1}",
                  sp.organisation.address +
                  " " +
                  sp.organisation.postal_code.city
                );
              } else if (snd.type == "SMS") {
                label = label.r | eplace("{1}", sp.phone);
              } else {
                label = label.replace("{1}", "#ERROR#");
              }
            } else if (!snd.person) {
              label = label.replace("{0}", '"' + loc.someone + '"');
            } else {
              label = label.replace("{0}", "person:" + snd.person);
              label = label.replace("{1}", "#ERROR#");
            }
            if (snd.comment) {
              label += "<br/>" + snd.comment;
            }
            var errorMessage = null;

            if (snd.errorCode) {
              errorMessage = loc.smtpStatus["generalError"];
              switch (snd.errorCode) {
                case 452:
                case 522:
                  errorMessage = loc.smtpStatus["boxFull"];
                  break;
                case 550:
                  errorMessage = loc.smtpStatus["badAddress"];
                  break;
                case 552:
                  errorMessage = loc.smtpStatus["tooBig"];
                  break;
                default:
                  if (500 > snd.errorCode) {
                    errorMessage = loc.smtpStatus["tempProblem"];
                  }
              }
              if (errorMessage) {
                errorMessage =
                  ' <span class="errMsg">' + errorMessage + "</span>";
              }
            }

            $a.append(
              '<div class="sending">' +
              label +
              (errorMessage ? errorMessage : "") +
              "</div>"
            );
          }

          // Display payments
          var payLen = act.payments ? act.payments.length : 0;
          for (var j = 0; j < payLen; j++) {
            var pay = act.payments[j];
            var texte = ns.formatAmount(pay.amount);
            if (loc.paymentTypes[pay.paymentType]) {
              texte += " " + loc.paymentTypes[pay.paymentType];
            }
            if (pay.description) {
              texte += " " + pay.description;
            }
            $a.append('<div class="payment">' + texte + "</div>");
          }

          $bd.append($a);
        }
      }
    }
  };

  $("#historyEventList").on(
    "click",
    ".cancelBt",
    function (e) {
      e.preventDefault();
      var $act = $(e.target).closest(".action");
      var actionId = $act.data("actionid");
      var ajHistoryOpt = {
        feedback: "#historyFeedback"
      };

      ns.ws(
        "PUT",
        "v1/messageAction/" + actionId + "/cancel",
        null,
        ajHistoryOpt
      ).then(function (result, options) {
        window.messageEditor.loadForm(result, true);
        $act.addClass("canceled");
        $act.find(".cancelBt").toggle(false);
        console.log("MessageAction cancel");
      });
    }.debounce(200, true)
  );

  // Close window
  ns.editor.closeAct = function () {
    this.doAfterSave(function () {
      ns.goHome();
    });
  };

  ns.editor.saveAct = function () {
    this.saveMessage(
      $("html").is(".auth")
        ? null
        : {
          reattach: true
        }
    );
  };

  ns.editor.modifyAct = function () {
    var message = $(window).data("message");
    if (message) {
      this.loadForm(
        {
          message: message,
          action: "edit"
        },
        true
      );
    } else {
      alert("No message data on page!");
    }
  };

  ns.editor.sendAct = function () {
    var that = this;
    if ($(window).data("action") == "edit") {
      this.saveMessage(
        {
          noReload: true
        },
        function (isOk, result) {
          that.loadActionScript("sending", "sending.js");
        }
      );
    } else {
      that.loadActionScript("sending", "sending.js");
    }
  };

  ns.editor.deleteAct = function () {
    var message = $(window).data("message");

    if ($("html").is(".auth") && message && message.id) {
      ns.ws("DELETE", "v1/message/" + message.id + "?onlyTrash=true");
    } else {
      alert(loc.shouldBeAuth);
    }
  };

  ns.editor.duplicateAct = function () {
    ns.logEvent("Document", "duplicate");
    var popup = window.open();
    this.saveAndMessWindow(popup, $(window).data("message_type"));
  };

  ns.editor.printPdfAct = function () {
    ns.logEvent("Document", "pdf");
    var self = ns.ec;

    if ($("html").is(".auth")) {
      this.doAfterSave(function (messageId) {
        var message = $(window).data("message");
        var fileName = message.message_number
          ? message.message_number
          : "commercialMessage";
        fileName += "-" + loc.locale;
        ns.newWindow(
          ns.serverUrl + "pdf-docm/mId-" + messageId + "/" + fileName + ".pdf",
          "_new",
          "scrollbars=yes"
        );
      });
    } else {
      if (!this.formToJson) {
        throw "formToJson not defined";
      }
      var data = this.formToJson(ns.getQueryParams());

      var fileNum = $("#message_number").getValue(),
        fileName = (fileNum || "document") + "-" + loc.locale;
      var postPdf = ns.newWindow(
        ns.clientUrl + "blank.html",
        "pdfWindow",
        "status=0,title=0,height=600,width=800,scrollbars=1"
      );
      if (postPdf) {
        postPdf.onload = function () {
          var bdy = this.document.body;
          bdy.innerHTML =
            '<form id="postPdf" method="POST" target="pdfWindow" action="' +
            ns.serverUrl +
            "pdf-docm/" +
            fileName +
            '.pdf"><input type="hidden" name="data" id="data"/><input type="submit"/></form>';
          $("#data", bdy)
            .val(JSON.stringify(data))
            .parent()
            .submit();
        };
      } else {
        console.log("pdfWindow window not oppened!");
      }
    }
  };
})();
