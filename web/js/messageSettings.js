"use strict";

ns.messageSettings = new Vue({
    el: '#message-settings-view',
    data: {
        prefs: {
            'allowModifyIssueDate': '',
            'allowModifyMessageNumber': '',
            'generateNumberOnIssue': '',
            'itemRefColumn': '',
            'UoMColumn': '',
            'priceVatIncluded': '',
            'noVat': '',
            'feeNote': '',
            'signDocument': '',
            'inalterableMode': '',
            'standardVatRate': '',
            'standardAdvanceRate': '',
            'paymentEndOfMonth': false,
            'standardPaymentDelay': ''
        },
        numberScheme: {
            "invoice": { "next": "", "scheme": "", "last": "" },
            "proforma_invoice": { "next": "", "scheme": "", "last": "" },
            "purchase_order": { "next": "", "scheme": "" },
            "quote": { "next": "", "scheme": "", "last": "" },
            "credit_note": { "next": "", "scheme": "", "last": "" },
            "letter": { "next": "", "scheme": "", "last": "" }
        },
        loading: true,
        modifyNumberDisabled: '',
        generation_mode: 'issue',
        editNumberOpen: false,
        currentEdition: '',
        currentEditedType: '',
        mustSubscribeForSign: '',
        credit_note_use_invoice: true,
        standardPaymentDelay: '',
        paymentEndOfMonth: ''
    },
    methods: {
        modify: function (e, field, message_type) {
            var that = this,
                req = {};
            req[field] = e.target.value;

            ns.ws('POST', 'v1/messageNumber?message_type=' + message_type, req).then(function (response) {
                var vals = response[message_type];
                if (vals)
                    that.numberScheme[message_type] = vals;
            });
        }.debounce(),
        toggleSchemeOverride: function (message_type) {
            var req = { schemeOverride: this.credit_note_use_invoice ? 'INVOICE' : "reset" };
            ns.ws('POST', 'v1/messageNumber?message_type=' + message_type, req);
        }.debounce(),
        openEdit: function (editedType) {
            this.editNumberOpen = true;
            this.currentEditedType = editedType;
            this.currentEdition = this.numberScheme[editedType].scheme;
        },
        addParam: function (par) {
            this.currentEdition += par;
        },
        submitEdit: function () {
            this.numberScheme[this.currentEditedType].scheme = this.currentEdition;
            var that = this;
            ns.ws('POST', 'v1/messageNumber?message_type=' + this.currentEditedType, { scheme: this.currentEdition }).then(function (response) {
                var vals = response[that.currentEditedType];
                if (vals)
                    that.numberScheme[that.currentEditedType] = vals;
                that.editNumberOpen = false;
            });
        },
        updateStdPaymentDelay: function(){
            ns.ws("POST", "v1/preference", {});
        }
    },
    watch: {
        generation_mode: function (val) {
            ns.resetMessages();
            ns.ws('POST', 'v1/preference', { 'generateNumberOnIssue': val === 'issue' });
        }
    },
    created: function () {

        // Preference values load
        var that = this,
            keys = Object.keys(this.prefs),
            prefsLoad = false,
            numLoad = false;
        ns.ws('GET', 'v1/preference/' + keys.join('+'), null, { handleError: true }).then(function (result) {
            for (var i = 0; i < keys.length; i++) {
                var key = keys[i];
                that.prefs[key] = result[key];
            }
            that.generation_mode = that.prefs.generateNumberOnIssue === true ? 'issue' : 'creation';
            prefsLoad = true;
            if (numLoad) that.loading = false;
        });

        // Number scheme value load
        var date = new Date(),
            month = (date.getMonth() + 1).toString().padStart(2,"0"),
            day = date.getDate().toString().padStart(2,"0");
        ns.ws('GET', 'v1/messageNumber?date=' + encodeURIComponent(date.getFullYear() + '-' + month + '-' + day), null, { handleError: true }).then(function (result) {
            that.numberScheme = result;
            numLoad = true;
            that.credit_note_use_invoice = result.credit_note.override == 'INVOICE';
            if (prefsLoad) that.loading = false;
        });

        ns.ws('GET', 'v1/session?canSignWith=all', null, { handleError: true }).then(function (result) {
            that.mustSubscribeForSign = result.failed_sign_prerequisites && result.failed_sign_prerequisites.indexOf("membership") > -1;
        });
    }
});