"use strict";

ns.logEvent('Document', 'openPaymentType');
console.log('Loading payment type panel');

ns.paymentType = {

    typeKeys: ['stripe'],

    // Go to payment system
    goToPayment: function(paymentType) {
        if(paymentType=='stripe'){
            this.doStripe();
        }
    },

    load: function(messageId, topay, currency) {

        $(window).openModal(
            'paymentTypeWindow',
            'payment-type',
            function($modal, init) {
                if (init) {
                    // Get preferences
                    var message = $(window).data('message');
                    
                    var prefsKs= [],
                        tyk = ns.paymentType.typeKeys,
                        loadStripe = false;
                    for (var tIndex = 0; tIndex < tyk.length; tIndex++) {
                        var k = tyk[tIndex];
                        prefsKs.push(k + 'FeesIncluded');
                        prefsKs.push(k + 'Allowed');
                    }
                    
                    ns.ws('GET', 'v1/preference/' + prefsKs.join('+') + '?scope=SENDER_ORG&msgId=' + message.id)
                        .then(function(result) {
                            var validCount = 0,
                                validKey = null,
                                validfeesIncled = false;
                            for (var tIndex = 0; tIndex < ns.paymentType.typeKeys.length; tIndex++) {
                                var typeKey = ns.paymentType.typeKeys[tIndex],
                                    feesInclK = result[typeKey + 'FeesIncluded'],
                                    allowK = result[typeKey + 'Allowed'],
                                    feesIncled = feesInclK != undefined && feesInclK,
                                    allowed = allowK != undefined || allowK;

                                if (allowed) {
                                    validCount++;
                                    validKey = typeKey;
                                    validfeesIncled = feesIncled;
                                }

                                $modal
                                    .find('#' + typeKey + 'Bt').toggle(allowed)
                                    .find('.included').toggle(feesIncled).end()
                                    .find('.charged').toggle(!feesIncled);

                                if (typeKey == 'stripe') {
                                    ns.paymentType.loadStripe();
                                }
                            }

                            // Go directly to payment if one solution with fees included
                            if (validCount == 1 && validfeesIncled && validKey != 'paypal') {
                                ns.paymentType.goToPayment(validKey);
                            }
                        });

                    $(".paymentBt", $modal).click(function(e) {
                        var ptype = $(this).closest('a').data('payment-type');
                        if (ptype != 'paypal') {
                            e.preventDefault();
                            ns.paymentType.goToPayment(ptype);
                        }
                    });
                }
            });
    },
    /* Load stripe if needed */
    loadStripeImp: function(deferred){
        var bt = document.querySelector("#stripeBt"),
        ai = bt.querySelector(".ajax-indicator");
        bt.disabled = true;
        ai.classList.add("on");
        ns.stripeloader.load().then(function(){
            ai.classList.remove("on");
            bt.disabled = false;
            deferred.resolve();
        }).catch(function(){
            ns.showMessages({errors: [{message: "No Stripe public key"}]});
            ai.classList.remove("on");
            deferred.reject();
        });
    },
    /* Load stripeloaded and stripe if needed */
    loadStripe: function(){
        var deferred = $.Deferred();
        if(ns.stripeloader){
            this.loadStripeImp(deferred);
        }else{
            ns.ld(['../js/stripeloader.js'], function(){
                ns.paymentType.loadStripeImp(deferred);
            });
        }
        return deferred.promise();
    },
    /* Present Stripe card form */
    doStripe: function(){
        if(ns.stripeloader.stripe){
            document.querySelector("#paymentBts").style.display='none';
            document.querySelector("#stripe-form").style.display='block';
            
            var onSubmit = $.Deferred();
            onSubmit.then(function(token){
                // Send token to server
                ns.paymentType.onSuccess(true);
            });
            
            ns.stripeloader.propose(onSubmit).catch(function(errorMsg){
                ns.showMessages({errors: [{message: "Stripe form init error, warn the administrator: " + errorMsg}]});
            });
        }else{
            var bt = document.querySelector("#stripeBt");
            if(!bt.disabled)
                ns.showMessages({errors: [{message: "Stripe not loaded! Warn the administrator."}]});
        }
    },
    onSuccess: function(result) {
        console.log('Payment type open success');
    }
};

//# sourceURL=/static/js/payment-type.js