"use strict";

(function () {

    ns.logEvent('Writing', 'import');
    console.log('Loading view writing import panel');

    ns.importWriting = {
        /* Parse tabular data from a string */
        parseTabularData: function (data) {
            // Detect fixed length table => Head, col header, table data and footer (TODO)
            // If nothing => detect and fix line break in " block
            return this.fixIndented(this.parseTable(this.fixLineBreakInQuoteBlock(data), "\t"));
        },
        // Detect and fix line break in " block
        fixLineBreakInQuoteBlock: function (data) {
            var inQuote = false,
                resp = '';
            for (var i = 0; i < data.length; i++) {
                var c = data[i];
                if (c === '"') inQuote = !inQuote;
                else if (c != "\n" || !inQuote) resp += c;
            }
            return resp;
        },
        // Parse text into table of strings
        parseTable: function (data, colSep) {
            var lines = data.split("\n"),
                resp = [];
            for (var i = 0; i < lines.length; i++) {
                var cells = lines[i].split(colSep),
                    l = [];
                for (var j = 0; j < cells.length; j++) {
                    l.push(cells[j].trim());
                }
                resp.push(l);
            }
            return resp;
        },
        // Merge indented cells into one line
        fixIndented: function (table) {
            var resp = [],
                prevRow = null;
            for (var i = 0; i < table.length; i++) {

                if (prevRow) {
                    if (table[i][0]) {
                        resp.push(prevRow);
                        prevRow = table[i];
                    } else {
                        for (var j = 0; j < table[i].length; j++) {
                            var c = table[i][j];
                            if (c && c.length > 0) {
                                prevRow[j] += ("\n" + c);
                            }
                        }
                    }
                } else {
                    prevRow = table[i];
                }
            }
            return table;
        },
        // Transform table into array of writing
        parseWriting: function (table, dv) {
            var resp = [];

            for (var i = 0; i < table.length; i++) {

                var isDebit = table[i][2],
                    amt = Math.abs(ns.parseNumber(isDebit ? table[i][2] : table[i][3]));
                if (!amt) {
                    continue;
                }
                var wtg = {
                    date: ns.parseDate(table[i][0], loc.date.dateTimeFormat) || dv.date,
                    status: "PENDING",
                    label: table[i][1] || dv.label,
                    proof: table[i][4] || dv.proof,
                    accounting_journal_id: dv.journalId,
                    moves: []
                };
                if(table[i][5]){
                    wtg.moves.push({
                        "amount": amt - parseFloat(table[i][5]),
                        "account": dv.debitAccount,
                        "direction": isDebit ? "DEBIT" : "CREDIT",
                        "asset": dv.asset
                    });
                    wtg.moves.push({
                        "amount": parseFloat(table[i][5]),
                        "account": dv.vatAccount,
                        "direction": isDebit ? "DEBIT" : "CREDIT",
                        "asset": dv.asset
                    });
                    wtg.moves.push({
                        "amount": amt,
                        "account": dv.creditAccount,
                        "direction": isDebit ? "CREDIT" : "DEBIT",
                        "asset": dv.asset
                    });
                } else {
                    wtg.moves.push({
                        "amount": amt,
                        "account": dv.debitAccount,
                        "direction": isDebit ? "DEBIT" : "CREDIT",
                        "asset": dv.asset
                    });
                    wtg.moves.push({
                        "amount": amt,
                        "account": dv.creditAccount,
                        "direction": isDebit ? "CREDIT" : "DEBIT",
                        "asset": dv.asset
                    });
                }
                resp.push(wtg);
            }
            return resp;
        },
        load: function () {

            $(window).openModal('importWritingWindow', 'importWriting', function ($modal, init) {
                if (init) {
                    window.impWriV = new Vue({
                        el: '#importWriting',
                        data: {
                            defaultValues: {
                                date: Math.round(Date.now() / 1000),
                                status: 'PENDING',
                                label: 'Ecriture importée',
                                proof: '',
                                journalCode: 'OD',
                                journalId: 1,
                                debitAccount: ns.getLocalPref("importDebitAccount", { "label": "Change" }),
                                creditAccount: ns.getLocalPref("importCreditAccount", { "label": "Change" }),
                                vatAccount: {label: "TVA",path:"445"},
                                asset: ns.getLocalPref("importAsset", { "label": "Change" }),
                            },
                            dataFile: '',
                            importData: '',
                            writings: [],
                            submited: false,
                            defAsset: ns.accounting.defAsset,
                            ajaxOpt: {
                                        indicator: '#importWritingWindow .ajax-indicator',
                                        feedback: '#editWritingWindow .feedBack'
                                    }
                        },
                        methods: {
                            submitImport: function (event) {
                                this.submited = true;
                                var isOk = this.defaultValues.label && this.defaultValues.status && this.defaultValues.date;

                                ns.logEvent('Writing', 'import');

                                if (isOk) {
                                    console.log('Import writing');
                                    ns.ws('POST', 'v1/accounting/writing/import', this.writings, this.ajaxOpt).then(function (result) {
                                        $('#importWritingWindow').toggleFade(false);
                                    });
                                }
                            },
                            emptyErr: function (val) {
                                return this.submited && (val == null || val == '') ? 'errField' : '';
                            },
                            changeVal: function (prefKey, $event) {
                                var target = { 'path': $event.path, 'label': $event.label };
                                ns.setLocalPref(prefKey, target);
                                return target;
                            },
                            parse: function () {
                                var table = ns.importWriting.parseTabularData(this.importData);
                                this.writings = ns.importWriting.parseWriting(table, this.defaultValues);
                                console.log(table);
                            },

                            journalChanged: function() {
                                ns.ws('GET', 'v1/accounting/journal?code=' + this.defaultValues.journalCode, null, this.ajaxOpt).then((result) => {
                                    this.defaultValues.journalId = result.journal && result.journal.id;
                                });
                            }
                        },
                        mounted: function () {
                            var that = this;
                            this.dp = ns.setDatePicker('.datePicker'),
                                this.dp.on('statechange', function (_, piker) {
                                    that.defaultValues.date = Math.round(piker.state.selectedDate.getTime() / 1000);
                                }); // Change model when dp changes
                        }
                    });
                }
            });
        },
        onSuccess: function (result) {
            console.log('Writing edition success');
        }
    };
}());

