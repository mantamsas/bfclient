"use strict";

ns.registerview = new Vue({
    el: '#register-view',
    data: {
        user: { "login": '', "persistent": true, "passwordVis": null, "password": null },
        pwVis: false,
        licenceAccepted: true,
        register_stripe: true,
        ajOpt: { feedback: '#signInForm .feedBack', indicator: '#regBt .ajax-indicator' }
    },
    mounted: function () {
        var that = this,
            params = ns.getQueryParams();
        this.register_stripe = params.register_stripe;
        if (params.email) {
            this.user.login = params.email;
        }
        ns.ws('GET', 'v1/session', null, this.ajOpt).then(function (result) {
            var con = result.status == 'ok' && result.connected == true;
            if (con) {
                window.location.href = that.nexturl;
            }
        });
        
    },
    methods: {
        togglePwVis: function (system) {
            if (this.pwVis) {
                this.user.password = this.user.passwordVis;
            } else {
                this.user.passwordVis = this.user.password;
            }
            this.pwVis = !this.pwVis;
        },
        openAuth: function (system) {
            if (!this.licenceAccepted) {
                return;
            }
            ns.oauthSignIn(system, { action: 'reg', nextUrl: window.location.href });
        },
        passAuth: function () {
            if (!this.licenceAccepted) {
                return;
            }
            var that = this;
            ns.resetMessages(this.ajOpt);

            this.user.persistent = true;
            this.user.validationUrl = window.location.origin + ns.locDir() + 'id-settings.html';
            ns.ws('POST', 'v1/session?action=register', this.user, this.ajOpt).then(function (result) {
                if (result.status == 'ok' && result.connected == true) {
                    ns.resetMessages(that.ajOpt);
                    window.location.href = that.nexturl;
                }
            });
        }
    },
    computed :{
        nexturl: function() {
            return 'register_identity.html' + (this.register_stripe ? '?register_stripe=true' : '');
        }
    }
});

// Adapt top menu to register
var topMenu = document.getElementById('loginTopMenu'),
    topCont = topMenu.parentNode,
    topLoginLnk = document.getElementById('topLoginLnk');

topCont.innerHTML = '';
topCont.append(topLoginLnk);
topLoginLnk.onclick = function() {
    $(window).openLogin('log');
};