"use strict";

ns.accountReport = new Vue({
  el: "#accountReport",
  data: {
    moves: [],
    loading: false,
    noContent: false,
    count: 0,
    hasCrit: false,
    crit: {
      label: "",
      accountPath: "",
      status: "",
      debit: "",
      credit: "",
      assetPath: "",
      from: "",
      periodId: ns.accounting.defPeriod && ns.accounting.defPeriod.value,
      page: ""
    },
    page: 0,
    pagesize: 2000,
    keepFeedback: true,
    filterOn: false,
    accountLabel: "",
    assetLabel: ""
  },
  mounted: function () {
    var that = this,
      dp = ns.setDatePicker("#dateCrit"),
      params = ns.getQueryParams();

    this.crit.assetPath =
      ns.accounting.defAsset && ns.accounting.defAsset.value;

    if (params.accountPath) {
      this.crit.accountPath = params.accountPath;
      this.accountLabel = params.accountPath;
    } else {
      var accDef = ns.getLocalPref("defAccount");
      if (accDef) {
        this.crit.accountPath = accDef.value;
        this.accountLabel = accDef.text;
      }
    }

    if (this.crit.accountPath && this.crit.assetPath) this.loadList();

    dp.on("statechange", function (_, piker) {
      that.crit.from = Math.round(piker.state.selectedDate.getTime() / 1000);
    });
    ns.nextAd();
  },
  methods: {
    openWriForm: function (event, writObj, clone) {
      var that = this;
      ns.requires("editWriting", "editWriting.js").then(function (formObj) {
        ns.ws("GET", "v1/accounting/writing/" + writObj.id).then(function (
          result
        ) {
          formObj.load(result.writing, false);
          formObj.onSuccess = function (result) {
            console.log("Account report add writing success");
            that.loadList();
          };
        });
      });
    },
    // Reload if crit changes
    changeCrit: function (name, val) {
      console.log("Crit " + name + " changed " + val);
      if (this.crit[name] != val) {
        this.crit[name] = val;
        this.loadList();
      }
    },
    // Load from database
    loadList: function (pageNb) {
      var that = this;

      if (!this.crit.accountPath) {
        this.moves = [];
        this.count = 0;
        this.noResults = true;
        return;
      }

      this.loading = true;
      if (pageNb) {
        this.page = pageNb;
      }

      if (!this.page) {
        this.page =
          (window.location.hash &&
            parseInt(window.location.hash.replace("#", ""))) ||
          0;
      }

      function addCrit(name) {
        var cr = that.crit[name];
        if (cr) {
          url +=
            "&" + name + "=" + encodeURIComponent(cr.trim ? cr.trim() : cr);
        }
      }

      // Get data from filter
      var url = "v1/accounting/accountReport?pageSz=" + this.pagesize;
      addCrit("page");

      var urlLen = url.length;

      addCrit("label");
      addCrit("accountPath");
      addCrit("status");
      addCrit("debit");
      addCrit("credit");
      addCrit("assetPath");
      addCrit("periodId");
      addCrit("from");

      this.hasCrit = url.length > urlLen;

      // Query and render result
      ns.ws("GET", url, null, {
        keepFbk: this.keepFeedback
      })
        .then(function (result) {
          if (
            !result.accountReport ||
            !result.accountReport.moves ||
            result.accountReport.moves.count == 0
          ) {
            that.noResults = true;
            that.moves = [];
          } else {
            // Update table
            that.moves = result.accountReport.moves;
            that.count = that.moves.length;
            that.noResults = false;
          }
          that.loading = false;
        })
        .catch(function (xhr) {
          ns.showMessages(xhr.jsonResp);
          console.log("Accounting report list load error");
          that.loading = false;
          ns.logError(xhr);
        });
    },
    formatAmount(value) {
      return value != null ? value.formatMoney().replace(" ", "&nbsp;") : "";
    }
  }
});
