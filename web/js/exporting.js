"use strict";

ns.exporting = {};

$(document).ready(function () {

    var currentDate = Math.round(Date.now()),
        toDp = ns.setDatePicker("#exportTo", currentDate),
        frDp = ns.setDatePicker("#exportFrom", currentDate - 31536000000); // One year in microseconds

    /*
    function setTimeFrame(key){
    	"CURRENT_WEEK"
    	"LAST_WEEK"
    	"LAST7_DAYS"
    	"CURRENT_MONTH"
    	"LAST_MONTH"
    	"LAST31_DAYS"
    	"CURRENT_YEAR"
    	"LAST_YEAR"
    }
    setTimeFrame('CURRENT_MONTH');
	
    $('select[name=timeFrameSelector]').on('change', function(e){
    	var key = $(this).val();
    	if(key === 'CUSTOM_DATES'){
    		$('#datesRegion').toggle(true);
    	}else{
    		$('#datesRegion').toggle(false);
    		setTimeFrame(key);
    	}
    });*/

    $('#preview').on('click', function (e) {
        e.preventDefault();
        var url = '',
            exportFrom = frDp.state.selectedDate / 1000,
            exportTo = toDp.state.selectedDate / 1000,
            typeCrit = $('select[name=typeCrit]').val();
        url += ('&from=' + Math.round(exportFrom));
        url += ('&to=' + Math.round(exportTo + 86400)); // For end of day
        url += ('&typeCrit=' + typeCrit);
        url += ('&includeDraft=' + $('input[name=includeDraft]').is(':checked'));
        url += ('&includeOutbox=' + $('input[name=includeOutbox]').is(':checked'));

        $('#launch').attr('href', ns.serverUrl + 'api/v1/pdfExport?action=archive' + url).toggle(true);
        // Query and render result
        ns.ws('GET', 'v1/pdfExport?action=preview' + url, null, {
            indicator: '.rightMenu .ajax-indicator'
        })
            .then(function (result) {
                var $tbody = $('#previewTable tbody');

                // Update table
                if (result.preview) {
                    var messCnt = result.preview.length;
                    $('tr', $tbody).remove();

                    for (var i = 0; i < messCnt; i++) {
                        var message = result.preview[i];
                        var row = '<tr><td>' + message.message_number + '</td><td>' + message.subject + '</td></tr>';
                        $tbody.append($(row));
                    }
                }
            });
    });
});