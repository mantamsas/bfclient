"use strict";

$(document).ready(function() {

    var currentDate = Math.round(Date.now()),
        toDp = ns.setDatePicker("#exportTo", currentDate),
        frDp = ns.setDatePicker("#exportFrom", currentDate - 31536000000); // One year in microseconds

    $('#launch').on('click', function(e) {
        var now = new Date(),
            exportFrom = frDp.state.selectedDate / 1000,
            exportTo = toDp.state.selectedDate / 1000,
            url = '?from=' + Math.round(exportFrom) + '&to=' + Math.round(exportTo + 86400); // For end of day

        $(this).attr('href', ns.serverUrl + 'api/v1/fecExport/FEC' + ns.formatDate(now, 'yyyymmdd') + '.TXT' + url);
    });
});