(function () {

    ns.stripeloader = {};

    // Preload Stripe
    ns.stripeloader.loadstripe = function () {
        return ns.ldone("Stripe", "https://js.stripe.com/v3/", true);
    };

    ns.stripeloader.getkey = function () {
        return new Promise(function (resolve, reject) {
            ns.ws("GET", "v1/stripe/key")
                .then(function (result) {
                    if (result.publicKey) {
                        ns.stripeloader.pk = result.publicKey;
                        resolve(result.publicKey);
                    } else {
                        ns.showMessages({ errors: [{ message: "No Stripe public key" }] });
                        reject();
                    }
                })
                .catch(function (xhr) {
                    reject(xhr);
                });
        });
    };

    ns.stripeloader.load = function (opts) {
        return new Promise(function (resolve, reject) {
            Promise.all([ns.stripeloader.loadstripe(), ns.stripeloader.getkey()])
                .then(function () {
                    ns.stripeloader.stripe = Stripe(ns.stripeloader.pk, opts);
                    console.log("Stripe init ok");
                    resolve(ns.stripeloader.stripe);
                })
                .catch(function (e) {
                    reject(e);
                });
        });
    };

    var handleResult = function (result) {
        if (result.error) {
            document.write(result.error.message);
        }
    };

    let params = ns.getQueryParams();
    if (params.message_id) {
        ns.stripeloader.load().then((stripe) => {

            ns.ws("POST", "v1/stripe/checkout", { message_id: params.message_id, web_base: window.location.origin + ns.clientUrl })
                .then(function (data) {
                    stripe.redirectToCheckout({ sessionId: data.stripeCheckoutSessionId, }).then(handleResult);
                });
        }
        );
    } else {
        document.write("URL parameter required: message_id");
    }

})();