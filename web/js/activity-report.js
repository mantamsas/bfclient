"use strict";

ns.getOverview = function (instance) {
    if (!instance.overview) {
        instance.overview = {

            loadGraph: function () {
                var visQEncoded = encodeURI(this.currentQuery);
                ns.ws('GET', 'v1/activity-report?tqx=reqId:0;out:json&tq=' + visQEncoded)
                    .then(function (queryResult) {
                        $('#loading').toggle(false);
                        $('#speadSheetEx').toggle(true);
                        var dataTable = new google.visualization.DataTable(queryResult.table);
                        if (dataTable) {
                            var colNb = dataTable.getNumberOfColumns();

                            var colIndex = {};
                            for (var colIdx = 0; colIdx < colNb; colIdx++) {
                                var colId = dataTable.getColumnId(colIdx)
                                colIndex[colId] = colIdx;
                            }

                            // table
                            var tableView = new google.visualization.DataView(dataTable);
                            var toShow = [];

                            for (var colIdx = 0; colIdx < colNb; colIdx++) {
                                var colId = dataTable.getColumnId(colIdx);
                                if (colId != 'unit' && colId != 'currency' /* && colId != 'totalwithouttaxes' && colId != 'tax' */) {
                                    toShow.push(colIdx);
                                }
                            }

                            tableView.setColumns(toShow);
                            var table = new google.visualization.Table(document.getElementById('tableContainer'));
                            var cssClassNames = {
                                'headerRow': 'header',
                                'tableRow': 'data',
                                'oddTableRow': 'data',
                                'selectedTableRow': 'n',
                                'hoverTableRow': 'n',
                                'headerCell': 'n',
                                'tableCell': 'n'
                            };

                            table.draw(tableView, {
                                width: '100%',
                                cssClassNames: cssClassNames
                            });

                            // Chart
                            var chartView = new google.visualization.DataView(dataTable);
                            toShow = [];
                            var xCat;
                            for (var colIdx = 0; colIdx < colNb; colIdx++) {
                                var colId = dataTable.getColumnId(colIdx);
                                if (!xCat) {
                                    xCat = colId;
                                    toShow.push(colIdx);
                                } else if (colId === 'totalwithouttaxes' || colId === 'tax') {
                                    toShow.push(colIdx);
                                }
                            }
                            chartView.setColumns(toShow);
                            var options = {
                                isStacked: true,
                                backgroundColor: '#f2f2f2'
                            };
                            var chart = new google.visualization.ColumnChart(document.getElementById('chartContainer'));
                            chart.draw(chartView, options);

                            var components = [{
                                type: 'html',
                                datasource: ns.serverUrl + 'api/v1/activity-report?tq=' + visQEncoded
                            },
                            {
                                type: 'csv',
                                datasource: ns.serverUrl + 'api/v1/activity-report?tq=' + visQEncoded
                            }
                            ];

                            // Query draft numbers and show mass send form if needed
                            var rowNb = dataTable.getNumberOfRows();
                            ns.ws('GET', 'v1/activity-report?includeDraft=true&tqx=reqId:0;out:count&tq=' + visQEncoded)
                                .then(function (result) {
                                    var draftCount = result && result.count;
                                    $('#autoSend').toggle(rowNb * 3 < draftCount);
                                });
                        }
                    });

                var exportUrl = ns.serverUrl + 'api/v1/activity-report?tqx=reqId:0;out:tsv-excel&tq=' + visQEncoded;
                $('#speadSheetEx').attr('href', exportUrl);
            },

            currentQuery: "select * where message_type='INVOICE' and issue_date < 98989898909 group by month"
        };
    }
    return instance.overview;
};

$(document).ready(function () {
    google.charts.load("current", {
        packages: ['corechart', 'table']
    });
    google.charts.setOnLoadCallback(function () {
        ns.getOverview(window).loadGraph("select * where message_type='INVOICE' and issue_date < 98989898909 group by month");
    });

    ns.ws('GET', 'v1/priceListMembership/PRO|ENTERPRISE/check').then(function (result) {
        if (!result || !result.isMember) {
            console.log('Activity report disabled due to lack of membership');
            // TODO: Disable
        }
    });

    $('#dataParamLink').click(function (e) {
        $(window).openModal(
            'chartWindow',
            'chartOptions',
            function ($modal) {
                var frDp = ns.setDatePicker("#chartFr"),
                    toDp = ns.setDatePicker("#chartTo"),
                    now = Date.now();

                toDp.setState({
                    selectedDate: now
                });
                now = now - (31536000000); // One year in microseconds
                frDp.setState({
                    selectedDate: now
                });

                $(".mainact", $modal).click(function (e) {
                    e.preventDefault();
                    $('#loading').toggle(true);

                    var $f = $(e.target).closest('form');

                    var mType = $('#message_type').val(),
                        vizQuery = "where message_type = '" + mType + "'";

                    var from = frDp.state.selectedDate;
                    if (from) {
                        vizQuery += " and issue_date >= " + Math.round(frDp.state.selectedDate.setHours(0, 0, 0, 0) / 1000);
                    }

                    var to = toDp.state.selectedDate;
                    if (to) {
                        vizQuery += " and issue_date <= " + Math.round(toDp.state.selectedDate.setHours(23, 59, 59, 999) / 1000);
                    }

                    var group = $("input[name='group']:checked").map(function () {
                        return $(this).val();
                    }).get();

                    var step = $("input[name='tempGroup']:checked").val();
                    if (step)
                        group[group.length] = step;

                    if (group.length > 0) {
                        vizQuery += ' group by ' + group;
                    }
                    console.log('vizQuery: ' + vizQuery);

                    var ov = ns.getOverview(window);
                    ov.currentQuery = vizQuery;
                    ov.loadGraph();
                    $modal.toggleFade(false);

                    for (var i = 0; i < group.length; i++) {
                        if (loc[group[i]])
                            group[i] = loc[group[i]];
                    }
                    document.getElementById('explain').innerHTML = ns.replaceParams(loc.statFeedback, [loc.messageTypes[mType], ns.formatDate(frDp.state.selectedDate), ns.formatDate(toDp.state.selectedDate), group.join(', ')]);
                });
            });
    });

    $('#trigMassSend').click(function (e) {
        ns.ws('PUT', 'v1/message_list?action=markDraftSent&delay=604800', null, {
            indicator: '#autoSend .ajax-indicator'
        })
            .then(function (result) {
                $('#autoSend').toggle(false);
                $('#autoSendResult').toggle(true);
                $('#sendCount').html(result && result.count);
            });
    });

    $('#afterMassSend').click(function (e) {
        ns.getOverview(window).loadGraph();
    });

    $('.closeLnk').click(function (e) {
        $(this).closest('.placeHolder').toggle(false);
    });
});