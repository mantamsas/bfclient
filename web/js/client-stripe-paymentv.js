"use strict";

ns.logEvent("Document", "openClientStripePayment");

ns["client-stripe-payment"] = {
	props: {
		"message_id": Number,
		"to_pay": Number,
		"currency": String,
		"payexpl": String
	},
	template: "#client-stripe-paymentComp",
	data: function () {
		return { loading: false }
	},
	methods: {
		submitCard: function () {
			this.loading = true;

			var that = this;

			ns.stripeloader.stripe
				.createToken(ns.stripeloader.card)
				.then(function (result) {
					if (result.error) {
						var errorElement = document.getElementById("payment-errors");
						errorElement.textContent = result.error.message;
						that.loading = false;
					} else {
						ns.logEvent("ClientPayment", "stripeOk");
						var tok = result.token;
						console.log("Submit card with token");
						console.log(tok);

						ns.ws('POST', 'v1/stripe/charge?messageId=', null,
							{ indicator: indicator, feedback: "#clientStripePaymentWindow .feedBack", noFbOnSuccess: true })
							.then(function (result) {
								ns.logEvent("ClientPayment", "bfOk");
								result.infos = result.infos || [];
								result.infos.push({ message: successMsg });

								// Log analytics
								/*var stripeRep = result.stripe,
									transac = stripeRep.subscription,
									tax = (stripeRep.price * stripeRep.tax) / 100,
									qte =
										stripeRep.frequency == "year"
											? 12
											: stripeRep.frequency == "semester"
												? 6
												: 1;
								ns.gaAction("ecommerce:addTransaction", {
									id: transac,
									revenue: stripeRep.price + tax,
									tax: tax
								});
								ns.gaAction("ecommerce:addItem", {
									id: transac,
									sku: stripeRep,
									price: stripeRep.price / qte,
									quantity: qte
								});
								ns.gaAction("ecommerce:send");*/
								that.$emit("paid", result);
							}).catch(function (err) {
								that.loading = false;
								ns.logEvent("ClientPayment", "bfKo");
								ns.logError(err);
							});
					}
				})
				.catch(function (errorObj) {
					ns.showMessages({ errors: [errorObj] }, null, null, true);
					ns.logEvent("ClientPayment", "stripeKo");
					ns.logError(errorObj);
					that.loading = false;
				});
		},
	}
};

//# sourceURL=/static/js/client-stripe-paymentv.js
