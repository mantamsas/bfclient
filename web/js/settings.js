$(document).ready(function() {

    function displayAuth(system) {
        ns.ws('GET', 'v1/oauth?system=Google')
            .then(function(result) {
                var connOk = result && result.auth && result.auth[0] && result.auth[0].system === "GOOGLE";
                if (connOk) {
                    $('#googleEmail').html(result.auth[0].label);
                }
                $('.deconnect' + system + ', .reconnect' + system).toggle(connOk);
                $('.connect' + system).toggle(!connOk);
            });
    }

    $('.oauth-signin').click(function(e) {
        var $t = $(this);
        var sys = $t.data('system');
        window.onLogin = function() {
            displayAuth(sys);
        };
        ns.oauthSignIn(sys);
    });

    $('.oauth-signout').click(function(e) {
        var $t = $(this);
        var system = $t.data('system');
        ns.ws('DELETE', 'v1/oauth?system=' + system)
            .then(function(result) {
                $('.deconnect' + system + ', .reconnect' + system).toggle(false);
                $('.connect' + system).toggle(true);
            });
    });

    var $ge = $('#googleEmail');
    if ($ge.lenght != 0) {
        displayAuth();
    }

    function onModifyMessageNumChoice($mNumEditableCb) {
        if ($mNumEditableCb.is(':checked')) {
            $('input[name=generateNumberOnIssue]')
                .prop("checked", false)
                .prop('disabled', true);
        } else {
            $('input[name=generateNumberOnIssue]')
                .prop('disabled', false);
        }
    }

    onModifyMessageNumChoice($('input[name=allowModifyMessageNumber]'));

    $('input[name=allowModifyMessageNumber]').on('change', function(e) {
        onModifyMessageNumChoice($(this));
    });
});