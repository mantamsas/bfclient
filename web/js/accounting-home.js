"use strict";

ns.addEvent(document, '#fecExport', 'click', function () {

});

ns.accountingHome = new Vue({
    el: "#accountingHome",
    data: {
        periodId: ''
    },
    mounted: function () {
        this.periodId = ns.accounting.defPeriod.value || ns.getLocalPref('defPeriod');
    },
    methods: {
        changeAccPeriod: function (theId) {
            this.periodId = theId;
            ns.accounting.defPeriod.value = theId;
            ns.setLocalPref('defPeriod', theId);
        },

        formatAmount: function (value) {
            return value ? value.formatMoney().replace(" ", "&nbsp;") : "";
        },

        launchFec: function () {
            var now = new Date()
            window.location.href = ('href', ns.serverUrl + 'api/v1/fecAccountExport/FEC' + ns.formatDate(now, 'yyyymmdd') + '.TXT' + '?periodId=' + this.periodId + '&pending=true');
        }
    }
});