"use strict";

(function () {

    ns.logEvent('Document', 'open-sending');
    console.log('Loading sending panel');

    ns.sending = {
        load: function (messageId, topay, currency) {
            $(window).openModal('sendingWindow', 'sending', function ($modal, init) {
                if (init) {
                    var now = Math.round(Date.now()),
                        dp = ns.setDatePicker("#sendingIssueDate", now),
                        ssd = ns.setDatePicker("#sendingStartDate", now),
                        sed = ns.setDatePicker("#sendingEndDate", now + 31536000000);

                    var $pd = $('.recipientPerson .personDisplay');
                    $pd.makeAddressEditable().on('change', function (e) {
                        var person = $(this).data('person');
                        fillSendData(person);
                    });

                    switchSendType('email');

                    $('#sendTypes > div').on('click', function (e) {
                        $('#sendTypes > div').removeClass('mainact');
                        switchSendType($(this).addClass('mainact').data('type'));
                    });

                    $('.recipientPerson .personDisplay').makeAddressEditable();

                    $('#emailBody').trumbowyg({
                        lang: ns.getLocale(),
                        btns: [
                            ['viewHTML'],
                            ['undo', 'redo'],
                            ['formatting'],
                            ['strong', 'em', 'del', 'underline'],
                            ['superscript', 'subscript'],
                            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                            ['unorderedList', 'orderedList'],
                            ['horizontalRule'],
                            ['removeformat'],
                            ['fullscreen']
                        ]
                    });

                    $('#planBt', $modal).click(function (e) {
                        $('#mailPlanning', $modal).toggle();
                    });

                    $('#sendingNeverEnds', $modal).click(function (e) {
                        $('#sendingEndDate', $modal).toggle(!$(this).is(':checked'));
                    });

                    $("#resetTemplateBt", $modal).click(function (e) {
                        e.preventDefault();
                        var expObj = $('#sender-cell .personDisplay').data('person');
                        var exp = expObj && (expObj.label || (expObj.address && expObj.address.name));
                        getTemplate(true, exp);
                    });

                    $("#send", $modal).click(function (e) {
                        e.preventDefault();
                        send(true, {
                            indicator: '#sendingWindow .ajax-indicator',
                            feedback: '#sendingWindow .feedback',
                            closeOnSucess: true
                        });
                    }.debounce(200, true));

                    $("#preview", $modal).click(function (e) {
                        e.preventDefault();
                        send(true, {
                            indicator: '#sendingWindow .ajax-indicator',
                            feedback: '#sendingWindow .feedback'
                        }, true);
                    }.debounce(200, true));

                    $("#createShare", $modal).click(function (e) {
                        e.preventDefault();
                        createShare(true, {
                            indicator: '#sendingWindow .ajax-indicator',
                            feedback: '#sendingWindow .feedback'
                        });
                    }.debounce(200, true));

                    $("#endShare", $modal).click(function (e) {
                        e.preventDefault();
                        $('#sendingWindow').toggleFade(false);
                        $('#endShare, #sharelinkDisplay').toggle(false);
                        $('#createShare, .recipientPerson').toggle(true);
                    });

                    $('#sharelinkDisplay')
                        .find('.copyLnk').click(function (e) {
                            var elem = e.target;

                            ns.clipboardCopy(document.getElementById('shareUrl').innerHTML.replace('&amp;', '&'));
                            $('#sharelinkDisplay .copied').toggle(true);

                        }).end()
                        .find('.shareLnk').click(function (e) {
                            var $t = $(e.target),
                                url = $t.parent().data('url'),
                                msg = $(window).data('message');
                            navigator.share({
                                title: msg && msg.subject,
                                text: url,
                                url: url,
                            });
                        });
                }

                $pd = $('.recipientPerson .personDisplay');
                $('#sendingFrom .feedBack').empty();
                updateWindow($pd);
            });
        },
        onSuccess: function (result) {
            console.log('Sending success');
        },
        sendType: 'email'
    };

    function fillSendData(person) {
        $('#emailSend').html((person && person.email) ? person.email : loc.fillLater);
    };

    function remplaceInTemplate(template, replacements) {
        for (var propsName in replacements) {
            var val = ns.xssFilter(replacements[propsName]);
            var regExp = new RegExp(propsName, 'gi');
            if (template.subject) {
                template.subject = template.subject.replace(regExp, val);
            }
            if (template.body) {
                template.body = template.body.replace(regExp, val);
            }
        }

        return template;
    };

    function getTemplate(reset, exp) {
        var mType = $(window).data('message_type'),
            prefs,
            mTypeTempl;

        if (mType) {
            mType = mType.toUpperCase();
            mTypeTempl = 'message_notification' + mType + '.templ';
            prefs = [mTypeTempl, 'message_notification.templ'];
        } else {
            prefs = ['message_notification.templ'];
        }

        ns.ws('GET', 'v1/preference/' + prefs.join('%7C') + (reset ? '?reset=true' : ''), null, {
            feedback: '#sendingFrom .feedBack'
        })
            .then(function (result) {
                var templ = null;
                for (var i = 0; i < prefs.length; i++) {
                    var prefName = prefs[i];
                    if (result[prefName]) {
                        templ = result[prefName];
                        break;
                    }
                }
                if (templ) {
                    console.log('Valid template returned');
                    templ = remplaceInTemplate(templ, {
                        '<span class="token sender">.*?</span>': exp,
                        '<span class="token subject">.*?</span>': $('#subject').getValue()
                    });
                    $('#sendingSubject').val(templ.subject);
                    $('#emailBody').trumbowyg('html', templ.body);
                    $('#emailBody').html(templ.body);
                } else {
                    console.log('No valid template returned');
                }
            });
    };

    // Pre-fill with probable recipient
    function updateWindow($pw) {
        var person = $('#recipient-cell .personDisplay').data('person');
        fillSendData(person);

        // Generate or get number
        $('#issueNumber').setValue($('#message_number').getValue());
        var generateNumOnIssue = $(window).data('generateNumOnIssue');
        if (generateNumOnIssue) {
            ns.ws('GET', 'v1/messageNumber?message_type=' + $(window).data('message_type'), null, {
                feedback: '#sendingFrom .feedBack'
            })
                .then(function (result) {
                    if (result.next) {
                        $('#issueNumber').val(result.next);
                    } else {
                        console.log('Message generator returned no next number');
                    }
                });
        }
        $('#issueNumber').toggle(generateNumOnIssue);

        // Disable number or not
        if (!$(window).data('editMessageNumber')) {
            $('#issueNumber').attr('disabled', 'disabled');
        }

        // Disable issue date or not and force it to today
        if (!$(window).data('editIssueDate')) {
            var now = Date.now() / 1000;
            $('#sendingIssueDate').attr('disabled', 'disabled').setValue(now);
            $('#issue_date').setValue(now);
        } else {
            $('#sendingIssueDate').setValue($('#issue_date').getValue());
        }

        // Define expeditor label
        var expObj = $('#sender-cell .personDisplay').data('person');
        var exp = expObj && (expObj.label || (expObj.address && expObj.address.name));
        $('.senderOption').toggle(exp != null);
        if (exp) {
            $('#expLabel').html(exp);
        } else {
            exp = loc.someone;
        }

        var $docRec = $('#recipient-cell .personDisplay');
        var recObj = $docRec.data('person');
        $('.recipientPerson .personDisplay').data('person', recObj).empty().html($docRec.html());

        getTemplate(false, exp);
    };

    function send(issue, ajaxOpt, preview) {
        var sendType = ns.sending.sendType ? ns.sending.sendType.toUpperCase() : 'EMAIL';
        if (issue) {
            ns.logEvent('Document', 'submitIssue', sendType);
        } else {
            ns.logEvent('Document', 'submitSending', sendType);
        }
        console.log('Loading emission panel');

        var sentByYou = $('input[name=exp]:checked').val() === 'you';

        var sendRequest = {
            sendingTypes: [sendType],
            recipientPerson: $('#sendingFrom .recipientPerson .personDisplay').data('person'),
            senderPerson: sentByYou ? $('#sender-cell .personDisplay').data('person') : null,
            sendingComment: $('#sendingComment').val(),
            sendingSubject: $('#sendingSubject').val(),
            sendingEmailBody: $('#emailBody').val(),
            keepAsTemplate: $('#keepAsTemplate').is(':checked'),
            preview: preview
        };

        var $ssd = $('#sendingStartDate');
        if (!$ssd.is(':hidden')) {
            sendRequest['sendingStartDate'] = $ssd.getValue();
            sendRequest['sendingFrequency'] = $('#sendingFrequency').getValue();
            if (!$('#sendingNeverEnds').is(':checked')) {
                sendRequest['sendingEndDate'] = $('#sendingEndDate').getValue();
            }
        }

        var message = $(window).data('message');
        if (message && message.id) {
            ns.ws('POST', 'v1/message/' + message.id + '/' + (issue ? 'issue' : 'send'), sendRequest, ajaxOpt)
                .then(function (result) {
                    if (!preview) {
                        if (result.messageStatus == 'ISSUED')
                            result.action = 'view';
                        window.messageEditor.loadForm(result);
                    }
                    if (ajaxOpt.closeOnSucess) {
                        $('#sendingWindow').toggleFade(false);
                    }
                });
        } else {
            alert(loc.saveItFirst);
        }
    };

    function createShare(issue, ajaxOpt) {
        if (issue) {
            ns.logEvent('Document', 'submitIssueAndShare');
        } else {
            ns.logEvent('Document', 'submitSendingAndShare');
        }
        console.log('Loading emission panel');

        var recipient = $('#sendingFrom .recipientPerson .personDisplay').data('person'),
            sendRequest = {
                sendingTypes: ['SHARELINK'],
                recipientPerson: recipient
            };

        var message = $(window).data('message');
        if (message && message.id) {
            ns.ws('POST', 'v1/message/' + message.id + '/' + (issue ? 'issue' : 'send'), sendRequest, ajaxOpt)
                .then(function (result) {
                    if (result && result.sendings && result.sendings.length > 0) {
                        // Show link
                        var s = result.sendings[0],
                            share = navigator.share;
                        $('#sharelinkDisplay').data('url', s.url)
                            .find('.emailLnk').toggle(!share).attr('href', 'mailto:' + recipient.email + '?body=' + encodeURIComponent(s.url)).end()
                            .find('.shareLnk').toggle(share).end()
                            .find('#shareUrl').text(s.url).end()
                            .toggle(true);

                        $('#createShare, .recipientPerson').toggle(false);
                        $('#endShare').toggle(true);

                        // Update background message display
                        result.action = 'view';
                        window.messageEditor.loadForm(result);
                    }
                });
        } else {
            alert(loc.saveItFirst);
        }
    };

    function switchSendType(type) {
        ns.sending.sendType = type;
        var $allElem = $('.emailSent, .otherSent, .sharelinkSent');
        $allElem.filter(':not(.' + type + 'Sent)').toggle(false);
        $allElem.filter('.' + type + 'Sent').toggle(true);
    };

}());

//# sourceURL=/static/js/sending.js