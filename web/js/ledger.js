"use strict";

ns.ledger = new Vue({
  el: "#ledger",
  data: {
    accounts: [],
    loading: false,
    noContent: false,
    count: 0,
    crit: {
      assetPath: "",
      periodId: ""
    },
    keepFeedback: true,
    assetLabel: '',
    account_filter: ''
  },
  mounted: function () {
    this.crit.periodId = ns.accounting.defPeriod.value || ns.getLocalPref('defPeriod');
    this.crit.assetPath = (ns.accounting.defAsset && ns.accounting.defAsset.value) || ns.getLocalPref('defAsset');
    this.loadList();
  },
  methods: {
    // Reload if crit changes
    changeCrit: function (name, val) {
      console.log("Crit " + name + " changed " + val);
      if (this.crit[name] != val) {
        this.crit[name] = val;
        this.loadList();
      }
    },
    // Load from database
    loadList: function () {
      var that = this;

      this.loading = true;

      function addCrit(name) {
        var cr = that.crit[name];
        if (cr) {
          url +=
            "&" + name + "=" + encodeURIComponent(cr.trim ? cr.trim() : cr);
        }
      }

      // Get data from filter
      var url = "v1/accounting/accountLedger?a=1";

      var urlLen = url.length;
      addCrit("assetPath");
      addCrit("periodId");

      // Query and render result
      ns.ws("GET", url, { handleError: true })
        .then(function (result) {
          if (
            !result.accountLedger ||
            !result.accountLedger.accounts ||
            result.accountLedger.accounts.count == 0
          ) {
            that.noResults = true;
            that.accounts = [];
          } else {
            // Update table
            that.accounts = result.accountLedger.accounts;
            that.count = result.accountLedger.accounts.count;
            that.noResults = false;
          }
          that.loading = false;
        });
    },
    close: function (indictr, successMsg) {
      var that = this;

      this.loading = true;
      ns.ws("PUT", "v1/accounting/period/" + this.crit.periodId + "/close?assetPath=" + this.crit.assetPath, null, { handleError: true, indicator: indictr })
        .then(function (result) {
          that.loading = false;
          ns.showMessages({ infos: [{ message: successMsg }] }, { reset: true });
        });
    },
    formatAmount: function (value) {
      return value ? value.formatMoney().replace(" ", "&nbsp;") : "";
    }
  }
});
