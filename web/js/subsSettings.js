"use strict";

ns.userSettings = {};

ns.userSettings.loadMemberships = function () {
    ns.ws('GET', 'v1/org?withMemberships=true', null, {
        indicator: "#membership .ajax-indicator",
        handleError: true
    })
        .then(function (result) {
            if (result.org) {
                $('#orgName').text(result.org.name ? result.org.name : result.org.id);

                var $mbs = $('#membership'),
                    nb = (result.org.priceListMemberships && result.org.priceListMemberships.length) || 0;
                $mbs.empty();

                for (var mbspi = 0; mbspi < nb; mbspi++) {
                    var mbsp = result.org.priceListMemberships[mbspi],
                        fromStr = new Date(mbsp.from_date * 1000).format(loc.date.dateFormat),
                        line;

                    if ((mbsp.until_date * 1000) - Date.now() < 2838240000000) {
                        var toStr = new Date(mbsp.until_date * 1000).format(loc.date.dateFormat);
                        line = ns.formatMessage(loc.fromUntil, [mbsp.price_list.displayLabel, fromStr, toStr]);
                    } else {
                        line = ns.formatMessage(loc.fromNow, [mbsp.price_list.displayLabel, fromStr]);
                    }
                    $mbs.append('<div>' + line + '</div>');
                }
            } else {
                $('#orgName').text("");
            }
        });
};

ns.userSettings.updateAttestationList = function () {
    ns.ws('GET', 'v1/attestation', null, { handleError: true })
        .then(function (result) {
            if (result.attestations && result.attestations.length > 0) {
                $('#specimen').toggle(false);
                var $atts = $('#certificateList');
                $atts.empty();

                for (var atti = 0; atti < result.attestations.length; atti++) {
                    var att = result.attestations[atti];
                    var line = new Date(att.from_date * 1000).format(loc.date.dateFormat) + ' - ' +
                        new Date(att.to_date * 1000).format(loc.date.dateFormat);
                    $atts.append('<div><a target="_blank" href="' + ns.serverUrl + 'pdf-attest/attestation-' + att.id + '.pdf?id=' + att.id + '">' + line + '</a></div>');
                }
            } else {
                $('#certificateList').text("");
                $('#specimen').toggle(true);
            }
        });
};

ns.userSettings.updateMemberships = function () {
    var params = ns.getQueryParams();
    $('#mainAjIndicator').addClass('on');
    if (params.paymentId) {
        $('#subscribePayBt .ajax-indicator').addClass('on');
        ns.ws('PUT', 'v1/priceListMembership?paymentId=' + params.paymentId + "&token=" + params.token + "&PayerID=" + params.PayerID)
            .then(function (result) {
                console.log("Payment confirmation");
                console.log(result);
                $('#subscribePayBt .ajax-indicator').addClass('on');
                ns.userSettings.loadMemberships();
            });
    } else {
        ns.userSettings.loadMemberships();
    }
};

$(document).ready(function () {

    ns.userSettings.updateMemberships();
    ns.userSettings.updateAttestationList();

    $('#genCertifBt').click(function (e) {
        e.preventDefault();
        var $w = $('#attestationWindow');
        $w.toggleFade(true);
        $w.find('.header .closeBt').on('click', function (e) {
            e.preventDefault();
            $(e.target).closest('#attestationWindow').toggleFade(false);
        });

        var now = new Date(),
            before = new Date();

        before.setFullYear(before.getFullYear() - 1);
        ns.setDatePicker("#fromDate", before);
        ns.setDatePicker("#toDate", now);
    });

    $('#attestationWindow .submitBt').click(function (e) {
        e.preventDefault();
        var $f = $('#attestationWindow form');
        var values = {
            from_date: $('#fromDate').getValue(),
            to_date: $('#toDate').getValue()
        };

        ns.ws('POST', 'v1/attestation', values, {
            feedback: '#attestationFeedback'
        })
            .then(function (result) {
                if (result.attestation) {
                    ns.userSettings.updateAttestationList();
                    $('#attestationWindow').toggleFade(false);
                }
            });

    });

    $('#update_payment_bt').click(function (e) {
        e.preventDefault();
        var values = { web_base: window.location.origin + ns.locDir() + "subscription-settings.html" };

        ns.ws('POST', 'v1/stripe/setup_checkout', values)
            .then(function (result) {
                if (result.url) {
                    window.location.href = result.url;
                }
            });

    });

    $('.attestExBt').attr("href", ns.serverUrl + 'pdf-attest/attestation-ex.pdf?specimen=true&locale=' + loc.locale);

    if(window.location.hash) {
        var msg_div = document.querySelector(window.location.hash);
        if(msg_div) {
            msg_div.style.display = 'block';
        }
    }
});