"use strict";

ns.logEvent("Document", "openPayment");

ns["payment-editor"] = {
	props: {
		"message_id": Number,
		"to_pay": Number,
		"currency": String
	},
	template: "#payment-editorComp",
	data: function () {
		return {
			payment: {
				paymentDate: ns.toTs(new Date()),
				paymentType: '',
				amount: this.to_pay,
				description: '',
				closing: false
			},
			feedback: "#paymentWindow .feedBack"
		};
	},
	methods: {
		submit: function (indicator, successMsg) {

			var that = this;
			this.loading = false;

			if (this.message_id) {
				ns.ws('POST', 'v1/message/' + this.message_id + '/addPayment', this.payment,
					{ indicator: indicator, feedback: this.feedback, noFbOnSuccess: true })
					.then(function (result) {
						ns.logEvent("Document", "paymentAdded");
						result.infos = result.infos || [];
						result.infos.push({ message: successMsg });
						that.$emit("input", result);
					});
			} else {
				ns.showMessages({ errors: [{ message: loc.saveItFirst }] }, { reset: true });
			}
		}
	}
};

//# sourceURL=/static/js/payment-editorv.js
