"use strict";

ns.messageSettings = new Vue({
  el: "#adminConsole",
  data: {
    planned_date: "",
    restart_delay: 0,
    restart_duration: 0,
    change_user_email: "",
    new_membership: {
      org_id: 0,
      price_list_name: "",
      from_date: 0,
      until_date: 0,
    },
  },
  methods: {
    triggerPlanned: function () {
      ns.ws("POST", "v1/planned_task?simulateOn=" + this.planned_date, {});
    },
    restartForm: function () {
      ns.ws("POST", "v1/admin?action=restartWarn", {
        delay: this.restart_delay,
        duration: this.restart_duration,
      });
    },
    change_user: function () {
      ns.ws("POST", "v1/session?action=login", {
        login: this.change_user_email,
      });
    },
    add_membership: function () {
      ns.ws("POST", "v1/membershipAdmin", this.new_membership);
    },
    modify_membership: function () {
      ns.ws("PUT", "v1/membershipAdmin", this.new_membership);
    },
    parseDate: function (event) {
      return event.target.valueAsDate && event.target.valueAsDate.getTime() / 1000;
    },
    toDate: function (value) {
      return value && value.toISOString && value.toISOString().split("T")[0];
    },
  },
});
