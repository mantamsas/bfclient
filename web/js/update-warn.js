var $buoop = {
    vs: {
        i: 10,
        f: 21,
        o: 15,
        s: 6,
        c: 23
    }
};
$buoop.ol = window.onload;
window.onload = function () {
    try {
        if ($buoop.ol)
            $buoop.ol();
    } catch (e) {
        ns.logError(e);
    }
    var e = document.createElement("script");
    e.setAttribute("type", "text/javascript");
    e.setAttribute("src",
        "//browser-update.org/update.js");
    document.body.appendChild(e);
};