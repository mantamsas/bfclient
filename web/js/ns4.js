"use strict";

// Base app env
var pathName = window.location.pathname.split('/'),
    onDev = window.location.host === 'dev.ns.eu';
var ns = {
    serverUrl: onDev ? '/netscribe_dev/' : '/' + pathName[1] + '/',
    clientUrl: onDev ? '/v1/' : '/' + pathName[1] + '/static/',
    vue3Url: pathName[1] === "netscribe" ? '/v' : '/test/v',
    CORS: true,
    debug: true,
    version: 'v2',
    defaultLoc: 'fr',
    jqueryFile: 'jquery-3.3.1.js',
    onProd: pathName[1] === 'netscribe'
};

ns.logError = function (input) {
    if (!input) return;
    console.error(input);
    if (typeof Sentry !== 'undefined' && Sentry) {
        if (typeof input == "exception") {
            Sentry.captureException(input);
        } else {
            Sentry.captureMessage(input);
        }
    }
};

ns.dispatch = function (eventName) {
    var event = document.createEvent('Event');
    event.initEvent(eventName, false, false);
    window.dispatchEvent(event);
};

ns.getRealName = function (scrUrl) {
    return (ns.debug || scrUrl.indexOf('.min.js') > -1) ? scrUrl : scrUrl.replace(/js$/i, 'min.js');
};

ns.ldone = function (objName, scrUrl, min, async) {
    return new Promise(function (resolve, reject) {
        var obj = objName && (ns[objName] || window[objName]);
        if (obj) {
            console.log(scrUrl + ' already loaded');
            resolve(obj);
            return;
        }

        var script = document.createElement("SCRIPT");
        if (async)
            script.async = 1;
        script.onload = function () {
            if (this.obj) {
                ns.dispatch(this.obj + '-loaded');
            }
            console.log(scrUrl + ' loaded');
            resolve(objName && (window[objName] || ns[objName]));
        };
        script.onerror = function () {
            console.log(scrUrl + ' load error');
            reject();
        };
        script.type = 'text/javascript';
        script.obj = objName;
        script.src = min ? ns.getRealName(scrUrl) : scrUrl;

        try {
            document.head.appendChild(script);
        } catch (e) {
            ns.logError(e);
            reject();
        }
        console.log(scrUrl + ' loading');
    });
}

ns.ld = function (urls) {
    var promise = Promise.resolve();

    // Create promise chain
    for (var i = 0; i < urls.length; i++) {
        if (!urls[i]) continue;
        let url = urls[i];
        promise = promise.then(function (scrUrl) {
            var urlStr = url.url || url,
                next = ns.ldone(url.obj, urlStr, url.min);
            console.log('will load ' + urlStr + ' after ' + scrUrl);
            return next;
        });
    }
    return promise;
};

ns.load = function (addUrls, noLocale) {
    var promise = noLocale ? Promise.resolve() : ns.ldone('locale', 'locale.js');

    var urls = noLocale ? [] : ['locale.js'];
    if (!this.debug) {
        promise = promise.then(function () {
            return Promise.all([
                ns.ldone('jQuery', '//code.jquery.com/' + ns.jqueryFile, true),
                ns.ldone('Vue', "//cdn.jsdelivr.net/npm/vue/dist/vue.js", true)
            ]);
        });
    }
    promise = promise.then(function () {
        return Promise.all([
            ns.ldone('jQuery', '../js/libs/' + ns.jqueryFile, true),
            ns.ldone('Vue', "../js/libs/vue.js", true)
        ]);
    });

    if (addUrls) {
        for (var i = 0; i < addUrls.length; i++) {
            urls.push(addUrls[i]);
        }
    }
    promise.then(function () {
        return ns.ld(urls);
    });

    if (ns.onProd) {
        ns.ldone('dataLayer', 'https://www.googletagmanager.com/gtag/js?id=GTM-W3BVRN2', false, true).then(() => {
            window.dataLayer = window.dataLayer || [];
            function gtag() { dataLayer.push(arguments); }
            gtag('js', new Date());
            gtag('config', 'G-H1ZHXP9S64'); // GA4
            //gtag('config', 'AW-1067357180'); // Adwords
        });

        /*ns.ldone('Sentry', '//browser.sentry-cdn.com/5.10.1/bundle.min.js').then(()=>{
            Sentry.init({ dsn: 'https://4d76d5216bbe4841a5fb423dca994ae2@sentry.io/1513898' });
        });*/
    }
};

ns.loadhtml = function (url, targetElem) {
    return ns.ajax('GET', url, 'html').then(function (xhr) {
        var add = document.createElement('span');
        add.innerHTML = xhr.response || xhr.responseText;
        var tgt = targetElem || document.body;
        tgt.appendChild(add);
    });
};

ns.addLoadListener = function (objName, listener) {
    if (window[objName]) {
        listener();
    } else {
        window.addEventListener(objName + '-loaded', listener);
    }
};

ns.extend = function () {
    for (var i = 1; i < arguments.length; i++)
        for (var key in arguments[i])
            if (arguments[i].hasOwnProperty(key))
                arguments[0][key] = arguments[i][key];
    return arguments[0];
};

// NS dependence-free init
ns = ns.extend(ns, {
    icon: {
        save: "fa fa-floppy-o",
        pdf: "fa fa-file-pdf-o",
        send: "fa fa-paper-plane",
        modify: "fa fa-pencil",
        plus: "fa fa-plus-circle",
        money: "fa fa-money",
        play: "fa fa-play",
        copy: "fa fa-copy",
        history: "fa fa-film",
        check: "fa fa-check",
        accept: "fa fa-thumbs-up",
        refuse: "fa fa-thumbs-down",
        close: "fa fa-times",
        delete: "fa fa-trash-o",
        recycle: "fa fa-recycle"
    },
    pad: function (val, len) {
        val = String(val);
        len = len || 2;
        while (val.length < len) val = "0" + val;
        return val;
    },
    array_values: function (input) {
        var tmp_arr = [];

        for (key in input) {
            tmp_arr[tmp_arr.length] = input[key];
        }

        return tmp_arr;
    },
    get_arr_key: function (input, value) {
        for (key in input) {
            if (input[key] === value)
                return key;
        }

        return null;
    },
    round: function (number, decimal) {
        if (decimal < 0) return number;
        var k = Math.pow(10, decimal);
        return Math.round(number * k) / k;
    },
    lt: /<\/?script/g,
    xssFilter: function (instr) {
        return (instr && instr.replace) ? instr.replace(ns.lt, '&lt;script').replace('<!--', '&lt;!--') : instr;
    },
    // Recursive null remover
    cloneObject: function (obj) {
        var clone = {};
        for (var i in obj) {
            if (obj[i] != null && obj[i] != [] && obj[i] != {}) {
                var o = obj[i];
                if (Array.isArray(o)) {
                    var n = o.length;
                    clone[i] = [];
                    for (var j = 0; j < n; j++) {
                        clone[i][j] = this.cloneObject(o[j]);
                    }
                } else if (o instanceof Number) {
                    clone[i] = o.valueOf();
                } else if (typeof (obj[i]) == "object")
                    clone[i] = this.cloneObject(o);
                else
                    clone[i] = o;
            }
        }
        return clone;
    },
    formatNumber2: function (number, decimals, dec_point, thousands_sep) {
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var ep = n.toString().split('e-');
                if (ep[1]) {
                    var e = parseInt(ep[1]);
                    return '0.' + '0'.repeat(e - 1) + ep[0].replace('.', '');
                } else {
                    return '' + ns.round(n, prec);
                }
            };

        // Keep precision if no decimals
        if (!decimals) {
            prec = n.getPrecision();
        }

        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    },

    formatMessage: function (template, values) {
        var rep = template;
        for (var i = 0; i < values.length; i++) {
            rep = rep.replace(new RegExp('\\{' + i + '\\}', 'g'), values[i]);
        }
        return rep;
    },

    // From text date to unix timestamp
    parseDate: function (data, format) {
        var forTokreg = new RegExp("[^dmyhHMslLtTZoS]+", "g"),
            tokens = (format || loc.date.dateFormat).split(forTokreg),
            forVarReg = new RegExp("[^0-9]+", "g"),
            values = data.split(forVarReg),
            max = tokens.length < values.length ? tokens.length : values.length,
            map = {};

        for (var i = 0; i < max; i++) {
            var token = tokens[i];
            map[token[0]] = values[i];
        }
        if (!map.H) map.H = 12;
        if (!map.M) map.M = 0;
        var yr = parseInt(map['y']),
            date = new Date((yr < 100 ? yr + 2000 : yr), parseInt(map['m'], 10) - 1, map['d'], map['H'], map['M']);
        return this.toTs(date);
    },

    toTs(date) {
        return Math.round(date.getTime() / 1000);
    },

    getLocale: function () {
        return loc.locale;
    },

    getStr: function (object) {
        return object ? object : '';
    },

    addBr: function (add) {
        if (add) {
            return add + '<br />';
        }
        return '';
    },
    /**
     * Format number with locale.
     */
    formatNumber: function (number, options) {
        var opt = {};
        options = ns.extend(opt, loc.number, options);
        if (isNaN(number)) {
            if (!options.nullVal) {
                return '';
            }
            number = options.nullVal;
        }

        return ns.formatNumber2(number, options.decimals, options.dec_sep, options.thou_sep);
    },
    /**
     * Parses a string of given format into a Number object.
     * 
     * @param {Object} string
     * @param {Object} options
     */
    parseNumber: function (numberString, options) {
        var opt = ns.extend({}, loc.number, options);

        if (!numberString)
            return 0;

        var valid = "1234567890-" + opt.dec_sep;

        if (numberString.indexOf('.') > -1 && numberString.indexOf(opt.dec_sep) == -1) {
            opt.dec_sep = '.';
        }

        var validText = "";
        for (var i = 0; i < numberString.length; i++) {
            var c = numberString.charAt(i);
            if (c === opt.dec_sep)
                validText += '.';
            else if (valid.indexOf(numberString.charAt(i)) > -1)
                validText += numberString.charAt(i);
        }
        return new Number(validText == "" ? 0 : validText);
    },

    // Regexes and supporting functions are cached through closure
    formatDate: function (date, mask, utc) {
        var dateLoc = loc.date,
            token = /d{1,4}|m{1,4}|y{1,4}|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
            timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
            timezoneClip = /[^-+\dA-Z]/g;

        // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
        if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
            mask = date;
            date = undefined;
        }

        // Passing date through Date applies Date.parse, if necessary
        date = date ? new Date(date) : new Date();
        if (isNaN(date)) {
            console.log("Invalid date: " + date);
            return '';
        }

        if (!mask) {
            mask = String(dateLoc.dateFormat);
        }

        // Allow setting the utc argument via the mask
        if (mask.slice(0, 4) == "UTC:") {
            mask = mask.slice(4);
            utc = true;
        }

        var _ = utc ? "getUTC" : "get",
            d = date[_ + "Date"](),
            D = date[_ + "Day"](),
            m = date[_ + "Month"](),
            y = date[_ + "FullYear"](),
            H = date[_ + "Hours"](),
            M = date[_ + "Minutes"](),
            s = date[_ + "Seconds"](),
            L = date[_ + "Milliseconds"](),
            o = utc ? 0 : date.getTimezoneOffset(),
            flags = {
                d: d,
                dd: ns.pad(d),
                ddd: dateLoc.days[D],
                dddd: dateLoc.dayNames[D + 7],
                m: m + 1,
                mm: ns.pad(m + 1),
                mmm: dateLoc.months[m],
                mmmm: dateLoc.monthNamesShort[m + 12],
                y: String(y).slice(2),
                yy: y,
                yyyy: y,
                h: H % 12 || 12,
                hh: ns.pad(H % 12 || 12),
                H: H,
                HH: ns.pad(H),
                M: M,
                MM: ns.pad(M),
                s: s,
                ss: ns.pad(s),
                l: ns.pad(L, 3),
                L: ns.pad(L > 99 ? Math.round(L / 10) : L),
                t: H < 12 ? "a" : "p",
                tt: H < 12 ? "am" : "pm",
                T: H < 12 ? "A" : "P",
                TT: H < 12 ? "AM" : "PM",
                Z: utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
                o: (o > 0 ? "-" : "+") + ns.pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
                S: ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
            };

        return mask.replace(token, function ($0) {
            return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
        });
    },

    setUrlParam: function (url, paramName, newValue) {
        if (url.indexOf(paramName + '=') > -1) {
            var regex = new RegExp("([?|&]" + paramName + "=)[^\&]+");
            url = url.replace(regex, newValue ? ('$1' + newValue) : '');
        } else if (url.indexOf('?') > -1 && newValue) {
            url += '&' + paramName + '=' + newValue;
        } else if (newValue) {
            url += '?' + paramName + '=' + newValue;
        }
        return url.replace('#', '');
    },

    updateArray: function (key, arr) {
        if ((!arr || Object.keys(arr).length == 0) && localStorage[key]) {
            delete localStorage[key];
        } else {
            localStorage[key] = JSON.stringify(arr);
        }
    },

    getJSONItem: function (storage, key) {
        var it = storage.getItem(key);
        try {
            it = it ? JSON.parse(it) : {};
        } catch (e) {
            it = {};
            ns.logError(e);
        }
        return it;
    },

    /* Add message to session storage to be displayed next time showMessages is called */
    addNextMessage: function (level, msg) {
        var nextMsg = ns.getJSONItem(sessionStorage, 'nextMessages');
        if (!nextMsg[level]) {
            nextMsg[level] = [];
        }
        nextMsg[level].push({
            message: msg
        });
        sessionStorage.setItem('nextMessages', JSON.stringify(nextMsg));
    },

    newWindow: function (url, name, options) {
        return window.open(url, name, options);
    },

    locDir: function () {
        return this.clientUrl + loc.locale + '/';
    },

    homeUrl: function () {
        return this.isAuth() ? ns.vue3Url + '/invoicing' : (this.locDir() +'index.html');
    },

    goHome: function () {
        window.location.href = this.homeUrl();
    },

    closeWindow: function () {
        window.close();
    },

    clipboardCopy: function (content) {
        var el = document.createElement('textarea');
        el.value = content;
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    },

    doMessageAction: function (messageId, action, callBack) {
        ns.ws('POST', 'v1/message/' + messageId + '/' + action).then(function (result) {
            ns.showMessages(result, { reset: true });
            if (callBack) {
                callBack(result);
            }
        });
    },

    isStorage(session) {
        return (localStorage && !session) || (sessionStorage && session);
    },

    setLocalPref: function (key, value, session) {
        if (!this.isStorage(session)) {
            ns.logError("Set storage not supported");
        }
        var st = session ? sessionStorage : localStorage;
        if (value) {
            try {
                st.setItem(key, JSON.stringify(value));
            } catch (e) {
                if (e.name == 'QuotaExceededError') {
                    e.message = 'Error inserting ' + key + ' in ' + (session ? 'session storage' : 'local storage');
                }
                throw e;
            }
        } else {
            st.removeItem(key);
        }
    },

    getLocalPref: function (key, defaultVal, session) {
        if (!this.isStorage(session))
            return null;
        var st = session ? sessionStorage : localStorage;
        var v = st.getItem(key);
        return v ? JSON.parse(v) : defaultVal;
    },

    /**
     * Create a post query
     */
    doPost: function (data) {
        var id = 'ffPostForm',
            txt = '<form id="' + id + '" action="' + decodeURIComponent(data.url) + '" style="display:none" method="POST">';
        delete data.url;
        for (var property in data) {
            if (data.hasOwnProperty(property)) {
                txt += '<input type="hidden" name="' + property + '" value="' + data[property] + '"/>';
            }
        }
        txt += '</form>';
        var o = document.getElementById(id);
        if (o) {
            o.parentNode.removeChild(o);
        }
        document.write(txt);
        document.getElementById(id).submit();
    },

    oauthSignIn: function (system, stateData, windowParam) {
        var wParam = {
            width: 580,
            height: 360,
            location: 'no',
            menubar: 'no',
            status: 'no',
            titlebar: 'no',
            toolbar: 'no'
        };
        wParam = ns.extend(wParam, windowParam);

        stateData = stateData ? stateData : {};
        stateData.system = system;
        ns.ws('GET', 'v1/oauth?action=getAuthUrl&system=' + system)
            .then(function (result) {
                ns.showMessages(result);
                if (stateData) {
                    result.tokenUrl += ('&state=' + encodeURI(JSON.stringify(stateData)));
                }
                if (result.tokenUrl) {
                    var wargs = [];

                    for (var key in wParam) {
                        if (wParam.hasOwnProperty(key)) {
                            wargs.push(key + '=' + wParam[key]);
                        }
                    }

                    window.location = result.tokenUrl;
                    console.log('OAuth toke window opened');
                } else {
                    console.log('No token URL provided');
                }
            });
    },

    gaAction: function (actType, params) {
        try {
            if (typeof (window.dataLayer) !== 'undefined' && !ns.debug) {
                window.dataLayer.push({ event: actType, param: params });
            }
        } catch (e) {
            ns.logError(e);
        }
    },

    logEvent: function (cat, action, label, val) {
        window.dataLayer && window.dataLayer.push({ 'event': action, eventCategory: cat, eventLabel: label });
    },

    setDatePicker: function (selector, value) {
        var elem = document.querySelector(selector),
            prs = function (str) {
                var ts = ns.parseDate(str) * 1000;
                return isNaN(ts) ? Date.now() : ts;
            };
        if (!elem) {
            console.log("No " + selector);
            return;
        }
        var dp = TinyDatePicker(elem, {
            mode: 'dp-below',
            lang: loc.date,
            format: ns.formatDate,
            parse: prs,
            hilightedDate: new Date()
        });
        if (value == 'now') {
            TinyDatePicker.selectToday(dp);
        } else if (value) {
            dp.setState({
                selectedDate: value
            });
        }
        var dateCss = document.getElementById('dateCss');
        if (!dateCss) {
            var css = document.createElement("LINK"),
                url = '../js/libs/datepicker/tiny-date-picker.css';
            css.id = 'dateCss';
            css.rel = 'stylesheet';
            css.type = 'text/css';
            css.href = ns.debug ? url : url.replace(/css$/i, 'min.css');
            css.media = 'all';
            document.getElementsByTagName("head")[0].appendChild(css);
        }
        elem.datePicker = dp;
        return dp;
    },

    getQueryParams: function () {
        if (!document.ns || !document.ns.params) {
            document.ns = { params: {} };

            if (window.location.search && window.location.search.length > 1) {
                var pairs = window.location.search.substring(1, 255).split('&');
                var nb = pairs.length;
                for (var i = 0; i < nb; i++) {
                    var elems = pairs[i].split('=');
                    if (elems.length == 2) {
                        document.ns.params[elems[0]] = elems[1];
                    } else if (elems.length == 1) {
                        document.ns.params[elems[0]] = true;
                    }
                }
            }
        }

        return ns.cloneObject(document.ns.params); // To avoid to add elements in params by mistake
    },

    params2Url: function (params) {
        var url = '';
        for (var property in params) {
            if (params.hasOwnProperty(property)) {
                url += "&" + property + "=" + params[property];
            }
        }
        return url;
    },

    createXhr: function (method, url, creds) {
        var xhr = new XMLHttpRequest();
        if (this.CORS && "withCredentials" in xhr) {
            xhr.open(method, url, true);
            xhr.withCredentials = creds;
        } else if (this.CORS && typeof XDomainRequest != "undefined") {
            xhr = new XDomainRequest();
            xhr.open(method, url);
        } else {
            xhr.open(method, url, true);
        }
        return xhr;
    },

    errHandler: function (xhr, opt, reject) {
        if (xhr > 0)
            ns.logError(xhr);
        if (opt.handleError) {
            if (xhr.status == 401 && (!xhr.jsonResp || xhr.jsonResp.warnings)) {
                ns.showMessages({ warnings: [{ message: loc.shouldBeAuth }] });
            }
            ns.showMessages(xhr.jsonResp, opt);
        } else {
            reject(xhr);
        }
    },

    ajax: function (method, url, type, post, options) {
        return new Promise(function (resolve, reject) {

            var opt = options || {},
                ind = (typeof opt.indicator == "string" ? document.querySelector(opt.indicator) : opt.indicator) || document.getElementById('mainAjIndicator'),
                xhr = ns.createXhr(method, url, opt.creds);

            if (!ind) {
                console.error("No indicator used for query " + url);
            }

            xhr.onload = function () {
                if (ind) ind.classList.remove('on');
                var resp = xhr.response || xhr.responseText;
                if (type === 'json' && resp) {
                    try {
                        xhr.jsonResp = JSON.parse(resp);
                    } catch (e) {
                        ns.logError(e);
                    }
                }
                if (xhr.status >= 200 && xhr.status < 300) {
                    // Success
                    if (localStorage && opt.cache && method == 'GET') {
                        localStorage.setItem(url, xhr.response);
                    }
                    resolve(xhr);
                } else {
                    // Error
                    ns.errHandler(xhr, opt, reject);
                }
                if (xhr.jsonResp && !opt.noFbOnSuccess) {
                    ns.showMessages(xhr.jsonResp, opt);
                }
            };

            xhr.onerror = function () {
                if (ind) ind.classList.remove('on');

                // If offline
                if (xhr.readyState == 4 && xhr.status == 0) {
                    console.log("Server connection refused for " + this);

                    if (localStorage && options.cache && method == 'GET') {
                        console.log("Get from cache"); // Implement offline indicator
                        var cached = localStorage.getItem(url);
                        if (cached)
                            resolve(cached);
                    }
                }

                ns.errHandler(xhr, opt, reject);
            };
            if (ind) ind.classList.add('on');
            try {
                xhr.send(post ? (type === 'json' ? JSON.stringify(post) : post) : null);
            } catch (ex) {
                console.log(ex);
                if (ind) ind.classList.remove('on');
                reject(ex);
            }
        });
    },

    ws: function (method, url, post, options) {
        options = options || {};
        options.creds = true;

        if (options.reset === false) {
            var out = ns.getMessCont(options);
            if (out)
                ns.rmChildren(out);
        }

        return new Promise(function (resolve, reject) {
            ns.ajax(method, ns.serverUrl + 'api/' + url, 'json', post, options).then(function (xhr) {
                resolve(xhr.jsonResp);
            }).catch(function (xhr) {
                reject(xhr);
            });
        });
    },

    requires: function (objectName, path) {
        return ns.ldone(objectName, '../js/' + path);
    },

    getMessCont: function (opt) {
        if (!opt)
            return null;

        var out;
        if (typeof opt.feedback == "object")
            out = opt.feedback;
        else if (typeof opt.feedback == "string")
            out = document.querySelector(opt.feedback);

        if (!out)
            out = document.getElementById('mainFeedBack');

        return out;
    },

    toggleFade: function (element, enable) {
        var el = typeof element == "string" ? document.querySelector(element) : element
        st = el.style;
        st.opacity = enable ? 1 : 0;
        st['pointer-events'] = enable ? 'auto' : 'none';
    },

    isVisible: function (el) {
        return el && (el.style.opacity !== 0) && (el.style.display != 'none');
    },

    replaceParams: function (message, params) {
        var resp = message;
        for (var j = 0; j < params.length; j++) {
            resp = resp.replace(new RegExp('\\{' + j + '\\}', 'g'), params[j]);
        }
        return resp;
    },

    rmChildren: function (parent) {
        if (!parent)
            return;
        while (parent.firstChild) {
            parent.removeChild(parent.firstChild);
        }
    },

    resetMessages: function (options) {
        var v = ns.getMessCont(options);
        if (v)
            ns.rmChildren(v);
    },

    showMessages: function (response, options) {
        if (!response) return;
        var opt = options || {},
            out = ns.getMessCont(opt);

        if (!out) {
            console.warn('No feedback output');
            return;
        }

        if (response.responseText) {
            response = response.responseText;
        }
        if (typeof response == 'string') {
            try {
                response = JSON.parse(response);
            } catch (e) {
                ns.logError("Error parsing result to display messages:");
                ns.logError(e);
                return;
            }
        }

        var delHandler = function (ev) {
            this.parentNode.removeChild(this);
        };
        function appendMessages(messArray, level) {
            if (messArray) {
                var nb = messArray.length;

                for (var i = 0; i < nb; i++) {
                    var newItem = null,
                        reason = messArray[i].reason;
                    if (!newItem) {
                        newItem = document.createElement('div');
                        newItem.className = level;
                        var m = messArray[i];
                        newItem.innerHTML = '<i class="fa fa-times"></i>' + (m.message ? m.message.replace('<', "&lt;").replace('>', '&gt;') : m.reason);
                    }
                    if (out.innerHTML.indexOf(newItem.innerHTML) == -1) {
                        newItem.addEventListener("click", delHandler);
                        out.appendChild(newItem);
                    }
                }
            }
        }

        var nextMessages = ns.getJSONItem(sessionStorage, 'nextMessages');
        appendMessages(response.errors, 'error');
        appendMessages(nextMessages.errors, 'error');
        appendMessages(response.warnings, 'warn');
        appendMessages(nextMessages.warnings, 'warn');
        appendMessages(response.infos, 'info');
        appendMessages(nextMessages.infos, 'info');
        sessionStorage.removeItem('nextMessages');
    },

    addMsgIfDone: function (res, message) {
        if ((!res.infos || res.infos.length == null) &&
            (!res.warnings || res.warnings.length == null) &&
            (!res.errors || res.errors.length == null))
            res.infos = [{ message: message }];
    }
});

// General JS upgrading
Function.prototype.debounce = function (threshold, execAsap) {

    var func = this,
        timeout;

    return function debounced() {
        var obj = this,
            args = arguments;

        function delayed() {
            if (!execAsap)
                func.apply(obj, args);
            timeout = null;
        }

        if (timeout)
            clearTimeout(timeout);
        else if (execAsap)
            func.apply(obj, args);

        timeout = setTimeout(delayed, threshold || 100);
    };
};

Number.prototype.formatMoneyBase = function (c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

ns.getPrecisionOf = function (v) {
    if (Math.floor(v) === v) return 0;
    var str = v.toString();
    var ep = str.split("e-");
    if (ep.length > 1) {
        var np = Number(ep[0]);
        return ns.getPrecision(np) + Number(ep[1]);
    }
    var dp = str.split(".");
    if (dp.length > 1) {
        return dp[1].length;
    }
    return 0;
}

Number.prototype.getPrecision = function () {
    return ns.getPrecisionOf(this.valueOf());
}

//For convenience...
Date.prototype.format = function (mask, utc) {
    return ns.formatDate(this, mask, utc);
};

/**
 * NS init after JQuery
 */
ns.addLoadListener('jQuery', function () {

    ns.formatAmount = function (value, currency) {
        if (value === null || typeof value === "undefined" || isNaN(value)) return "";
        value = Number(value);
        var p = value.getPrecision();
        var wp = $(window).data('precision') || $(window).data('currencyPrecision') || -1;
        return ns.formatNumber(value, {
            decimals: wp > p ? wp : p
        }) + (currency ? (' ' + currency) : '');
    };

    // Set value in cell or in input in cell, formats are number, amount, date
    $.fn.setValue = function (inVal, triggerChange) {
        return this.each(function () {
            var $t = $(this);
            var value = inVal;
            if ($t.is('.date') && !isNaN(value)) {
                value = new Date(value * 1000).format(loc.date.dateFormat);
            } else if ($t.is('.number')) {
                value = ns.formatNumber(value);
            } else if ($t.is('.amount')) {
                value = ns.formatAmount(value);
            } else if ($t.is('.percent')) {
                value = ns.formatNumber(value) + '%';
            }
            value = value ? ns.xssFilter(value) : '';

            if ($t.is(":text")) {
                $t.val(value);
            } else {
                var $child = $('input, select', $t);
                if ($child.length > 0) {
                    $child.val(value);
                } else {
                    var $child = $('textarea', $t);
                    if ($child.length > 0) {
                        $child.val(value);
                    } else {
                        $t[0].innerHTML = value;
                    }
                }
            }
            if (triggerChange) {
                $t.change();
            }
        });
    };

    function getValue($t) {
        var val;
        if ($t.is(":text")) {
            val = $t.val();
        } else if ($t.is("textarea")) {
            val = $t.html();
        } else {
            var sl = 'input,textarea,select',
                $fch = $t.is(sl) ? $t : $(sl, $t);
            if ($fch.length > 0) {
                val = $fch.val();
            } else {
                val = $t.html();
            }
        }

        if ($t.is('.percent')) {
            val = val.replace('%', '');
        }

        if ($t.is('.date')) {
            val = ns.parseDate(val, loc.date.dateFormat);
        } else if ($t.is('.number') || $t.is('.percent') || $t.is('.amount')) {
            val = ns.parseNumber(val);
        }
        return val ? ns.xssFilter(val) : null;
    }

    // Get value in cell or in input in cell
    $.fn.getValue = function () {
        if (this.length > 0) {
            return getValue($(this[0]));
        }
        return null;
    };

    // Return true if one of the elements have a non void or empty value.
    $.fn.isFilled = function () {
        for (var i = 0; i < this.length; i++) {
            var val = getValue($(this[i]));
            if (val && val.trim() != '') {
                return true;
            }
        }
        return false;
    };

    /**
     * Parse number with locale.
     */
    $.fn.parseNumber = function () {
        // get text
        var text = this.getValue();
        // parse text
        return ns.parseNumber(text);
    };

    Number.prototype.toFixed = function (precision) {
        return jQuery._roundNumber(this, precision);
    };

    jQuery._roundNumber = function (number, decimalPlaces) {
        var power = Math.pow(10, decimalPlaces || 0);
        var value = String(Math.round(number * power) / power);

        // ensure the decimal places are there
        if (decimalPlaces > 0) {
            var dp = value.indexOf(".");
            if (dp == -1) {
                value += '.';
                dp = 0;
            } else {
                dp = value.length - (dp + 1);
            }

            while (dp < decimalPlaces) {
                value += '0';
                dp++;
            }
        }
        return value;
    };

    /**
     * Transform basic element into form input.
     */
    $.fn.elem2input = function (type) {
        return this.each(function () {
            var $t = $(this);
            if ($t.find('input, textarea, select').length == 0) {
                $t.addClass('edited');
                if (type === 'textarea') {
                    $t.html('<textarea>' + $t.html() + '</textarea>');
                } else if (type === 'select') {
                    $t.html('<select data-selected="' + $t.html() + '"></select>');
                } else {
                    $t.html('<input value="' + $t.html() + '"/>');
                }
            }
        });
    };

    /**
     * Transforme back form input element into 
     */
    $.fn.input2elem = function () {
        return this.each(function () {
            var $t = $(this);
            $t.removeClass('edited');
            var $i = $t.find('input, textarea');
            var $s = $t.find('select');
            if ($i.length > 0 || $s.length > 0) {
                var val = $i.val();
                var sel = $s.data('selected');
                $i.remove();
                $s.remove();
                $t.html((val ? val : '') + (sel ? sel : ''));
            }
        });
    };

    $.fn.nextOnReturn = function (next) {
        $(this).keydown(function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
            }
        });
    };

    ns.selectedInTable = function (source, prop) {
        return $('.select-item input:checkbox', source)
            .filter(
                function (index) {
                    return this.checked;
                })
            .parent()
            .parent()
            .map(function () {
                return $(this).data(prop);
            }).get();
    };



    ns.selectOptions = function ($select, options, attr) {
        var opt = '';
        for (var key in options) {
            var value = options[key];
            opt += '<option value="' + key + '">' + (attr ? value[attr] : value) + '</option>';
        }
        $(opt).appendTo($select);
    };

    ns.selectOptionsLoc = function ($select, options, localisation) {
        var opt = '';
        for (var i = 0; i < options.length; i++) {
            var key = options[i];
            var value = localisation[key];
            opt += '<option value="' + key + '">' + value + '</option>';
        }
        $(opt).appendTo($select);
    };

    // Update active menu item
    ns.hightLightMenu = function () {
        var path = window.location.pathname.split('/');
        path = path[path.length - 1];

        $('#main-menu li')
            .removeClass('active-trail')
            .find('a')
            .filter(function () {
                return $(this).attr('href').indexOf(path) > -1;
            })
            .parent()
            .appendClass('active-trail');
    };

    ns.initPrefChanges = function (ids, scope) {
        for (var i = ids.length - 1; i > -1; i--) {

            $('input[name=' + ids[i] + ']').on('keyup change', function (e) {
                var $t = $(this),
                    req = {};
                req[$t.attr('name')] = $t.attr('type') === 'checkbox' ? $t.is(":checked") : $t.getValue();
                ns.ws('POST', 'v1/preference', req)
                    .then(function (e) {
                        $t.addClass('updating');
                        $('#linkStatus').toggle(true);
                        setTimeout(function () {
                            $t.removeClass('updating');
                            $('#linkStatus').toggle(false);
                        }, 1000)
                    });
            }.debounce(400, true));

        }
    };

    // Get current preferences values
    ns.initPrefInputs = function (booleanIds, textIds, scope) {

        var allIds = $.merge($.merge([], booleanIds), textIds);

        return ns.ws('GET', 'v1/preference/' + allIds.join('+') + (scope ? '?scope=' + scope : ''), null, { handleError: true })
            .then(function (result) {
                var ps = ns.printSettings;

                for (var i = booleanIds.length - 1; i > -1; i--) {
                    if (typeof result[booleanIds[i]] !== 'undefined')
                        $('input[name=' + booleanIds[i] + ']').attr("checked", result[booleanIds[i]]);
                }

                for (var i = textIds.length - 1; i > -1; i--) {
                    if (typeof result[textIds[i]] !== 'undefined')
                        $('input[name=' + textIds[i] + ']').setValue(result[textIds[i]]);
                }
            });
    };

    /* create one div per content */
    $.fn.appendDiv = function (content, theClass) {
        var $t = $(this);

        if (content) {
            if (content.constructor === Array) {
                for (var idx = 0; idx < content.length; idx++) {
                    $t.appendDiv(content[idx], theClass);
                }
            } else if (content.trim().length > 0) {
                $t.append('<div' + (theClass ? ' class="' + theClass + '"' : '') + '>' + content + '</div>');
            }
        }
        return $t;
    };

    $.fn.enableLink = function (enable) {
        var $t = $(this);
        if (enable) {
            $t.attr('href', '#').removeClass("disabled");
        } else {
            $t.removeAttr('href').addClass("disabled");
        }
    };

    $.fn.toggleFade = function (enable) {
        return $(this).css(enable ? {
            "opacity": 1,
            "pointer-events": "auto"
        } : {
            "opacity": 0,
            "pointer-events": "none"
        });
    };

    ns.setAuthUi = function (connected) {
        var $h = $('html');
        if (connected) {
            $h.addClass('auth').removeClass('noAuth');
        } else {
            $h.addClass('noAuth').removeClass('auth');
        }
    }

    $.fn.load = function (url, options) {
        var opt = ns.extend({
            target: this
        }, options);
        return ns.ajax('GET', url, 'html', null, opt)
            .then(function (xhr) {
                $(opt.target).append(xhr.response || xhr.responseText);
            });
    };

    ns.isAuth = function () {
        return $('html').is('.auth');
    };

    // Test if session is active and authenticated
    ns.testSession = function (nextStep, options) {

        // If auth and tested less than one minute => auth, bypass session query
        var $h = $('html'),
            params = ns.getQueryParams(),
            vCode = params['vCode'];

        if (this.isAuth() && !vCode) {
            var now = Date.now();
            if ($h.data('lastQuery') - now < 60000) {
                nextStep({
                    connected: true
                });
                return;
            }
        }

        ns.ws('GET', 'v1/session' + (vCode ? '?vCode=' + vCode : ''), options).then(function (result) {
            delete params.vCode;
            $h.data('lastQuery', now);
            ns.setAuthUi(result.connected);
            nextStep(result);
            ns.showMessages(result);
        });
    };

    $.fn.loadPersonObjDisplay = function (person) {
        var $pd = $(this);
        $pd.empty();
        if (person.address) {
            $pd.appendDiv(person.address.name, 'address-name');
            $pd.appendDiv([
                person.address.street1,
                person.address.street2
            ], 'address-street');

            if (person.address.postal_code) {
                var t = person.address.postal_code.code ? person.address.postal_code.code : '';
                t += (t == '' ? '' : ' ');
                t += person.address.postal_code.city ? person.address.postal_code.city : '';
                t += person.address.postal_code.country ? (' (' + person.address.postal_code.country + ')') : '';
                $pd.appendDiv(t, 'address-city');
            }

            $pd.appendDiv(person.address.misc, 'address-misc');
        }

        $pd.appendDiv(person.label, 'person-label');
        $pd.appendDiv(person.email, 'person-email');
        if (person.phone)
            $pd.appendDiv(loc.phoneAbrev + ': ' + person.phone, 'person-phone');
        if (person.fax)
            $pd.appendDiv(loc.faxAbrev + ': ' + person.fax, 'person-fax');

        $pd.appendDiv(person.misc, 'person-misc');
        return $pd;
    };

    $.fn.loadPersonObj = function (person) {

        var $pd = $(this);
        if (!$pd.is('.personDisplay')) {
            $pd.addClass('personDisplay');
        }

        if (person && person.id) {
            $pd
                .loadPersonObjDisplay(person)
                .data('person', person)
                .data('personid', person.id);
        } else {
            $pd
                .html(loc.fillLater)
                .data('person', null)
                .data('personid', null);
        }
        return $pd;
    };

    // Visible password logic
    $.fn.initPasswd = function () {
        var $tt = $(this);
        $tt.find('.fa-eye').click(function (e) {
            var $pwv = $tt.find('input[name=passwordVis]'),
                $pw = $tt.find('input[name=password]'),
                hidden = $pw.is(':visible');
            var $s = hidden ? $pw : $pwv;
            var $t = hidden ? $pwv : $pw;
            $t.val($s.val());
            $s.val('');
            $t.toggle(true);
            $s.toggle(false);
            if (hidden) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    };

    /**
     * Get the modal.
     * 
     * Load and init it if it does not exists.
     */
    $.fn.openModal = function (id, fileId, onLoad, data, style) {
        var selector = '#' + id,
            $modalW = $(selector);

        if ($modalW.length == 0) {

            console.log('Loading modal ' + id);

            $modalW = $('<div id="' + id + '" class="modalWindow"><div class="window"' + (style ? ' style="' + style + '"' : '') + '></div></div>')
                .appendTo('body', this);

            $('#' + id + ' .window').load(ns.locDir() + fileId + ".html")
                .then(function () {
                    onLoad($modalW, true, data);
                    $('<a href="#" class="closeBt"><i class="fa fa-times"></i></a>').appendTo('.modalWindow .header').click(function (e) {
                        e.preventDefault();
                        $(this).closest('.modalWindow').toggleFade(false);
                    });
                    $modalW.toggleFade(true);
                });
        } else {
            onLoad($modalW, false, data);
            $modalW.toggleFade(true).find('.feedBack').empty();
        }

        return $modalW;
    };

    window.onLogin = function (result) {
        ns.setAuthUi(result.connected);
        this.ns.goHome();
        if (window.opener) {
            if (window.opener.onLogin) {
                window.opener.onLogin();
            } else {
                window.opener.location.reload();
            }
        }
        if (result) {
            window.setTimeout(function () {
                ns.showMessages(result, { reset: true });
            }, 500);
        }

    };

    ns.addEvent = function (parent, selector, eventName, callback) {
        var els = parent.querySelectorAll(selector);
        for (var i = 0; i < els.length; ++i) {
            $(els[i]).on(eventName, callback);
        }
    };

    ns.addLoginEvent = function (parent, selector, task) {
        ns.addEvent(parent, selector, 'click', function (e) {
            e.preventDefault();
            $(window).openLogin(task);
        });
    };

    // Init session links
    ns.initSessionLink = function (element) {
        //ns.addLoginEvent(element, '.loginLnk', 'log');
        ns.addLoginEvent(element, '.passwordRecLnk', 'pw');
    };

    // Session links
    $.fn.openLogin = function (mode, options) {

        if (mode == 'log') ns.logEvent('Session', 'openLogin', 'Open login form')
        else if (mode == 'reg') ns.logEvent('Session', 'openRegister', 'Open register form')
        else if (mode == 'pw') ns.logEvent('Session', 'passwordRec', 'Open password recovery');

        $(this).openModal('signInWindow', 'signIn', function ($modal, init) {
            if (init) {

                // Submit logic
                $modal.find('#signInBt,#regBt,#sendBt').on('click', function (e) {
                    e.preventDefault();
                    var $f = $(this),
                        $m = $f.closest('.modalWindow');

                    var formOptions = $m.data('options');

                    var wMode = $m.data('mode');
                    // check licence
                    var req = {
                        login: $m.find('input[name=email]').val(),
                        password: $m.find('input[name=passwordVis]').val() + $m.find('input[name=password]').val(),
                        persistent: true,
                        validationUrl: window.location.origin + ns.locDir() + 'id-settings.html'
                    };
                    var action = 'login';
                    if (wMode == 'reg') {
                        if (!$m.find('.agreeLicence').is(':checked')) {
                            return;
                        }
                        action = 'register';
                        if (formOptions && formOptions.adminPerson) {
                            req.adminPerson = formOptions.adminPerson;
                        }
                    } else if (wMode == 'pw') {
                        action = 'passwordRecall';
                    }
                    ns.logEvent('Session', 'submit' + action, 'Submit ' + action);

                    ns.ws('POST', 'v1/session?action=' + action, req, {
                        feedback: '#signInWindow .feedBack',
                        indicator: '#signInWindow .ajax-indicator'
                    })
                        .then(function (result) {
                            if (wMode == 'pw') {
                                // Display message and stay on page
                            } else if (wMode == 'reg') {
                                if (window.onRegister) {
                                    window.onRegister(result);
                                } else if (window.onLogin) {
                                    window.onLogin(result);
                                }
                            } else {
                                if (window.onLogin) {
                                    window.onLogin(result);
                                }
                            }
                        }).catch(function (result) {
                            ns.logEvent('Session', 'submitError', 'Submit Error');
                            ns.logError("session query error " + action);
                        });
                });

                // Mode swith logic
                $modal.on('changeMode', function (e, mode) {
                    var $t = $(this),
                        $els = $t.find('.reg, .log, .pw'),
                        sel = '.' + mode;
                    $els.filter(sel).toggle(true);
                    $els.not(sel).toggle(false);
                    $t.data('mode', mode);
                    $t.find('.feedBack').empty();
                });

                $modal.find('.loginLnk').click(function () {
                    $modal.trigger('changeMode', ['log']);
                }).
                    end().find('.passwordRecLnk').click(function () {
                        $modal.trigger('changeMode', ['pw']);
                    });

                $modal.initPasswd();

                $modal.find('.agreeLicence').on('change', function (e) {
                    $('.licenceWarn').toggle(!$(this).is(':checked'));
                });

                $modal.find('.oauth-signin').click(function (e) {
                    var $t = $(this);
                    var $m = $t.closest('.modalWindow');
                    var mode = $m.data('mode');
                    if (mode == 'reg' && !$m.find('.agreeLicence').is(':checked')) {
                        return;
                    }
                    var param = {
                        action: mode
                    };

                    var o = $m.data('options');
                    if (o && o.adminPerson) {
                        param.adminPerson = o.adminPerson;
                    }
                    var system = $t.data('system');
                    ns.logEvent('Session', system + 'oauthSubmit', system + ' OAuth submit');

                    param.nextUrl = ns.vue3Url + '/invoicing';
                    ns.oauthSignIn(system, param);
                });

                // No thanks link
                $modal.find('#noThanksLnk').toggle(options && options.noThanks).on('click', function (e) {
                    ns.goHome();
                });
            }
            $modal.trigger('changeMode', [mode]);
            $modal.data('options', options);

            if (options) {
                var $emailField = $modal.find('input[name=email]');
                if (options && options.adminPerson && options.adminPerson.email && $emailField.val() == '') {
                    $emailField.val(options.adminPerson.email);
                }
            }
        });
    };

    // Share link toggle
    var shareLnk = document.getElementById('shareLnk');
    if (shareLnk)
        shareLnk.onclick = function () {
            var divs = document.querySelectorAll('.shaIt'), i;
            for (i = 0; i < divs.length; ++i) {
                divs[i].classList.toggle('invisible');
            }
            return false;
        };

    ns.showAd = function (ad) {
        var $adPHolder = $('#advertising');
        $adPHolder.empty();
        if (ad) {
            var txt = '<div class="ad" data-adid="' + ad.id + '"><a href="#" class="cls"><i class="fa fa-times"></i></a>';
            txt += '<h1><a href="' + ad.link + '" class="lnk" target="_blank">' + ad.title + '</a></h1>';
            txt += '<p>' + ad.text + '</p>';
            txt += '<address><a href="' + ad.link + '" class="lnk" target="_blank">' + ad.display_link + '</a></address>';
            $adPHolder.append(txt);

            $('.cls', $adPHolder).click(function (e) {
                var $ad = $(this).closest('.ad');
                var adId = $ad.data('adid');

                ns.ws('PUT', 'v1/ad/' + adId + '/dismiss')
                    .then(function (result) {
                        $ad.remove();
                    });
            });
            $('.lnk', $adPHolder).click(function (e) {
                var adId = $(this).closest('.ad').data('adid');
                ns.ws('PUT', 'v1/ad/' + adId + '/click');
            });
        }
    };

    ns.nextAd = function () {
        var $adPHolder = $('#advertising');
        if ($adPHolder.length > 0) {

            ns.ws('GET', 'v1/ad/next')
                .then(function (result) {
                    ns.showMessages(result);
                    ns.showAd(result.ad);
                })
                .catch(function (xhr) {
                    if (xhr)
                        ns.showMessages(xhr.jsonResp);
                });
        }
    };

    /**
     * Generate the menu thanks based on action items.
     */
    ns.createActionMenu = function ($cont, items, actionsObj) {
        if (items) {
            $cont.empty();
            var itemNb = items.length;
            for (var i = 0; i < itemNb; i++) {
                var item = items[i];
                if (item.children) {
                    if (item.children.length > 0) {
                        var $n1 = $('<div><a href="#"><i class="fa ' + ns.icon[item.type] + '"></i><span class="lbl">' + item.label + '</span></a><div class="n1"></div></div>').appendTo($cont).find('.n1');
                        ns.createActionMenu($n1, item.children, actionsObj);
                    } else {
                        console.log('Empty sub-menu');
                    }
                } else {
                    $('<div><a href="#" id="' + item.key + (item.confirm ? '" data-confirm="' + item.confirm : '') + '"><i class="fa ' + ns.icon[item.type] + '"></i><span class="lbl">' + item.label + '</span></a></div>').appendTo($cont);
                    if (item.data) {
                        $cont.find('a').data('postit', item.data);
                    }
                }
            }
        }

        var $mItems = $cont.find('a');
        if ($mItems.length == 0) {
            $('<div><a href="#" id="close"><i class="fa ' + ns.icon['close'] + '"></i></a></div>').appendTo($cont);
            $mItems = $cont.find('a');
        }

        $mItems.data('actionsObject', actionsObj).click(function (e) {
            var a = $(this).closest('a'),
                attrId = a.attr('id'),
                id = attrId + 'Act',
                ao = a.data('actionsObject'),
                postit = a.data('postit'),
                f = ao[id];
            if (a.attr('href') == "#") {
                e.preventDefault();
            }
            if (postit) {
                ns.doPost(postit);
            } else if (f) {
                f.debounce(300, true).bind(ao)();
            } else if (attrId == undefined) {
                // Do nothing if no id defined
            } else {
                var message = $(window).data('message');
                ns.ws('POST', 'v1/message/' + message.id + '/' + attrId);

                // If confirmation : open windows with confirmation
                console.log('Action ' + attrId + ' not defined');
            }
        });
    };

    /* Trumbowyg HTML editor options */
    ns.html4pdfButtons = [
        ['viewHTML'],
        ['undo', 'redo'],
        ['formatting'],
        ['strong', 'em', 'del', 'underline'],
        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
        ['removeformat'],
        ['fullscreen']
    ];

    ns.xmlForPdfFilter = function (inStr) {
        return inStr == null ? null : inStr.replace('<o:OfficeDocumentSettings>.*?<\/o:OfficeDocumentSettings>', '')
            .replace('<o:OfficeDocumentSettings>.*?<\/o:OfficeDocumentSettings>', '')
            .replace('<w:WordDocument>.*?<\/w:WordDocument>', '')
            .replace('<style>.*?<\/style>', '')
            .replace('<!.+?-->', '')
            .replace('\sclass=".+?"', '');
    };

    /**
     * Common page initialization.
     */
    $(document).ready(function () {

        $(document).on('click', '.close', function (e) {
            e.preventDefault();
            $(this).closest('.modalWindow').css({
                "opacity": 0,
                "pointer-events": "none"
            });
        }.debounce());

        $('#supportLnk').click(function (e) {
            e.preventDefault();
            $(window).openModal('supportWindow', 'support', function ($modal, init) {
                if (init) {
                    $modal.find('form .mainact').on('click', function (e) {
                        e.preventDefault();
                        var $f = $(this).closest('form');
                        var loca = window.location;
                        var content = $("textarea[name='content']", $f).val();
                        var phone = $("input[name='phone']", $f).val();
                        if (phone) {
                            content += ('Phone:' + phone);
                        }
                        var suppReq = {
                            reason: 'NOT_WORKING',
                            content: content,
                            email: $("input[name='email']", $f).val(),
                            hint: loca.path + loca.hash + loca.search
                        };
                        //reason content
                        ns.ws('POST', 'v1/userFeedback', suppReq, {
                            feedback: '#supportWindow .support',
                            indicator: '#supportWindow .ajax-indicator'
                        })
                            .then(function (result) {
                                if (ns.isAuth()) {
                                    $('#supportWindow').toggleFade(false);
                                } else {
                                    setTimeout(function () {
                                        $('#supportWindow').toggleFade(false)
                                    }, 3000);
                                }
                            });
                    });

                    if (1) {
                        ns.ws('GET', 'v1/person?for=admin')
                            .then(function (result) {
                                var pers = result.persons;
                                var person = pers && pers[0] && pers[0].person;
                                if (person.email) {
                                    $modal.find("input[name='email']").val(person.email);
                                }
                                if (person.phone) {
                                    $modal.find("input[name='phone']").val(person.phone);
                                }
                            })
                    }
                }
            });
        });

        var $sh = $('#auth-status');
        if ($sh.length > 0) {
            ns.testSession(function (result) {
                if (result.connected && result['user-name']) {
                    $('#auth-status').html(result['user-name']);
                }
                console.log(result);
            });
        }

        ns.initSessionLink(document);

        $('#logoutLnk').click(function (e) {
            var path = ns.serverUrl;
            if (path != '/') {
                if (path.startsWith('http')) {
                    var firstSl = path.substr(10).indexOf('/');
                    if (firstSl > -1) {
                        path = path.substr(firstSl + 10);
                    }
                }
                if (path.endsWith('/')) {
                    path = path.substring(0, path.length - 1);
                }
            }
            document.cookie = 'rememberMe=; path=' + path;
            ns.ws('POST', 'v1/session?action=logout')
                .then(function (result) {
                    if (localStorage) {
                        localStorage.clear();
                    }
                    window.location = ns.locDir() + 'welcome.html';
                });
        });

        $('.menu .opener').click(function (e) {
            e.preventDefault();
            $(this).parent().find('.options').toggle();
        });

        // Upon external completion
        var hash = window.location.hash;

        if (hash && hash.startsWith('#msg')) {
            var levels = {
                ok: 'infos',
                payOk: 'infos',
                ko: 'errors',
                cancel: 'warnings',
                wait: 'warnings'
            },
                hashParams = hash.split(new RegExp('[&\?]')),
                msg = hashParams[0].substr(4);
            ns.addNextMessage(levels[msg], loc[msg] ? loc[msg] : msg);
            window.location.hash = '';
        } else if (hash.indexOf('reg') == 1) {
            $(window).openLogin('reg');
            ns.logEvent('Session', 'openRegister', 'Open register form');
        } else if (hash.indexOf('log') == 1) {
            $(window).openLogin('log');
            ns.logEvent('Session', 'openLogin', 'Open login form');
        }
    });
});

window.onerror = function (msg, url, lineNo, columnNo, error) {
    //ns.clientReport({ message: msg, url: url, lineNo: lineNo, columnNo: columnNo, error: error });
    return false;
}

