"use strict";

$(document).ready(function() {
    window.onLogin = function(result) {
        this.location.href = ns.serverUrl;
        if (result) {
            setTimeout(function() {
                ns.showMessages(result, {reset: true});
            }, 1000);
        }
    };
    $(window).openLogin('log');
});