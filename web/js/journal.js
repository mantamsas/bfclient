"use strict";

/*global ns Vue $ window*/
ns.journal = {};

ns.journal = new Vue({
  el: "#journal",
  data: {
    writings: [],
    loading: false,
    noContent: false,
    count: 0,
    writing_count: 0,
    hasCrit: false,
    crit: {
      label: null,
      accountPath: null,
      status: null,
      debit: null,
      credit: null,
      assetPath: null,
      from: null,
      periodId: null,
      journalCode: 'OD'
    },
    page: 1,
    pagesize: 2000,
    keepFeedback: true,
    filterOn: false,
    assetLabel: ''
  },
  mounted: function () {
    var that = this,
      dp = ns.setDatePicker("#dateCrit"),
      defAsset = ns.accounting.defAsset,
      defPeriod = ns.accounting.defPeriod;

    if (defAsset) {
      that.crit.assetPath = defAsset.value;
      that.assetLabel = defAsset.text;
    }

    if (defPeriod) {
      that.crit.periodId = defPeriod.value;
    }

    dp.on("statechange", function (_, piker) {
      that.crit.from = Math.round(piker.state.selectedDate.getTime() / 1000);
    });
    this.loadList();
    ns.nextAd();
  },
  methods: {
    openWriForm: function (event, writObj, clone) {
      var that = this;
      ns.requires("editWriting", "editWriting.js").then(function (formObj) {
        formObj.load(writObj);
        formObj.onSuccess = function (result) {
          console.log("Journal add writing success");
          if (writObj && (!writObj.id || clone)) {
            that.loadList();
          }
        };
      });
    },
    validMovesNb: function (writObj) {
      var nb = 0;
      for (var i = 0; i < writObj.moves.length; i++) {
        var mv = writObj.moves[i];
        if (mv && ((mv.account && mv.account.label) || mv.amount)) nb++;
      }
      return nb;
    },
    openAccRep: function (account) {
      if (account) {
        window.location.href =
          "./accountReport.html?accountPath=" + account.path;
      }
    },
    openImportWriting: function (event) {
      var that = this;
      ns.requires("importWriting", "importWriting.js").then(function (formObj) {
        formObj.load();
        formObj.onSuccess = function () {
          console.log("Journal add writing success");
          that.loadList();
        };
      });
    },
    // Reload if crit changes
    changeCrit: function (name, val) {
      console.log("Crit " + name + " changed " + val);
      this.crit[name] = val;
      this.loadList();
    },
    // Load from database
    loadList: function (pageNb) {
      this.loading = true;
      if (pageNb) {
        this.page = pageNb;
      }

      // Get data from filter
      var that = this,
        url =
          "v1/accounting/writing?count_writing=true&pageSz=" +
          this.pagesize +
          "&pageNb=" +
          this.page;

      var urlLen = url.length;

      function addCrit(name) {
        var cr = that.crit[name];
        if (cr) {
          url +=
            "&" + name + "=" + encodeURIComponent(cr.trim ? cr.trim() : cr);
        }
      }

      addCrit("label");
      addCrit("accountPath");
      addCrit("status");
      addCrit("debit");
      addCrit("credit");
      addCrit("assetPath");
      addCrit("periodId");
      addCrit("from");
      addCrit("journalCode");

      this.hasCrit = url.length > urlLen;

      // Query and render result
      ns.ws("GET", url, null, {
        keepFbk: this.keepFeedback
      })
        .then(function (result) {
          that.noResults = !(
            result.writings && result.writings.length != undefined
          );
          if (result.writing_count) that.writing_count = result.writing_count;
          that.count = result.count;

          if (that.noResults) {
            that.writings = [];
          } else {
            // Update table
            var wrs = result.writings;
            for (var i = 0; i < wrs.length; i++) {
              wrs[i].moves = wrs[i].moves.sort(ns.journal.moveCompare);
            }
            that.writings = wrs;
          }
          that.loading = false;
        })
        .catch(function (xhr) {
          ns.showMessages(xhr.jsonResp);
          console.error("Journal load error");
          that.loading = false;
        });
    },
    formatAmount: function(value) {
      return value ? value.formatMoney().replace(" ", "&nbsp;") : "";
    },
    formatDate: function(value) {
      const date =  new Date(value * 1000);
      return date.toLocaleDateString(loc.locale, { year: 'numeric', month: 'numeric', day: 'numeric',  timeZone: 'UTC'}) + ' ' + date.toLocaleTimeString(loc.locale, { timeZone: 'UTC'});
    }
  }
});

ns.journal.moveCompare = function (a, b) {
  var ca = a.asset.path + (a.direction == "CREDIT") + a.account.path;
  var cb = b.asset.path + (b.direction == "CREDIT") + b.account.path;
  if (ca < cb) {
    return -1;
  } else if (ca > cb) {
    return 1;
  }
  return b.amount - a.amount;
};
