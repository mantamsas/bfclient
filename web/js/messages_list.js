"use strict";

ns.message_list = {};

ns.message_list.view = new Vue({
  el: "#message_list_view",
  data: {
    params: {
      filter: {},
      order: "",
      reverseOrder: false,
      orderval: 0,
      pageSize: 10,
      trash: false
    },
    loading: true,
    filtered: false,
    messages: [],
    ordervals: [],
    total_count: -1,
    showFilterRow: false,
    now: Math.floor(Date.now() / 1000),
    userActions: {},
    newDocOpen: false,
    feeNote: "",
    noVat: "",
    priceVatIncluded: ""
  },

  methods: {
    loadList: function (rekonTotal) {
      // Build query URL
      var that = this,
        url = "v1/message_list?pageSz=10",
        fKeys = Object.keys(this.params.filter);
      this.filtered = false;

      if (this.params.orderval) {
        url += ("&orderval=" + this.params.orderval);
      }

      for (var i = 0; i < fKeys.length; i++) {
        var fkey = fKeys[i],
          fVal = this.params.filter[fkey];
        if (fVal) {
          url += "&" + fkey + "Filter=" + (fVal.trim ? fVal.trim() : fVal);
          this.filtered = true;
        }
      }
      for (var i = 0; i < this.params.order.length; i++) {
        url += "&orderBy=" + this.params.order[i].trim();
      }

      if (this.params.trash) {
        url += "&trash=true";
      }

      if (this.total_count > -1 && !rekonTotal) {
        url += "&rekonTotalCount=false";
      }

      return ns
        .ws("GET", url, null, { cache: true, handleError: true })
        .then(function (result) {
          that.loading = false;
          if (!result)
            return;

          var messages =
            (result.all_messages && result.all_messages.messages) || [];

          for (var i = 0; i < messages.length; i++) {
            var m = messages[i];
            m.showMenu = false;
            if (!m.message_number) {
              m.message_number = loc.toIssue;
            }
            if (!m.subject || !m.subject.trim()) {
              m.subject = loc.fillLater;
            }
          }

          that.messages = messages;
          if (result.all_messages.count)
            that.total_count = result.all_messages.count;
          that.userActions = result.userActions;
        })
        .catch(function (xhr) {
          that.loading = false;
          ns.logError(xhr);
        });
    },

    search: function () {
      this.loadList(true);
    },

    filterToggle: function () {
      this.showFilterRow = !this.showFilterRow;
    },

    setOrder: function (col) {
      if (this.params.order == col) {
        this.params.reverseOrder = !this.params.reverseOrder;
      } else {
        this.params.order = col;
        this.params.reverseOrder = false;
      }
      this.loadList();
    },

    messageUrl: function (message) {
      return (
        (message.content_type == "COMMERCIAL_MESSAGE"
          ? "edit_commercialv.html"
          : "edit_generic.html") +
        "?messageId=" +
        message.message_id
      );
    },

    messageRef: function (message) {
      return message.content_type == "COMMERCIAL_MESSAGE"
        ? loc.messageTypes[message.message_type] +
        " " +
        message.message_number +
        " "
        : "";
    },

    messagePerson: function (message) {
      return message.address_label || message.person_label || "";
    },

    amount: function (message) {
      return ns.formatAmount(message.total, message.currency);
    },

    status: function (message) {
      return (
        loc.messageStatus[message.message_type + "-" + message.status] ||
        loc.messageStatus[message.status]
      );
    },

    openNewDoc: function () {
      this.newDocOpen = true;
    },

    menuShown: function (message) {
      return "menu " + (message.showMenu ? "on" : "off");
    },

    actionUrl: function (message, action) {
      var url = "#";
      if (action == "PRINT") {
        var mn = message.message_number ? message.message_number : "document";
        url =
          ns.serverUrl +
          "pdf-docm/mId-" +
          message.message_id +
          "/" +
          encodeURIComponent(mn.replace(/[?\/#=]+/g,'-')) +
          ".pdf";
      } else if (action == "DUPLICATE") {
        url = this.messageUrl(message) + "&action=duplicate";
      }
      return url;
    },

    actionProp: function (act, propName) {
      return this.userActions[act] && this.userActions[act][propName];
    },

    actionTitle: function (message) {
      var mn = message.message_number ? message.message_number : "document";
      return ns.serverUrl + "pdf-docm/mId-" + message.id + "/" + mn + ".pdf";
    },

    dateChange: function ($event) {
      this.params.page = 1;
      this.params.toDate = $event.target;
    },

    doAction: function ($event, act, message) {
      var that = this,
        messageId = message.message_id;
      act = act && act.toUpperCase();
      if (act == "PRINT" || act == "DUPLICATE") {
        return true;
      }

      var updateIfSuccess = function (result) {
        if (!result.warnings && !result.errors) {
          that.loadList();
        }
      };

      $event.preventDefault();
      if (act == "ADD_PAYMENT") {
        ns.requires("paymentForm", "payment.js").then(function (paymentForm) {
          paymentForm.list = that;
          paymentForm.load(messageId, message.to_pay, message.currency);
          paymentForm.onSuccess = function (result) {
            this.list.loadList();
            ns.showMessages(result, { reset: true });
          };
        });
      } else if (act == "TRASH") {
        ns.ws("DELETE", "v1/message/" + messageId + "?onlyTrash=true")
          .then(function (result) {
            console.log("Trashing message " + messageId);
            updateIfSuccess(result);
          })
          .catch(function (xhr) {
            console.log("Trashing message " + messageId + "  error ");
            ns.logError(xhr);
          });
      } else if (act == "DELETE") {
        ns.ws("DELETE", "v1/message/" + messageId).then(function (result) {
          console.log("Deleting message " + messageId);
          updateIfSuccess(result);
        });
      } else if (act == "RESTORE") {
        ns.ws("POST", "v1/message/" + messageId + "/restore").then(function (
          result
        ) {
          console.log("Restoring message " + messageId);
          updateIfSuccess(result);
        });
      } else {
        console.log('Message list action "' + act + '" not implemented!');
      }
    },

    /* Got to page before date of last message in list  */
    goPreviousPage: function() {
      if(!this.messages || this.messages.length == 0)
        return;
      const before = this.messages[this.messages.length - 1].orderval - 1;
      this.ordervals.push(before);
      this.params.orderval = before;
      this.loadList();
    },

    /* Got to page after  */
    goNextPage: function() {
      if(this.ordervals.length > 0) {
        this.params.orderval = this.ordervals.pop();
      }
      this.loadList();
    },

    goLastCurrentPage: function() {
      this.ordervals = [];
      this.params.orderval = 0;
      this.loadList();
    }
  },
  mounted: function () {
    this.params.page =
      window.location.hash && parseInt(window.location.hash.replace("#", ""));
    if (!this.params.page) {
      this.params.page = 1;
    }
    this.loadList();

    var that = this,
      toDate = ns.setDatePicker("#toDateFilter");

      toDate.on("statechange", function (_, piker) {
      that.params.filter.todate = Math.round(
        piker.state.selectedDate.getTime() / 1000
      );
      that.params.page = 1;
    });

    that.extraParams = "";
    ns.ws("GET", "v1/preference/noVat+priceVatIncluded+feeNote", null, {
      cache: true, handleError: true
    }).then(function (result) {
      if (!result) return;
      var keys = Object.keys(result),
        kNb = keys ? keys.length : 0;
      for (var i = 0; i < keys.length; i++) {
        var key = keys[i],
          val = result[key];
        if (
          val &&
          (key == "noVat" || key == "feeNote" || key == "priceVatIncluded")
        )
          that[key] = "&" + key + "=" + val;
      }
    });

    ns.nextAd();
  },
  computed: {
    has_before: function(){
      return this.messages.length == this.params.pageSize;
    },
    has_after: function(){
      return this.ordervals.length > 0;
    }
  }
});
