ns['doc-template-picker'] = {
  template: "#doc-template-pickerComp",
  props: {
    showw: Boolean
  },
  data: function () {
    return {
      templates: {}
    };
  },
  mounted: function () {
    console.log("Template picker loaded");
    // Get available models
    var that = this;
    ns.ws('GET', 'v1/document_template/list').then(function (result) {
      that.templates = result && result.doc_templates;});
  },
  methods: {
    selectTemplate: function(modelId) {
      var that = this;
      ns.ws("POST", "v1/preference", {ComMessageHtmlTemplate: modelId})
        .then(function(){
          ns.logEvent('Settings', 'setTemplate', "Set doc template", modelId);
          that.$emit('close');
        });
    },
    reinit: function(modelId) {
      var that = this;
      ns.ws("POST", "v1/preference", {ComMessageHtmlTemplate: null})
        .then(function(){
          ns.logEvent('Settings', 'initTemplate', "Re-init doc template");
          that.$emit('close');
        });
    },
    templateIconUrl: function(templateId) {
      return '../img/templates/' + templateId + '.jpg';
    }
  }
};

//# sourceURL=/static/js/doc-template-pickerv.js
