"use strict";

(function () {

    /**
     * Create an person edition panel
     */
    ns.personEditor = {
        getPanel: function (data) {
            return $(window).openModal('personWindow', 'personEdition',
                function ($modal, init, data2) {
                    if (init) {
                        initPersonPanel($modal);
                    }
                    setPersonPanel($modal, data2);
                },
                data);
        },
        updateContact: updateContact
    };

    var ajaxPersOpt = {
        indicator: "#personIndicator"
    };

    /**
     * Did once the person edition panel is loaded.
     */
    function initPersonPanel() {
        console.log('Init person form window');

        $('.form input[type=text], .form textarea').each(function (index) {
            $(this).attr("placeholder", loc.placeholder[this.id]);
        });

        $('#submitPerson').on('click', function (e) {
            e.preventDefault();
            var values = {
                person: {}
            },
                person = values.person,
                $pw = $('#personWindow'),
                forAction = $pw.data('for'),
                isBlank = true;

            $('.form input[type=text], .form textarea, .form select').each(function (index) {
                var val = $(this).val(),
                    keyElem = this.id.split("-");

                if (val != null && val != "") {
                    if (!this.id || this.id.indexOf('country') == -1)
                        isBlank = false;
                    if (keyElem[0] == 'address') {
                        if (!person.address) {
                            person.address = {};
                        }
                        person.address[keyElem[1]] = val;
                    } else if (keyElem[0] == 'postal_code') {
                        if (!person.address) {
                            person.address = {};
                        }
                        if (!person.address.postal_code) {
                            person.address.postal_code = {};
                        }
                        person.address.postal_code[keyElem[1]] = val;
                    } else {
                        person[keyElem[1]] = val;
                    }
                }
            });

            if (isBlank) {
                $pw.trigger('ns-select', [{}, loc.fillLater, $pw]);
            } else {
                // Pass other values from form
                if ($pw.data('orgid')) {
                    values.org = {
                        id: $pw.data('orgid')
                    };
                }
                if ($pw.data('contactid')) {
                    values.contact = {
                        id: $pw.data('contactid')
                    };
                }
                person.id = null; // To make the API resolve a new address
                values.validationUrl = window.location.href;

                ns.ws('POST', 'v1/person' + (forAction ? '?for=' + forAction : ''), values, {
                    indicator: "#personIndicator",
                    feedback: '#personWindow .feedBack'
                })
                    .then(function (result) {
                        console.log('Created address');
                        console.log(result);

                        if (result.persons && result.persons.length > 0) {
                            var addressRow = result.persons[0];
                            $('#personWindow').trigger('ns-select', [addressRow, $pw]);
                            addPerson2Window(addressRow.person);
                        } else {
                            $pw.trigger('ns-select', [{}, loc.fillLater, $pw]);
                        }
                    }).catch((resp)=>{
                        console.log(resp);
                    });
            }
        }.debounce(200, true));

        $('<div class="result">' + loc.noResultsSearch + '</div>').appendTo('.results');

        $('.person-search-input').on('keyup', function (e) {
            var val = $(e.target).val();
            if (val.length > 2) {

                var forAction = $('#personWindow').data('for');
                ns.ws('GET', 'v1/person?s=' + encodeURIComponent(val) + (forAction ? ('&for=' + forAction) : ''), null, ajaxPersOpt)
                    .then(function (result) {
                        console.log('Searching address with ' + val + ': ');
                        console.log(result);

                        var $res = $('.results'),
                            $pw = $('#personWindow');

                        $('#newAddressLnk').toggle(!($pw.data('hidenew')));
                        $res.toggle(true).empty();
                        $('.form').toggle(false);
                        var hasAddress = result.persons && result.persons.length > 0;

                        if (hasAddress) {
                            for (var rowIdx = 0; rowIdx < result.persons.length; rowIdx++) {
                                var addressRow = result.persons[rowIdx];
                                var person = addressRow.person;

                                var $addr = $('<div class="result intAddr"></div>');
                                $addr.loadPersonObj(person);
                                $addr.prepend('<div style="float:right; font-weight: normal">' + new Date(person.updated * 1000).format(loc.date.dateFormat) + '</div>');
                                $addr.appendTo($res);
                            }

                            $('.intAddr').on('click', function (e) {
                                e.preventDefault();
                                var $r = $(this);

                                var personId = $r.data('person') ? $r.data('person').id : $r.data('personid');

                                if (personId) {
                                    var values = {
                                        person: {
                                            id: personId
                                        }
                                    };
                                    if ($pw.data('contactid')) {
                                        values.contact = {};
                                        values.contact.id = $pw.data('contactid');
                                    }
                                    var forAction = $pw.data('for');
                                    values.validationUrl = window.location.href;

                                    ns.ws('POST', 'v1/person' + (forAction ? '?for=' + forAction : ''), values, ajaxPersOpt)
                                        .then(function (result) {
                                            console.log('Created address');
                                            console.log(result);

                                            $('#personWindow').trigger('ns-select', [$r.data(), $pw]);
                                            addPerson2Window($r.data('person'));
                                        });
                                } else {
                                    $('#personWindow').trigger('ns-select', [$r.data(), $pw]);
                                    addPerson2Window($r.data('person'));
                                }
                            }.debounce(100, true));
                        }

                        var hasGoogleContact = result.googleContacts && result.googleContacts.length > 0;
                        if (hasGoogleContact) {
                            for (var rowIdx = 0; rowIdx < result.googleContacts.length; rowIdx++) {
                                var person = result.googleContacts[rowIdx];

                                var $addr = $('<div class="result extAddr"></div>');
                                $addr.data('person', person).loadPersonObjDisplay(person);
                                $addr.appendTo($res);
                            }

                            $('.extAddr').on('click', function (e) {
                                e.preventDefault();
                                var $r = $(this);
                                setPersonPanel($('#personWindow'), $r.data());
                            }.debounce(100, true));
                            ns.logEvent('Integration', 'gotGoogleContact', null, result.googleContacts.length);
                        }

                        if (!hasAddress && !hasGoogleContact) {
                            var explain = loc.noResultsSearchFor;
                            if (explain) {
                                explain = explain.replace('{0}', val);
                            }
                            $('<div class="result">' + explain + '</div>').appendTo($res);
                        }
                    });
            }
        }.debounce(300, true));

        $('#fillLaterLnk').click(function (e) {
            console.log("Address set to fill-later");
            var $r = $(e.target).closest('.result');
            e.preventDefault();
            var $pw = $('#personWindow');
            $pw.trigger('ns-select', [{}, loc.fillLater, $pw]);
        }.debounce(200, true));

        $('#newAddressLnk').click(function (e) {
            console.log("Open form");
            e.preventDefault();
            $('.results').toggle(false);
            $('.form').toggle(true);
        }.debounce(200, true));
    };

    /**
     * Set the currently edited person
     */
    function setPersonPanel($pw, data) {
        $pw.removeData();
        for (var prop in data) {
            $pw.data(prop, data[prop]);
        }

        var person = data.person,
            personId = data.personid,
            addressId = data.addressid;

        if (person) { // use stored person obj
            mapDataToFrom({
                person: person
            });
            $pw.data('showform', true);
            toggleForm($pw);
        } else if (personId && personId != 0) { // get person obj from id
            console.log('editing person: ' + personId);
            ns.ws('GET', 'v1/person/' + personId, null, ajaxPersOpt)
                .then(function (result) {
                    console.log('Getting address ' + result);

                    if (result && result.persons && result.persons[0]) {
                        var address = result.persons[0];
                        mapDataToFrom(address);
                        $pw.data('person', address.person);
                        $pw.data('showform', true);
                        toggleForm($pw);
                    } else {
                        console.log("No person to update form in response");
                        console.log(result);
                        $pw.data('showform', false);
                        toggleForm($pw);
                    }
                });
        } else if (addressId && addressId != 0) { // get address obj from id
            console.log('editing address: ' + personId);
            ns.ws('GET', 'v1/address/' + addressId, null, ajaxPersOpt)
                .then(function (result) {
                    console.log('Getting address ' + result);

                    if (result && result.address) {
                        var address = result.address,
                            ddd = {
                                'person': {
                                    'address': address
                                }
                            };
                        mapDataToFrom(ddd);
                        $pw.data('address', address);
                        $pw.data('showform', true);
                        toggleForm($pw);
                    } else {
                        console.log("No adress to update form in response");
                        console.log(result);
                        $pw.data('showform', false);
                        toggleForm($pw);
                    }
                });
        } else { // get empty form
            mapDataToFrom();
            $('#postal_code-country').val('FR');
            toggleForm($pw);
        }
    };

    function toggleForm($pw) {
        var inD = $pw.data();
        var auth = $('html').hasClass('auth');
        var showForm = !auth || inD.showform;

        $('.form').toggle(showForm);
        $('.results').empty().toggle(!showForm);

        $('#personWindow .search').toggle(!(inD.hidesearch || !auth));
        $('#fillLaterLnk').toggle(!(inD.hideblank || !auth));
        $('#newAddressLnk').toggle(!(inD.hidenew || !auth || showForm));

        if (showForm) {
            $('#address-name').focus();
        } else {
            $('.person-search-input').focus();
        }

        $pw.trigger('ns-ready');
    }

    function mapDataToFrom(d) {

        var mapping = ['address-name',
            'address-street1',
            'address-street2',
            'postal_code-code',
            'postal_code-city',
            'postal_code-country',
            'address-misc',
            'person-label',
            'person-email',
            'person-phone',
            'person-fax',
            'person-misc'
        ];

        for (var idx = 0; idx < mapping.length; idx++) {
            var key = mapping[idx];

            var v = null;
            if (d) {
                var keyElem = key.split("-");
                if (keyElem[0] == 'address' && d.person && d.person.address) {
                    v = d.person.address[keyElem[1]];
                } else if (keyElem[0] == 'postal_code' && d.person && d.person.address && d.person.address.postal_code) {
                    v = d.person.address.postal_code[keyElem[1]];
                } else if (keyElem[0] == 'person' && d.person) {
                    v = d.person[keyElem[1]];
                }
            }

            $('#' + mapping[idx]).val(v ? v : '');
        }
    };

    function updateContact(contactId, personId, callback) {
        ns.ws('PUT', 'v1/person/' + contactId + '?personid=' + personId, null, ajaxPersOpt)
            .then(function (result) {
                console.log('Updating address ' + result);
                if (callback) {
                    callback();
                }
            });
    };

    function addPerson2Window(person) {
        var map = $(window).data('persons');
        if (!map) {
            var map = {};
        }
        map[person.id] = person;
        $(window).data('persons', map);
    }

    // JQuery extensions
    $.fn.appendIcons = function () {
        var $t = $(this);
        $t.append('<i class=""></i>');
        return $t;
    };

    /**
     * Activate person edition if hidden field present
     */
    $.fn.makeAddressEditable = function () {
        var $t = $(this);
        $t.addClass('edited')
            .each(function () {
                $(this).attr('tabindex', 0);
            })
            .click(function (e) {
                e.preventDefault();
                $('.selectedPerson').removeClass('selectedPerson');
                var $pd = $(this);
                $pd.addClass('selectedPerson');

                var $pw = ns.personEditor.getPanel($pd.data());

                $pw.off('ns-select').on('ns-select', function (e, data, $pw) { // Called when address is selected
                    $('.selectedPerson').empty()
                        .loadPersonObj(data.person)
                        .data('contact', data.contact)
                        .removeClass('selectedPerson')
                        .trigger('change');
                    console.log('Address selection to ' + (data.person && data.person.id));
                    $(this).toggleFade(false);
                });

            }.debounce(200, true));
        return $t;
    };
}());