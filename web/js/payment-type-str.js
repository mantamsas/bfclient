"use strict";

ns.logEvent('Document', 'openPaymentType');
console.log('Loading payment type panel');

ns.paymentType = {

    typeKeys: ['stripe'],

    // Go to payment system
    goToPayment: function(paymentType) {
        if(paymentType=='stripe'){
            this.doStripe();
        }
    },

    load: function(messageId, topay, currency) {

        $(window).openModal(
            'paymentTypeWindow',
            'payment-type',
            function($modal, init) {
                if (init) {
                    // Get preferences
                    var message = $(window).data('message');
                    
                    ns.paymentType.loadStripe();

                    $(".paymentBt", $modal).click(function(e) {
                        var ptype = $(this).closest('a').data('payment-type');
                        if (ptype != 'paypal') {
                            e.preventDefault();
                            ns.paymentType.goToPayment(ptype);
                        }
                    });
                }
            });
    },
    /* Load stripe if needed */
    loadStripeImp: function(deferred){
        var bt = document.querySelector("#stripeBt"),
        ai = bt.querySelector(".ajax-indicator");
        bt.disabled = true;
        ai.classList.add("on");
        ns.stripeloader.load().then(function(){
            ai.classList.remove("on");
            bt.disabled = false;
            deferred.resolve();
        }).catch(function(){
            ns.showMessages({errors: [{message: "No Stripe public key"}]});
            ai.classList.remove("on");
            deferred.reject();
        });
    },
    /* Load stripeloaded and stripe if needed */
    loadStripe: function(){
        var deferred = $.Deferred();
        if(ns.stripeloader){
            this.loadStripeImp(deferred);
        }else{
            ns.ld(['../js/stripeloader.js'], function(){
                ns.paymentType.loadStripeImp(deferred);
            });
        }
        return deferred.promise();
    },
    /* Present Stripe card form */
    doStripe: function(){
        if(ns.stripeloader.stripe){
            document.querySelector("#paymentBts").style.display='none';
            document.querySelector("#stripe-form").style.display='block';
            
            var onSubmit = $.Deferred();
            onSubmit.then(function(token){
                // Send token to server
                ns.paymentType.onSuccess(true);
            });
            
            ns.stripeloader.propose(onSubmit).catch(function(errorMsg){
                ns.showMessages({errors: [{message: "Stripe form init error, warn the administrator: " + errorMsg}]});
            });
        }else{
            var bt = document.querySelector("#stripeBt");
            if(!bt.disabled)
                ns.showMessages({errors: [{message: "Stripe not loaded! Warn the administrator."}]});
        }
    },
    onSuccess: function(result) {
        console.log('Payment type open success');
    }
};

//# sourceURL=/static/js/payment-type.js