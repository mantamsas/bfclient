"use strict";

ns.registerview = new Vue({
    el: '#register-view-identity',
    
    data: {
        person: { email: '', address: { name: null, postal_code: {} } },
        org: { year_end_month: 12 },
        country: 'FR',
        register_stripe: true,
        ajOpt: { feedback: '#signInWindow .feedBack', indicator: '#regBt .ajax-indicator' }
    },

    mounted: function () {
        var params = ns.getQueryParams();
        this.register_stripe = params.register_stripe;

        ns.ws('GET', 'v1/identity', null, this.ajOpt).then((result) => {
            var con = result.status == 'ok' && result.connected == true;
            if (con) {
                if (result.identity && result.identity.person) {
                    this.person = this.padAddress(result.identity.person);
                } else {
                    var anonmessage = ns.getLocalPref("anonmessage", null, true);
                    if (anonmessage && anonmessage.roles.sender) {
                        this.person = this.padAddress(anonmessage.roles.sender);
                    }
                    if (result['user-name']) {
                        this.person.email = result['user-name'];
                    }
                }
            } else {
                window.location.href = '/v/register/' + (this.register_stripe ? '?register_stripe=true' : '');
            }
        });
    },

    methods: {
        submitAdress: function (e) {
            this.org.name = this.person?.address?.name || this.person?.label || this.person?.email;
            this.person.address.postal_code.country = this.country;
            this.org.country = this.country;
            this.org.id = null; // new org only of no org created
            if (this.org.reg_id) {
                this.org.reg_id_name = 'SIREN';
            }
            var that = this,
                req = { person: this.person, org: this.org };

            ns.resetMessages(that.ajOpt);
            ns.ws('POST', 'v1/person?for=admin', req, this.ajOpt).then(function (regResp) {
                // Get previous anonymously created message
                var message = ns.getLocalPref("anonmessage", null, true);
                if (message)
                    ns.ws("POST", "v1/message?reattach=true", message, that.ajOpt).then(function () {
                        ns.setLocalPref("anonmessage", null, true);
                    });
            }).then(function () {
                window.location.href = '/v/invoicing/subscription/?next_step=' + encodeURIComponent('register_welcome.html') + (that.register_stripe ? '?register_stripe=true' : '');
            });
        },
        padAddress: function(person) {
            if(!person.address) {
                person.address = {name: null, street1: '', street2: '', misc: ''};
            }
            if(!person.address.postal_code)
            person.address.postal_code = {code: ''};
            return person;
        }
    }
});