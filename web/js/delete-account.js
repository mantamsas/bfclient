"use strict";

ns.deleteAccountVue = new Vue({
    el: '#deletePage',
    methods: {
        // Load from database
        deleteAccount: function() {
            ns.ws('DELETE', 'v1/session');
        }
    }
});