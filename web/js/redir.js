"use strict";

// Redirection script depending on connected state and locale
ns.testSession(function(result) {
    var locale = ns.defaultLoc || (result && result.locale);
    if(result && result.connected) {
        window.location.href = ns.vue3Url + '/invoicing';
    } else {
        window.location.href = ns.clientUrl + locale + '/welcome.html';
    }    
});