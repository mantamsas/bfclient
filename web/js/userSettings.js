"use strict";

ns.userSettings = {};

$(document).ready(function () {

    // Getting user preferences and information
    if ($('#login').length > 0)
        ns.ws('GET', 'v1/session', null, { handleError: true })
            .then(function (result) {
                $('#login').setValue(result['user-name']);
                $('#regSince').setValue(result.registeredOn);

                $('#localeSelector').val(result.locale);
            });

    $('#passwordChange').initPasswd();

    $('#changePasswordBt').click(function (e) {
        e.preventDefault();

        var $t = $(this);
        var $f = $t.closest('form');
        var req = {
            password: $f.find('input[name=passwordVis]').val() + $f.find('input[name=password]').val()
        }

        ns.ws('POST', 'v1/session?action=changePassword', req, {
            indicator: '.rightMenu .ajax-indicator',
            feedback: '#passwordFeedback'
        });
    }.debounce(200, true));

    // Locale change

    $('#localeSelector').change(function (e) {
        e.preventDefault();

        var val = $('#localeSelector').val();

        ns.ws('POST', 'v1/preference', {
            locale: val
        }, {
            feedback: '#localeFeedback'
        }).then(function () {
            window.location.href = '../' + val + '/user-settings.html';
        });
    });

    // OAuth connection

    function displayAuth($div) {
        var sys = $div.data('system');

        ns.ws('GET', 'v1/oauth?system=' + sys, null, { handleError: true })
            .then(function (result) {
                var connOk = result && result.auth != undefined && result.auth[0] && result.auth[0].system === sys.toUpperCase();
                if (connOk) {
                    $('.oauthEmail', $div).html(result.auth[0].label);
                }
                $('.signout, .resignin', $div).toggle(connOk);
                $('.signin', $div).toggle(!connOk);
            });
    }

    $('.oauthDiv').each(function () {
        displayAuth($(this));
    });

    $('.oauthDiv .signin, .oauthDiv .resignin').click(function (e) {
        var $div = $(this).closest('.oauthDiv');
        window.onLogin = function () {
            displayAuth($div);
        };
        var wArgs = {};
        if ($div.data('width')) {
            wArgs.width = $div.data('width');
        }
        if ($div.data('height')) {
            wArgs.height = $div.data('height');
        }
        ns.oauthSignIn($div.data('system'), {
            nextUrl: ns.locDir() + 'user-settings.html'
        }, wArgs);
    });

    $('.oauthDiv .signout').click(function (e) {
        var $div = $(this).closest('.oauthDiv');
        var sys = $div.data('system');
        ns.ws('DELETE', 'v1/oauth?system=' + sys)
            .then(function (result) {
                $('.signout, .resignin', $div).toggle(false);
                $('.signin', $div).toggle(true);
            });
    });
});