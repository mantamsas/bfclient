/**
 * Use and validate code
 */
(function(window) {

    // Get data from URL
    var params = ns.getQueryParams();
    var l = $('html').attr('lang');

    // Re-validate data on server
    console.log("Using code " + params.code);
    var after = function(result) {
        $('#pleaseWait').toggle(false);
        $('#backLnk').toggle(true);
    };
    ns.ws('GET', 'v1/session?lang=' + l + '&vCode=' + params.code).then(after, after);
})(this);