"use strict";

Vue.component("upload", {
    props: {
        "prefkey": String,
        "role": String,
        "type": String,
        "label": String,
        "init_label": String
    },
    template: `<form enctype="multipart/form-data" method="post" action="#" novalidate v-if="isInitial || isSaving">
      <div class="fileUploader">
        <div v-if="isSaving" class="ajax-indicator">&nbsp;</div>
        <label>{{label}}</label>
        <input type="file" :disabled="isSaving" @change="filesChange($event.target)"
          accept="image/*" class="input-file" v-if="!uploadedFile">
        <span class="uploadLevel" v-if="uploadLevel > 0">{{uploadLevel}}</span>
        <div class="fileSet" v-if="uploadedFile">
            <span>{{ uploadedFile || init_label }}</span>&nbsp;<i class="fa fa-trash" @click.prevent="deleteFile" style="cursor: pointer"></i>
        </div>
        <p id="tooBig" class="error" v-if="tooBig">Le fichier est trop gros.</p>
      </div>
    </form>`,
    data: function () {
        return {
            uploadedFile: '',
            uploadLevel: 0,
            uploadError: null,
            currentStatus: null,
            tooBig: false
        };
    },
    computed: {
        isInitial() {
            return this.currentStatus === 'initial';
        },
        isSaving() {
            return this.currentStatus === 'saving';
        },
        isSuccess() {
            return this.currentStatus === 'success';
        },
        isFailed() {
            return this.currentStatus === 'failed';
        },
        uploadUrl() {
            var res = this.type == 'image' ? 'v1/upload/image' : 'v1/upload/file';
            res += '?preference=' + this.prefkey;
            if (this.role) {
                res += '&role=' + this.role;
            }
            return res;
        }
    },
    methods: {
        reset() {
            this.currentStatus = 'initial';
            this.uploadedFile = '';
            this.uploadError = null;
            this.tooBig = false;

            var that = this;
            ns.ws('GET', this.uploadUrl, null, { handleError: true })
                .then(function (result) {
                    that.uploadedFile = (result && result.fileName) ? result.fileName : '';
                });
        },

        filesChange(input) {

            const formData = new FormData(),
                fileList = input.files;

            if (!fileList.length) return;

            if (input.size > 10000000) {
                this.tooBig = true;
            }

            this.uploadLevel = 0;
            formData.append("afile", input.files[0]);

            var that = this,
                xhr = ns.createXhr('POST', ns.serverUrl + 'api/' + that.uploadUrl, { creds: true }),
                catchFile;

            xhr.upload.onprogress = function (e) {
                if (e.lengthComputable) {
                    that.uploadLevel = Math.round(e.loaded * 100 / +e.total) + '%';
                }
            };
            xhr.onload = function () {
                var jsonResp;
                try {
                    jsonResp = JSON.parse(xhr.response);
                } catch (e) {
                    ns.logError(e);
                }
                if (this.status > 199 && this.status < 300) {
                    if (navigator.userAgent.indexOf('Chrome')) {
                        catchFile = input.value.replace(/C:\\fakepath\\/i, '');
                    } else {
                        catchFile = input.value.val();
                    }
                    that.uploadedFile = catchFile;
                } else {
                    this.currentStatus = 'failed';
                }
                that.uploadLevel = 0;
                this.currentStatus = 'success';
                if (jsonResp) {
                    ns.showMessages(jsonResp);
                }
            };

            xhr.onerror = function () {
                this.currentStatus = 'failed';
            };
            this.currentStatus = 'saving';
            xhr.send(formData);
        },

        deleteFile() {
            this.currentStatus = 'saving';
            var that = this;
            ns.ws('DELETE', this.uploadUrl, null)
                .then(function (result) {
                    that.currentStatus = 'success';
                    that.uploadedFile = '';
                });
        }
    },
    mounted() {
        this.reset();
    },
});

ns.createVueComp("doc-template-picker");

ns.printSettings = new Vue({
    el: '#print-settings-view',
    data: {
        prefs: {
            'logoReplaceName': false,
            'printSenderEmail': false,
            'printRecipientEmail': false,
            'printSenderPhone': false,
            'printRecipientPhone': false,
            'printSenderFax': false,
            'printRecipientFax': false,
            'printStatusMention': false,
            'logoFileName': '',
            'logoWidth': 0,
            'logoXOffset': 0,
            'logoYOffset': 0,
            'defaultFontSize': 10,
            'printRecipientTopMargin': 85,
            'forceFooterFont': true
        },
        loading: true,
        templatePickerOpened: false
    },
    mounted: function () {

        // Preference values load
        var that = this,
            keys = Object.keys(this.prefs);

        that.loading = true;
        ns.ws('GET', 'v1/preference/' + keys.join('+'), null, { handleError: true }).then(function (result) {
            for (var i = 0; i < keys.length; i++) {
                var key = keys[i];
                that.prefs[key] = result[key];
            }
            that.generation_mode = that.prefs.generateNumberOnIssue === true ? 'issue' : 'creation';
            that.loading = false;
        });
    }
});

