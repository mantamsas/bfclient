"use strict";

ns.moneyTransfert = {};

ns.moneyTransfert.vue = new Vue({
    el: '#moneyTransfert',
    data: {
        params: {},
        stripe_reg_param: {
            user_id: '',
            client_id: '',
            state: ''
        },
        status: 'loading',
        prefs: {
            docPaymentLink: false
        },
    },
    created: function () {
        var that = this;
        that.params = ns.getQueryParams();

        if (that.params.code && that.params.scope && that.params.state) {
            ns.ws('POST', 'v1/stripe/user_id', this.params, { handleError: true }).then(function (result) {
                that.status = 'success';
                that.stripe_reg_param.user_id = result.user_id
            });
        } else if (that.params.error) {
            that.status = 'error';
            ns.ws('POST', 'v1/stripe/user_id', that.params, { handleError: true });
        } else {
            // Get account data
            ns.ws('GET', 'v1/stripe/user_id?prepareReg=true', null, { handleError: true }).then(function (result) {
                that.stripe_reg_param = result;
            });
        }

        var keys = Object.keys(this.prefs);
        ns.ws('GET', 'v1/preference/' + keys.join('+'), null, { handleError: true }).then(function (result) {
            for (var i = 0; i < keys.length; i++) {
                var key = keys[i];
                that.prefs[key] = result[key];
            }
            that.status = 'success';
        });
    },
    methods: {
        logEvent: function (cat, action, actionLabel) {
            ns.logEvent(cat, action, actionLabel);
        },

        createAccount: function () {
            var that = this,
                returnUrl = encodeURIComponent(window.location);
            ns.ws('POST', 'v1/stripe/account?return_url=' + returnUrl, null, { handleError: true }).then(function (result) {
                if (result.stripeAccountId) {
                    that.stripe_reg_param.user_id = result.stripeAccountId;
                } else if (result.stripeCheckoutSessionId.url) {
                    window.location = result.stripeCheckoutSessionId.url;
                }
            });
            this.logEvent('Payment','integrateStripe','Integrate Stripe');
        }
    },
});
