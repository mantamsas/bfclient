"use strict";

ns.logEvent('Document', 'openPayment');
console.log('Loading payment panel');

ns.paymentForm = {

	load: function (messageId, topay, currency) {

		$(window).openModal(
			'paymentWindow',
			'payment',
			function ($modal, init) {
				if (init) {
					ns.setDatePicker('#paymentDate', new Date());
					var ajaxOpt = { indicator: '#paymentWindow .ajax-indicator', feedback: '#paymentWindow .feedBack' };

					$(".mainact", $modal).click(function (e) {
						e.preventDefault();

						var paymentRequest = {
							paymentDate: $('#paymentDate').getValue(),
							paymentType: $('#paymentType').val(),
							amount: $('#paymentAmount').getValue(),
							description: $('#paymentDesc').val(),
							closing: $('#closing').is(':checked')
						};

						if (messageId) {
							ns.ws('POST', 'v1/message/' + messageId + '/addPayment', paymentRequest, ajaxOpt)
								.then(function (result) {
									$('#paymentWindow').toggleFade(false);

									if (ns.paymentForm.onSuccess) {
										ns.paymentForm.onSuccess(result);
									}
								});
						} else {
							alert(loc.saveItFirst);
						}
					});
				}
				$("#paymentAmount").setValue(topay ? topay : '');
				$("#paymentCurrency").setValue(currency ? currency : '');
			});
	},
	onSuccess: function (result) {
		console.log('Add payment success');
	}
};

//# sourceURL=/static/js/payment.js