"use strict";

ns.passwordChange = new Vue({
    el: '#lostPassword',
    data: {
        login: 'no-login',
        code: null,
        returnLink: false,
        showForm: true,
        password: '',
        showPassword: false
    },
    mounted: function () {
        var that = this;
        ns.ws('GET', 'v1/session')
            .then(function (result) {
                that.login = result['user-name'];
            });

        var ucode = /passChangeCode=([^&]+)/.exec(window.location.href);
        this.code = ucode && ucode[1];
        if (!this.code) {
            this.showForm = false;
            this.returnLink = true;
        }
    },
    methods: {
        submitChange: function () {
            var req = {
                password: this.password,
                passwordChangeCode: this.code
            };

            var that = this;
            ns.ws('POST', 'v1/session?action=changePassword', req, {
                indicator: '#lostPassword .ajax-indicator',
                feedback: '#passwordFeedback'
            })
                .then(function (r) {
                    if (lostPassword) {
                        that.showForm = false;
                        that.returnLink = true;
                    }
                });
        },

        togglePass: function () {
            this.showPassword = !this.showPassword;
        }
    }
});
