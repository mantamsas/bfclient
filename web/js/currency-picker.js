"use strict";

Vue.component("currency-picker", {
  props: {
    value: String,
    readonly: Boolean
  },
  data: function () {
    return {
      editedval: '',
      loading: false,
      options: false,
      currencies: false
    };
  },
  watch: {
    value:  {
      handler: function (newVal) {
        this.editedval = newVal;
      },
      immediate: true 
    }
  },
  template: `
    <span>
      <template v-if="readonly">{{value}}</template>
      <div v-else class="autocomplete">
        <input type="text" v-model="editedval" @keyup="search"/>
        <div class="results" v-if="options" style="position: absolute; top:21px;white-space: nowrap; z-index: 20; right: 0; background-color:#e2e2e2">
          <div class="result" v-for="opt in options" @click="select(opt)">{{opt.label}}</div>
        </div>
      </div>
    </span>
    `,
  methods: {
    getCurrencies: function () {
      var that = this;
      return new Promise(function (accept, reject) {
        if (that.currencies) {
          accept(that.currencies)
        }
        var curs = ns.getLocalPref("currencies", null, true);
        if (curs) {
          that.currencies = curs;
          accept(curs);
        }
        ns.ws(
          "GET",
          "v1/currency"
        ).then(function (result) {
          that.currencies = result.currencies
          ns.setLocalPref("currencies", that.currencies, true);
          accept(that.currencies);
        });
      });
    },

    search: function (event) {
      var that = this,
        val = event.target.value.toUpperCase();
      if (val.length > 1) {
        that.getCurrencies().then(function (curs) {
          that.options = [];
          for (let i = 0; i < curs.length; i++) {
            var cur = curs[i];
            if (cur.code.indexOf(val) > -1 || cur.label.toUpperCase().indexOf(val) > -1) {
              that.options.push(cur);
            }
          }
        });
      }
    },

    select: function (cur) {
      console.log("seelect oK");
      this.editedval = cur.code;
      this.$emit("change", cur);
      this.options = false;
    },
  }
});
