"use strict";

(function () {
  ns.logEvent("Writing", "edit");
  console.log("Loading view writing edition panel");

  function newMove() {
    var defAsset = ns.getLoca;
    return {
      amount: null,
      account: {
        path: null,
        label: null
      },
      direction: null,
      asset: {
        path: ns.accounting.defAsset.value,
        label: ns.accounting.defAsset.text,
        unit: null
      }
    };
  }

  function newWri() {
    return {
      id: null,
      date: Math.round(Date.now() / 1000),
      status: "PENDING",
      label: null,
      proof: null,
      accounting_journal: '',
      moves: [newMove(), newMove()]
    };
  }

  ns.editWriting = {
    load: function (writing, clone) {
      var thatWriting = writing;
      $(window).openModal("editWritingWindow", "editWriting", function (
        $modal,
        init
      ) {
        if (init) {
          window.edWriV = new Vue({
            el: "#editWriting",
            data: {
              writing: {
                date: 0
              },
              submited: false,
              readonly: false,
              balances: {}
            },
            methods: {
              submitWri: function () {
                this.submited = true;
                var isOk =
                  this.writing.label &&
                  this.writing.status &&
                  this.writing.date,
                  ajaxOpt = {
                    indicator: "#editWritingWindow .ajax-indicator",
                    feedback: "#editWritingWindow .feedBack"
                  };

                ns.logEvent("Writing", "submit");

                if (isOk) {
                  this.rekon();
                  for (var ass in this.balances) {
                    var bal = this.balances[ass];
                    if (Math.abs(bal.credit - bal.debit) > 0.099) {
                      isOk = false;
                      break;
                    }
                  }
                }

                if (isOk) {
                  console.log("Saving writing");

                  var req = ns.cloneObject(this.writing);
                  req.moves = req.moves.filter(function (obj) {
                    return obj.amount > 0 && obj.account && obj.asset;
                  });
                  if (req.moves.length > 0) {
                    ns.ws(
                      req.id ? "PUT" : "POST",
                      "v1/accounting/writing",
                      ns.cloneObject(this.writing),
                      ajaxOpt
                    ).then(function (result) {
                      $("#editWritingWindow").toggleFade(false);
                      ns.editWriting.onSuccess(result);
                    });
                  }
                }
              },
              cloneWri: function () {
                this.writing = ns.cloneObject(this.writing);
                this.writing.id = null;
                this.writing.status = "PENDING";
                var mvNb = this.writing.moves ? this.writing.moves.length : 0;
                for (var i = 0; i < mvNb; i++) {
                  var move = this.writing.moves[i];
                  move.writing_id = null;
                  move.id = null;
                }
                this.readonly = false;
              },
              deleteWri: function () {
                if (confirm(loc.confirm)) {
                  var ajaxOpt = {
                    feedback: "#editWritingWindow .feedBack"
                  };
                  ns.ws(
                    "DELETE",
                    "v1/accounting/writing/" + this.writing.id,
                    null,
                    ajaxOpt
                  ).then(function (result) {
                    $("#editWritingWindow").toggleFade(false);
                    ns.editWriting.onSuccess(result);
                  });
                }
              },
              emptyErr: function (val) {
                return this.submited && (val == null || val == "")
                  ? "errField"
                  : "";
              },
              addLine: function () {
                writing.moves.push(newMove());
              },
              setMAmount: function (move, direction, amount) {
                move.direction = direction;
                move.amount = ns.parseNumber(amount);
                this.rekon();
              },
              getMAmount: function (move, direction) {
                return move.direction == direction ? move.amount : "";
              },
              changeVal: function (field, move, $event) {
                var fld = move[field];
                fld.path = $event.path;
                fld.label = $event.label;
                fld.id = $event.value;
                this.rekon();
              },
              rekon() {
                this.balances = {};
                var mvNb =
                  (this.writing.moves && this.writing.moves.length) || 0,
                  addLine = false;
                for (var i = 0; i < mvNb; i++) {
                  var mv = this.writing.moves[i],
                    b =
                      this.balances[mv.asset] ||
                      (this.balances[mv.asset] = {
                        debit: 0,
                        credit: 0
                      });
                  if (mv.direction && mv.amount) {
                    b[mv.direction.toLowerCase()] += mv.amount;
                  }
                  addLine = mv.account && mv.account.id > 0;
                }
                if (addLine && !this.readonly)
                  this.writing.moves.push(newMove());
                for (var ass in this.balances) {
                  var bal = this.balances[ass];
                  if (bal.credit || bal.debit) {
                    bal.creditBal =
                      bal.credit - bal.debit > 0.0099
                        ? bal.credit - bal.debit
                        : null;
                    bal.debitBal =
                      bal.debit - bal.credit > 0.0099
                        ? bal.debit - bal.credit
                        : null;
                  }
                }
              },
              initWri(theWri) {
                this.submited = false;
                if (theWri) {
                  this.writing = theWri;
                  if (theWri.date)
                    this.dp.state.selectedDate = new Date(theWri.date * 1000);
                  if (clone) {
                    this.writing.id = null;
                    this.writing.status = "PENDING";
                  }
                  this.readonly = this.writing.status == "VALID";
                  this.rekon();
                } else {
                  this.writing = newWri();
                  if(this.dp)
                    this.dp.state.selectedDate = new Date();
                }
              }
            },
            mounted: function () {
              var that = this;
              (this.dp = ns.setDatePicker(".datePicker")),
                this.dp.on("statechange", function (_, piker) {
                  that.writing.date = ns.toTs(piker.state.selectedDate);
                }); // Change model when dp changes
            }
          });
        }

        // Data init
        window.edWriV.initWri(thatWriting);
      });
    },
    onSuccess: function (result) {
      console.log("Writing edition success");
    }
  };
})();

//# sourceURL=editWriting.js
