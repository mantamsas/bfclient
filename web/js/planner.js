"use strict";

ns.planner = {};

ns.planner.dateFormat = function (ts) {
    return value ? new Date(value * 1000).format(loc.date.dateFormat) : '';
};

Vue.filter('dateFormat', function (value) {
    return ns.planner.dateFormat(value);
})

ns.planner = new Vue({
    el: '#planner-view',
    data: {
        planned_tasks: [],
        ptcount: 0,
        ptpage: 1,
        ptnoContent: false,
        ptloading: true,
        pageSize: 50
    },
    mounted: function () {
        this.loadTasks();
        this.showpage = true;
    },
    methods: {
        loadTasks: function (pageNb, source) {
            this.loading = true;
            if (pageNb) {
                this.ptpage = pageNb;
            }
            var that = this,
                url = 'v1/planned_task?pageNb=' + (this.ptpage || 1);
            url += '&pageSz=50'; //&source=former_msg

            ns.ws('GET', url, null, { handleError: true }).then(function (result) {
                that.planned_tasks = result.planned_tasks;
                that.ptcount = result.planned_tasks ? result.planned_tasks.length : 0;
                that.ptloading = false;
                that.ptnoContent = !result.planned_tasks || result.planned_tasks.length == 0;
            });
        },
        taskExpl: function (task) {
            var act = task.action,
                page = act.content_type == "GENERIC_MESSAGE" ? "generic" : "commercial",
                mUrl = 'edit_' + page + '.html?messageId=' + act.template_id;
            return task.title && task.title.replace("#msg", mUrl);
        },
        deleteTask: function (taskId) {
            var that = this;

            ns.ws('DELETE', 'v1/planned_task/' + taskId).then(function (result) {
                that.loadTasks();
            });
        }
    }
});

Vue.filter('dateFormat', function (value) {
    return value ? new Date(value * 1000).format(loc.date.dateFormat) : '';
})
