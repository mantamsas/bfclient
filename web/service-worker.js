"use strict";

var content = [
    'css/ads.css',
    'css/editing.css',
    'css/jquery.fileupload.css',
    'css/jquery.pnotify.css',
    'css/large.css',
    'css/style_ie.css',
    'css/style.css',
    'css/styleHtml5.css',
    'fr/accountBalance.html',
    'fr/accounting-home.html',
    'fr/accountReport.html',
    'fr/adList.html',
    'fr/adminConsole.html',
    'fr/cancelInvoice.html',
    'fr/catalog-pickerv.html',
    'fr/catalog.html',
    'fr/chartOptions.html',
    'fr/contact.html',
    'fr/contacts.html',
    'fr/delete-account.html',
    'fr/edit_commercialv.html',
    'fr/edit_generic.html',
    'fr/editWriting.html',
    'fr/export-fec.html',
    'fr/export.html',
    'fr/functions.html',
    'fr/help-sign.html',
    'fr/help.html',
    'fr/id-settings.html',
    'fr/importWriting.html',
    'fr/inalterable-mode.html',
    'fr/index.html',
    'fr/legal.html',
    'fr/licence.html',
    'fr/locale.js',
    'fr/message-settings.html',
    'fr/messages_list.html',
    'fr/money-transfer.html',
    'fr/numbering.html',
    'fr/oauth.html',
    'fr/overview.html',
    'fr/passwordChange.html',
    'fr/payend.html',
    'fr/payment-editorv.html',
    'fr/payment-type.html',
    'fr/payment.html',
    'fr/personEdition.html',
    'fr/planner.html',
    'fr/print-settings.html',
    'fr/register_identity.html',
    'fr/register_welcome.html',
    'fr//v/register/',
    'fr/sending.html',
    'fr/sendingv.html',
    'fr/signIn.html',
    'fr/subscription-settings.html',
    'fr//v/invoicing/subscription/',
    'fr/summaryHelp.html',
    'fr/support.html',
    'fr/tutorial.html',
    'fr/unwanted.html',
    'fr/updates.html',
    'fr/useCode.html',
    'fr/user-settings.html',
    'fr/usual-mentions.js',
    'fr/welcome.html',
    'img/body-background.png',
    'img/classic-offer-background.png',
    'img/date-picker.png',
    'img/download.png',
    'img/downloadify_bt.png',
    'img/edition-invoice-dashboard-header.gif',
    'img/edition-quote-dashboard-header.gif',
    'img/logo-mail.png',
    'img/logo-no-slogan-2.png',
    'img/logo-no-slogan-2.svg',
    'img/logo-no-slogan.png',
    'img/logo.png',
    'img/menu-background.png',
    'img/skyline-offer-background.png',
    'img/social-networks-sprite.png',
    'img/standard-offer-background.png',
    'img/stripe-logo-blue.png',
    'img/stripe-logo-white.png',
    'js/accountBalance.js',
    'js/accounting-home.js',
    'js/accounting.js',
    'js/accountReport.js',
    'js/activity-report.js',
    'js/ad.js',
    'js/admin.js',
    'js/cancelInvoice.js',
    'js/catalog-pickerv.js',
    'js/catalog.js',
    'js/contacts.js',
    'js/currency-picker.js',
    'js/delete-account.js',
    'js/edit-commercialv.js',
    'js/edit-generic.js',
    'js/editor.js',
    'js/editWriting.js',
    'js/enrollCb.js',
    'js/export-fec.js',
    'js/exporting.js',
    'js/forceSignIn.js',
    'js/idSettings4.js',
    'js/importWriting.js',
    'js/ledger.js',
    'js/journal.js',
    'js/messages_list.js',
    'js/messageSettings.js',
    'js/moneyTransfert.js',
    'js/ns4.js',
    'js/oauth.js',
    'js/passwordChange.js',
    'js/payend.js',
    'js/payment-editorv.js',
    'js/payment-type-str.js',
    'js/payment-type.js',
    'js/payment.js',
    'js/personEditor.js',
    'js/personEditorv2.js',
    'js/planner.js',
    'js/printSettings.js',
    'js/redir.js',
    'js/register_identity2.js',
    'js/register_welcome.js',
    'js/register.js',
    'js/registerProposal.js',
    'js/sending.js',
    'js/sendingv.js',
    'js/settings.js',
    'js/stripeloader.js',
    'js/subscription.js',
    'js/subsSettings.js',
    'js/update-warn.js',
    'js/useCode.js',
    'js/userSettings.js',
    'js/vue-comp.js',
    'apple-touch-icon-57x57-precomposed.png',
    'apple-touch-icon-72x72-precomposed.png',
    'apple-touch-icon-114x114-precomposed.png',
    'apple-touch-icon-precomposed.png',
    'apple-touch-icon.png',
    'blank.html',
    'crossdomain.xml',
    'favicon.ico',
    'index.html',
    'logo_square77.png',
    'maint.html',
    'useCode.html'];

var cacheName = 'cache-name2',
    cache1 = caches.open(cacheName);

self.addEventListener('install', function (event) {
    console.log("PWA install");

    event.waitUntil(
        cache1.then((cache) => {
            content.forEach(url => {
                console.log('[Service Worker] Caching ' + url);
                cache.add(url);
            });
            //return cache.addAll(content);
        })
    );
});

self.addEventListener('activate', function (event) {
    console.log("SW activate");
});

self.addEventListener('fetch', function (event) {
    var request = event.request,
        requestUrl = new URL(request.url),
        pn = requestUrl.pathname;

    if (!pn.includes('/api/'))
        event.respondWith(
            cache1.then(
                function (cache) {
                    return cache.match(pn);
                },
                function (ars) {
                    console.log('No cache for ' + pn);
                }
            ).then(function (response) {
                return response ? response : fetch(request);
            }, function (arg) {
                console.log('No resp for ' + pn);
            }
            )
        );
});