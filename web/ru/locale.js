if (!loc) {
  var loc = {};
}
loc.locale = "fr";

loc.payOk = "Votre paiement a bien été pris en compte";
loc.ok = "Opération réussie";
loc.ko = "Opération en échec";
loc.cancel = "Opération annulée";
loc.wait = "Opération en attente";

loc.send = "Envoyer";
loc.confirm = "Etes-vous certain de vouloir faire cela ?";
loc.generalError = "Votre demande n`'a pu aboutir.";
loc.mandatoryFields = "Ces champs sont obligatoires";
loc.pleasAgreeLicence =
  "Veuillez accepter les conditions générale d'utilisation pour continuer.";
loc.passwordConfirm =
  "Le mot de passe et sa confirmation ne sont pas identiques.";
loc.other = "Autre";
loc.results = "résultats";

loc.entreprise = "Entreprise";
loc.addresses = "Adresses";
loc.contacts = "Contacts";
loc.newContact = "Nouveau contact";

loc.fillLater = "Non rempli";
loc.noResultsSearch = "Pas de résultats, veuillez lancer une rechercher.";
loc.noResultsSearchFor = 'Pas de résultats pour "{0}"';
loc.workUnsaved =
  "Votre travail sera perdu s'il est pas sauvegadé dans un compte. Voulez-vous toujours fermer cette fenêtre ?";
loc.shouldBeAuth =
  "Vous devez être identifié pour faire cela. Veuillez vous re-connecter ou vous enregistrer.";
loc.shouldBeHttps =
  "Votre connection doit être sécurisée par HTTPS pour cela. Passer en HTTPS ?";
loc.saveItFirst = "Vous devez sauver votre travail avant de faire cela.";
loc.popupBlocked =
  "Pour un bon fonctionnement, veuillez désactiver le bloquage des popup pour ce site.";
loc.thank = "Merci!";
loc.someone = "quelqu'un";
loc.paymentLeft = "A payer {0}";

loc.payDelay = "dans {0} j";
loc.payLate = "avec {0} j de retard";

loc.del = "Supprimer";
loc.edit = "Editer";
loc.setIdentity = "Définir comme identité en cours";
loc.newDoc = "Nouveau document";

loc.phoneAbrev = "Tel.";
loc.faxAbrev = "Fax";
loc.fromUntil = "{0} du {1} jusqu'au {2}";
loc.fromNow = "{0} du {1} jusqu'à présent (abonnement)";

loc.messageStatus = {};
loc.messageStatus["NONE"] = "Non sauvegardé";
loc.messageStatus["MODIFIED"] = "Brouillon";
loc.messageStatus["SENT"] = "Envoyé";
loc.messageStatus["INVOICE-SENT"] = "Envoyée";
loc.messageStatus["ISSUED"] = "Emis";
loc.messageStatus["INVOICE-ISSUED"] = "Emise";
loc.messageStatus["RECEIVED_BY"] = "Reçu";
loc.messageStatus["INVOICE-RECEIVED_BY"] = "Reçue";
loc.messageStatus["SENDER_ACK_RECEPTION"] = "Reçu";
loc.messageStatus["INVOICE-SENDER_ACK_RECEPTION"] = "Reçue";
loc.messageStatus["ACCEPTED"] = "Accepté";
loc.messageStatus["INVOICE-ACCEPTED"] = "Acceptée";
loc.messageStatus["REFUSED"] = "Refusé";
loc.messageStatus["INVOICE-REFUSED"] = "Refusée";
loc.messageStatus["CLOSED"] = "Terminé";
loc.messageStatus["ISSUE-ACTION_CANCELED"] = "Emission annulée";
loc.messageStatus["CLOSED-ACTION_CANCELED"] = "Fermeture annulée";
loc.messageStatus["TRASH"] = "Corbeille";
loc.messageStatus["DELETED"] = "Supprimé";
loc.messageStatus["INVOICE-DELETED"] = "Supprimée";
loc.messageStatus["PAYMENT"] = "Payé en partie";
loc.messageStatus["UNPAID"] = "A payer";
loc.messageStatus["PAID"] = "Payé";
loc.messageStatus["UNREAD"] = "Non lu";
loc.messageStatus["READ"] = "Lu";

loc.placeholder = {};
loc.placeholder["address-name"] = "Nom ou raison sociale";
loc.placeholder["address-street1"] = "Adresse";
loc.placeholder["address-street2"] = "Adresse (suite)";
loc.placeholder["postal_code-code"] = "Code postal";
loc.placeholder["postal_code-city"] = "Ville";
loc.placeholder["country"] = "Pays";
loc.placeholder["address-misc"] = "Autre informations";
loc.placeholder["person-label"] = "Nom du contact";
loc.placeholder["person-email"] = "Email du contact";
loc.placeholder["person-phone"] = "Téléphone du contact";
loc.placeholder["person-fax"] = "Fax du contact";
loc.placeholder["person-misc"] = "Autres informations sur le contact";

loc.addressDel = "L 'adresse à été supprimée";

Number.prototype.formatMoney = function () {
  return this.formatMoneyBase(2, ",", " ");
};

loc.defaultCurrency = "EUR";

loc.date = {
  days: ["dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam."],
  dayNames: [
    "dimanche",
    "lundi",
    "mardi",
    "mercredi",
    "jeudi",
    "vendredi",
    "samedi"
  ],
  months: [
    "janvier",
    "février",
    "mars",
    "avril",
    "mai",
    "juin",
    "juillet",
    "août",
    "septembre",
    "octobre",
    "novembre",
    "décembre"
  ],
  monthNamesShort: [
    "janv.",
    "févr.",
    "mars",
    "avr.",
    "mai",
    "juin",
    "juil.",
    "août",
    "sept.",
    "oct.",
    "nov.",
    "déc."
  ],
  today: "Aujourd'hui",
  clear: "Init",
  close: "Fermer",
  dateFormat: "dd/mm/yy",
  dateTimeFormat: "dd/mm/yy HH:MM"
};

loc.number = {
  dec_sep: ",",
  thou_sep: " "
};

loc.linkStatus = {};
loc.linkStatus.loading = "Chargement...";
loc.linkStatus.saving = "Sauvegarde...";
loc.linkStatus.saved = "Contenu sauvegardé";
loc.linkStatus.saveError = "Erreur de sauvegarde";
loc.linkStatus.error = "Erreur";

loc.history = {};
loc.yourself = "vous-même";
loc.history["MODIFIED"] = "Modifié par {0}";
loc.history["ISSUED"] = "Emis par {0}";
loc.history["SENT"] = "Envoyé par {0}";
loc.history["RECEIVED_BY"] = "Reçu par {0}";
loc.history["SENDER_ACK_RECEPTION"] = "Expéditeur {0} averti de la réception";
loc.history["ACCEPTED"] = "Accepté par {0}";
loc.history["REFUSED"] = "Refusé par {0}";
loc.history["PAYMENT"] = "Paiement enregistré par {0}";
loc.history["CLOSED"] = "Fermé par {0}";
loc.history["ACTION_CANCELED"] = "Annulé le {0} par {1}";
loc.history["TRASH"] = "Mis à la corbeille par {0}";
loc.history["DELETED"] = "Supprimé de ses registres par {0}";
loc.history.sending = {};
loc.history.sending["OTHER"] = "à {0} (hors site)";
loc.history.sending["EMAIL"] = "à {0} par e-mail";
loc.history.sending["FAX"] = "à {0} par fax au {1}";
loc.history.sending["POST"] = "à {0} par courrier postal au {1}";
loc.history.sending["SMS"] = "à {0} par SMS au {1}";
loc.history.sending["SITE"] = "à {0} par le site";

loc.messageTypes = {};
loc.messageTypes["QUOTE_REQUEST"] = "Demande de devis";
loc.messageTypes["QUOTE"] = "Devis";
loc.messageTypes["PURCHASE_ORDER"] = "Commande";
loc.messageTypes["INVOICE"] = "Facture";
loc.messageTypes["PROFORMA_INVOICE"] = "Facture proforma";
loc.messageTypes["SELF_INVOICE"] = "Facture";
loc.messageTypes["FEE_NOTE"] = "Note d'honoraire";
loc.messageTypes["CREDIT_NOTE"] = "Avoir";
loc.messageTypes["PAYMENT_REQUEST"] = "Demande de paiement";
loc.messageTypes["USER_FEEDBACK"] = "Remarque utilisateur";
loc.messageTypes["LETTER"] = "Lettre";

loc.paymentTypes = {};
loc.paymentTypes.CASH = "en espèces";
loc.paymentTypes.CB = "par carte banquaire";
loc.paymentTypes.PAYPAL = "par Paypal";
loc.paymentTypes.TRANSFERT = "par virement";
loc.paymentTypes.CHECK = " par chèque";
loc.paymentTypes.OTHER = "";

loc.paymentSent = "Le paiement a été envoyé avec la référence {0}";

loc.writingStatus = {};
loc.writingStatus.VALID = "Validé";
loc.writingStatus.UNVALID = "Erreur";
loc.writingStatus.PENDING = "Non-terminé";
loc.writingStatus.CHALLENGED = "Litige";

loc.smtpStatus = {};
loc.smtpStatus.boxFull = "Boite email pleine";
loc.smtpStatus.badAddress = "Adresse email erronée";
loc.smtpStatus.tooBig = "Message trop volumineux";
loc.smtpStatus.tempProblem =
  "Erreur temporaire de serveur courrier du destinataire";
loc.smtpStatus.generalError = "Erreur de serveur courrier du destinataire";

loc.first = "Premier";
loc.prev = "Précédent";
loc.page = "Page {0}";
loc.next = "Suivant";
loc.last = "Dernier";

loc.statFeedback = "{0} de {1} à {2}, groupé par {3}";
loc.year = "année";
loc.quarter = "trimestre";
loc.month = "mois";
loc.week = "semaine";
loc.message_number = "numéro de message";
loc.toIssue = "à émettre";
loc.recipient = "destinataire";
loc.reference = "référence";
loc.description = "description";
loc.tax_rate = "taux de taxe";
loc.none = "période";

loc.registerProposal = {
  title: "Validation de l'e-mail",
  initialText:
    '<p><b>{0}</b> vous a envoyé un message à l\'adresse <b>{1}</b>.</p><p>Après avoir lu et accepté les <a href="licence.html">Conditions Générales</a>, veuillez valider cette adresse en cliquant sur le lien que nous allons vous y envoyer.</p><p>Par la suite, vous aurez directement accès à vos messages.</p>',
  sendMeLink: "Envoyez-moi le lien !",
  confirmation:
    "Veuillez cliquer sur ce lien pour prendre connaissance de votre message. "
};

loc.countries = {
  AF: "Afghanistan",
  ZA: "Afrique du Sud",
  AL: "Albanie",
  DZ: "Algérie",
  DE: "Allemagne",
  AD: "Andorre",
  AO: "Angola",
  AI: "Anguilla",
  AQ: "Antarctique",
  AG: "Antigua et Barbuda",
  AN: "Antilles Néerlandaises",
  SA: "Arabie Saoudite",
  AR: "Argentine",
  AM: "Arménie",
  AW: "Aruba",
  AU: "Australie",
  AT: "Autriche",
  AZ: "Azerbaïdjan",
  BS: "Bahamas",
  BH: "Bahreïn",
  BD: "Bangladesh",
  BB: "Barbade",
  BE: "Belgique",
  BZ: "Bélize",
  BJ: "Benin",
  BM: "Bermudes",
  BT: "Bhoutan",
  BY: "BiéloRussie",
  BO: "Bolivie",
  BA: "Bosnie-Herzégovine",
  BW: "Botswana",
  BV: "Île Bouvet",
  BR: "Brésil",
  BN: "Brunei",
  BG: "Bulgarie",
  BF: "Burkina Faso",
  BI: "Burundi",
  KY: "Îles Caïmans",
  KH: "Cambodge",
  CM: "Cameroun",
  CA: "Canada",
  CV: "Cap Vert",
  CL: "Chili",
  CN: "Chine",
  CX: "Île Christmas",
  CY: "Chypre",
  VA: "Vatican",
  CC: "Îles Cocos",
  CO: "Colombie",
  KM: "Comores",
  CG: "Congo",
  CD: "République démocratique du Congo",
  CK: "Îles Cook",
  KR: "Corée du Sud",
  KP: "Corée du Nord",
  CR: "Costa Rica",
  CI: "Côte d'Ivoire",
  HR: "Croatie",
  CU: "Cuba",
  DK: "Danemark",
  UM: "Dépendances américaines du Pacifique",
  DJ: "Djibouti",
  DM: "Dominique",
  EG: "Egypte",
  AE: "Emirats Arabes Unis",
  EC: "Equateur",
  ER: "Erythrée",
  ES: "Espagne",
  EE: "Estonie",
  US: "Etats-Unis",
  ET: "Ethiopie",
  MK: "Macédoine",
  FO: "Îles Féroé",
  FJ: "Fidji",
  FI: "Finlande",
  FR: "France",
  GA: "Gabon",
  GM: "Gambie",
  GE: "Géorgie",
  GS: "Géorgie du Sud et îles Sandwich du Sud",
  GH: "Ghana",
  GI: "Gibraltar",
  GR: "Grèce",
  GD: "Grenade",
  GL: "Groenland",
  GP: "Guadeloupe",
  GU: "Guam",
  GT: "Guatemala",
  GN: "Guinée",
  GQ: "Guinée Equatoriale",
  GW: "Guinée-Bissau",
  GY: "Guyana",
  GF: "Guyane française",
  HT: "Haïti",
  HM: "Île Heard et îles McDonald",
  HN: "Honduras",
  HU: "Hongrie",
  IN: "Inde",
  ID: "Indonésie",
  IQ: "Irak",
  IR: "Iran",
  IE: "Irlande",
  IS: "Islande",
  IL: "Israël",
  IT: "Italie",
  JM: "Jamaïque",
  JP: "Japon",
  JO: "Jordanie",
  KZ: "Kazakhstan",
  KE: "Kenya",
  KG: "Kyrgyzstan",
  KI: "Kiribati",
  KW: "Koweit",
  RE: "La Réunion",
  LA: "Laos",
  LS: "Lesotho",
  LV: "Lettonie",
  LB: "Liban",
  LR: "Liberia",
  LY: "Libye",
  LI: "Liechtenstein",
  LT: "Lithuanie",
  LU: "Luxembourg",
  MG: "Madagascar",
  MY: "Malaisie",
  MW: "Malawi",
  MV: "Maldives",
  ML: "Mali",
  FK: "Îles Malouines",
  MT: "Malte",
  MP: "Îles Mariannes du Nord",
  MA: "Maroc",
  MH: "Îles Marshall",
  MQ: "Martinique",
  MU: "Maurice",
  MR: "Mauritanie",
  YT: "Mayotte",
  MX: "Mexique",
  FM: "Micronésie",
  MD: "Moldavie",
  MC: "Monaco",
  MN: "Mongolie",
  MS: "Montserrat",
  MZ: "Mozambique",
  MM: "Myanmar",
  NA: "Namibie",
  NR: "Nauru",
  NP: "Népal",
  NI: "Nicaragua",
  NE: "Niger",
  NG: "Nigéria",
  NU: "Niue",
  NF: "Île Norfolk",
  NO: "Norvège",
  NC: "Nouvelle-Calédonie",
  NZ: "Nouvelle-Zélande",
  OM: "Oman",
  UG: "Ouganda",
  UZ: "Ouzbékistan",
  PK: "Pakistan",
  PW: "Belau",
  PA: "Panama",
  PG: "Papouasie-Nouvelle-Guinée",
  PY: "Paraguay",
  NL: "Pays-Bas",
  PE: "Pérou",
  PH: "Philippines",
  PN: "Pitcairn",
  PL: "Pologne",
  PF: "Polynésie Française",
  PR: "Porto Rico",
  PT: "Portugal",
  QA: "Qatar",
  HK: "Hong-Kong",
  MO: "Macao",
  CF: "République Centre-Africaine",
  DO: "République Dominicaine",
  CZ: "République Tchèque",
  RO: "Roumanie",
  GB: "Royaume-Uni",
  RU: "Russie",
  RW: "Rwanda",
  KN: "Saint-Christophe-et-Niévès",
  SH: "Sainte-Hélène",
  LC: "Sainte-Lucie",
  SM: "Saint-Marin",
  PM: "Saint-Pierre-et-Miquelon",
  VC: "Saint-Vincent-et-les Grenadines",
  SB: "Îles Salomon",
  SV: "El Salvador",
  WS: "Samoa",
  AS: "Samoa américaines",
  ST: "Sao Tomé et Principe",
  SN: "Sénégal",
  SC: "Seychelles",
  SL: "Sierra Leone",
  SG: "Singapour",
  SK: "Slovaquie",
  SI: "Slovénie",
  SO: "Somalie",
  SD: "Soudan",
  LK: "Sri Lanka",
  SE: "Suède",
  CH: "Suisse",
  SR: "Suriname",
  SJ: "Svalbard et Jan Mayen",
  SZ: "Swaziland",
  SY: "Syrie",
  TJ: "Tadjikistan",
  TW: "Taiwan",
  TZ: "Tanzanie",
  TD: "Tchad",
  TF: "Territoires Français du Sud",
  IO: "Territoires britanniques de l'Océan Indien",
  TH: "Thaïlande",
  TP: "TP",
  TG: "Togo",
  TK: "Tokelau",
  TO: "Tonga",
  TT: "Trinité-et-Tobago",
  TN: "Tunisie",
  TM: "Turkménistan",
  TC: "Îles Turks et Caicos",
  TR: "Turquie",
  TV: "Tuvalu",
  UA: "Ukraine",
  UY: "Uruguay",
  VU: "Vanuatu",
  VE: "Vénézuela",
  VI: "Iles Vierges Américaines",
  VG: "Iles Vierges Britanniques",
  VN: "Vietnam",
  WF: "Wallis-et-Futuna",
  YE: "Yémen",
  ZM: "Zambie",
  ZW: "Zimbabwe"
};
