var sum = {
  template: `\
  <div style="padding-bottom: 1.5em">\
<p>\
  Cliquez sur les liens ci-dessous pour ajouter des mentions types à votre\
  document.\
</p>\
<h2 style="padding-top: 0.5em">Generalités</h2>\
<ul>\
  <li><a href="#" @click.prevent.stop="paste($event)">En votre aimable réception.</a></li>\
  <li><a href="#" @click.prevent.stop="paste($event)">Paiement à réception de la facture.</a></li>\
  <li>Pénalités de retard, escompte.</li>\
</ul>\
<h2 style="padding-top: 0.5em">Lois françaises</h2>\
<ul>\
  <li>\
    Si vous êtes membre d'une AGA ou d'un CGA:\
    <a href="#" @click.prevent.stop="paste($event)"\
      >Membre d'une AGA ou d'un CGA, le règlement par chèque est accepté.</a\
    >\
  </li>\
  <li>\
    Si vous avez le statut de micro-entreprise:\
    <a href="#" @click.prevent.stop="paste($event)"\
      >T.V.A. non applicable ou exonérée, article 293 B du CGI.</a\
    >\
  </li>\
  <li>\
    Livraison extra-communautaire:\
    <a href="#" @click.prevent.stop="paste($event)"\
      >T.V.A. non applicable ou exonérée, article 262 ter I du CGI.</a\
    >\
  </li>\
  <li>\
    Prestations localisables extra-territoriales:\
    <a href="#" @click.prevent.stop="paste($event)"\
      >T.V.A. non applicable ou exonérée, article 259 A du CGI.</a\
    >\
  </li>\
  <li>\
    Prestations immatérielles extra-territoriales:\
    <a href="#" @click.prevent.stop="paste($event)"\
      >T.V.A. non applicable ou exonérée, article 259 B du CGI.</a\
    >\
  </li>\
  <li>\
    <a href="#" @click.prevent.stop="paste($event)">Franchise TVA spécifique oeuvres sans but lucratif, article 261-7-1° -b\
      du CGI.</a>\
  </li>\
  <li>\
    <a href="#" @click.prevent.stop="paste($event)">Manifestation de soutien exonérée: article 261-1°-c° du CGI.</a>\
  </li>\
</ul>\
</div>\
`,
  methods: {
    paste: function(event) {
      this.$emit("paste", event.target.innerHTML);
    }
  }
};
sum;
